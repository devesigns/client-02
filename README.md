This project uses React-Native. To get started, visit: https://facebook.github.io/react-native/docs/getting-started

To build the project:

* Install node_modules: At project root, run ```npm install```
* On MacOS, navigate to **ios** folder and run ```pod install```, then open the project **.workspace** file