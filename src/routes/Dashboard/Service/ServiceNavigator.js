import {
    createStackNavigator,
} from 'react-navigation';

import ServiceScene from '../../../components/Dashboard/Service/ServiceScene';
import ServiceListScene from '../../../components/Dashboard/Service/ServiceListScene';
import ServiceListSelectedScene from '../../../components/Dashboard/Service/ServiceListSelectedScene';
import ServiceDetailsScene from '../../../components/Dashboard/Service/ServiceDetailsScene';
import ServiceCompleteScene from '../../../components/Dashboard/Service/ServiceCompleteScene';
import ServiceContactUserProfileScene from '../../../components/Dashboard/Service/ServiceContactUserProfileScene';
import { verticalScale } from '../../../utils';

const ServiceStack = createStackNavigator(
    {
        ServiceHome: {
            screen: ServiceScene,
            navigationOptions: {
                title: 'Dịch vụ hỗ trợ'
            }
        },
        ServiceList: {
            screen: ServiceListScene,
            navigationOptions: {
                title: 'Giúp việc xanh'
            }
        },
        ServiceListSelected: {
            screen: ServiceListSelectedScene,
            navigationOptions: {
                title: 'Dịch vụ đã chọn'
            }
        },
        ServiceDetails: {
            screen: ServiceDetailsScene,
            navigationOptions: {
                title: 'Thuê dịch vụ'
            }
        },
        ServiceComplete: {
            screen: ServiceCompleteScene,
            navigationOptions: {
                title: 'Tóm tắt dịch vụ',
                headerLeft: null
            }
        },
        ServiceContactUserProfileScene: {
            screen: ServiceContactUserProfileScene
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

ServiceStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }
    
    return {
        tabBarVisible
    };
};

export default ServiceStack;