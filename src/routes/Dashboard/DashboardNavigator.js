// Routing for the Dashboard tabs
// (each tab can be a navigation stack for further navigation)

import React from 'react';
import {
    View
} from 'react-native';
import {
    createBottomTabNavigator,
} from 'react-navigation';

// Import nav stacks
import HomeStack from './Home/HomeNavigator';
import ServiceStack from './Service/ServiceNavigator';
import ChoreStack from './Chore/ChoreNavigator';
import AssignmentTabs from './Assignment/AssignmentNavigator';
import AccountStack from './Account/AccountNavigator';

import Icon from 'react-native-vector-icons/FontAwesome';
import InvitationNotiTabBarIcon from '../../components/Dashboard/Assignment/InvitationNotiTabBarIcon';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../utils';

const styles = {
    tabStyle: {
        padding: 5
    },
    labelStyle: {
        fontSize: verticalScale(10)
    },
    tabBarStyle: {
        height: verticalScale(50)
    },
    iconSize: verticalScale(24)
}

const DashboardTabs = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                title: 'Trang chủ',
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="home" size={styles.iconSize} color={tintColor} />
                )
            }
        },
        Service: {
            screen: ServiceStack,
            navigationOptions: {
                title: 'Dịch vụ',
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="th-large" size={styles.iconSize} color={tintColor} />
                )
            }
        },
        Chore: {
            screen: ChoreStack,
            navigationOptions: {
                title: 'Checklist',
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="sticky-note" size={styles.iconSize} color={tintColor} />
                )
            }
        },
        Assignment: {
            screen: AssignmentTabs,
            navigationOptions: {
                title: 'Việc của tôi',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon name="list" size={styles.iconSize} color={tintColor} />
                        <InvitationNotiTabBarIcon />
                    </View>
                )
            }
        },
        Account: {
            screen: AccountStack,
            navigationOptions: {
                title: 'Tài khoản',
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="user" size={styles.iconSize} color={tintColor} />
                )
            }
        },
    },
    {
        tabBarOptions: {
            style: styles.tabBarStyle,
            tabStyle: styles.tabStyle,
            labelStyle: styles.labelStyle,
            activeTintColor: '#29ABE2',
            inactiveTintColor: '#E5E5E5'
        }
    }
);

export default DashboardTabs;