import {
    createStackNavigator,
} from 'react-navigation';

import HomeScene from '../../../components/Dashboard/Home/HomeScene';

import { verticalScale } from '../../../utils';

const HomeStack = createStackNavigator(
    {
        ServiceHome: {
            screen: HomeScene,
            navigationOptions: {
                title: 'Trang chủ'
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

export default HomeStack;