import {
    createStackNavigator,
} from 'react-navigation';

import HouseMainScene from '../../../components/Dashboard/Chore/HouseMainScene';
import HouseChoreMainScene from '../../../components/Dashboard/Chore/HouseChoreMainScene';
import HouseFormScene from '../../../components/Dashboard/Chore/HouseFormScene';
import HouseRoomScene from '../../../components/Dashboard/Chore/HouseRoomScene';
import HouseRoomChoreScene from '../../../components/Dashboard/Chore/HouseRoomChoreScene';
import HouseChoreFormScene from '../../../components/Dashboard/Chore/HouseChoreFormScene';
import HouseMassAssignScene from '../../../components/Dashboard/Chore/HouseMassAssignScene';
import HouseMassDeleteScene from '../../../components/Dashboard/Chore/HouseMassDeleteScene';

import { verticalScale } from '../../../utils';

const ChoreStack = createStackNavigator(
    {
        HouseMainScene: {
            screen: HouseMainScene,
            navigationOptions: {
                title: 'Danh sách nhà ở'
            }
        },
        HouseChoreMainScene: {
            screen: HouseChoreMainScene,
        },
        HouseFormScene: {
            screen: HouseFormScene
        },
        HouseRoomScene: {
            screen: HouseRoomScene
        },
        HouseRoomChoreScene: {
            screen: HouseRoomChoreScene,
            navigationOptions: {
                title: 'Quản lý công việc phòng'
            }
        },
        HouseChoreFormScene: {
            screen: HouseChoreFormScene
        },
        HouseMassAssignScene: {
            screen: HouseMassAssignScene,
            navigationOptions: {
                title: 'Phân công hàng loạt'
            }
        },
        HouseMassDeleteScene: {
            screen: HouseMassDeleteScene,
            navigationOptions: {
                title: 'Xóa hàng loạt'
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

ChoreStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default ChoreStack;