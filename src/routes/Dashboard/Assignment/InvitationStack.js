import {
    createStackNavigator
} from 'react-navigation';

import AssignmentInvitedScene from '../../../components/Dashboard/Assignment/AssignmentInvitedScene';
import AssignmentMassAssignDetailScene from '../../../components/Dashboard/Assignment/AssignmentMassAssignDetailScene';
import AssignmentChoreViewScene from '../../../components/Dashboard/Assignment/AssignmentChoreViewScene';

import { verticalScale } from '../../../utils';

const InivitationStack = createStackNavigator(
    {

        AssignmentInvitedScene: {
            screen: AssignmentInvitedScene,
            headerMode: 'none',
            navigationOptions: {
                headerVisible: false,
                header: null
            }
        },
        AssignmentMassAssignDetailScene: {
            screen: AssignmentMassAssignDetailScene,
            navigationOptions: {
                title: 'Danh sách công việc'
            }
        },
        AssignmentChoreViewScene: {
            screen: AssignmentChoreViewScene
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

InivitationStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default InivitationStack;