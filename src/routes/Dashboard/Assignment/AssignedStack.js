import {
    Platform
} from 'react-native';
import {
    createStackNavigator
} from 'react-navigation';

import AssignmentMainScene from '../../../components/Dashboard/Assignment/AssignmentMainScene';
import AssignmentAssignedScene from '../../../components/Dashboard/Assignment/AssignmentAssignedScene';
import AssignmentChoreViewScene from '../../../components/Dashboard/Assignment/AssignmentChoreViewScene';
import AssignmentHouseViewScene from '../../../components/Dashboard/Assignment/AssignmentHouseViewScene';

import { verticalScale } from '../../../utils';

let defaultNavigationOptions = {
    headerStyle: {
        backgroundColor: '#29ABE2',
        height: verticalScale(55),
    },
    headerTitleStyle: {
        color: '#FFFFFF',
        fontSize: verticalScale(18)
    },
    headerRightContainerStyle: {
        paddingRight: verticalScale(15),
    },
    headerTintColor: '#FFFFFF'
}

if (Platform.OS === 'ios') {
    defaultNavigationOptions.headerForceInset = {
        top: 'never'
    }
}

const AssignedStack = createStackNavigator(
    {

        AssignmentMainScene: {
            screen: AssignmentMainScene,
            navigationOptions: {
                title: 'Nhà có công việc đã nhận'
            }
        },
        AssignmentAssignedScene: {
            screen: AssignmentAssignedScene
        },
        AssignmentChoreViewScene: {
            screen: AssignmentChoreViewScene
        },
        AssignmentHouseViewScene: {
            screen: AssignmentHouseViewScene
        },
    },
    {
        defaultNavigationOptions
    }
)

// AssignedStack.navigationOptions = ({ navigation }) => {
//     let tabBarVisible = true;
//     if (navigation.state.index > 0) {
//         tabBarVisible = false;
//     }

//     return {
//         tabBarVisible,
//     };
// };

export default AssignedStack;