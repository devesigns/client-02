import {
    Platform
} from 'react-native';
import {
    createMaterialTopTabNavigator
} from 'react-navigation';

import AssignedStack from './AssignedStack';
import InvitationStack from './InvitationStack';

import { verticalScale } from '../../../utils';

const AssignmentTabs = createMaterialTopTabNavigator(
    {
        AssignedStack: {
            screen: AssignedStack,
            navigationOptions: {
                title: 'Đã nhận'
            }
        },
        InvitationStack: {
            screen: InvitationStack,
            navigationOptions: {
                title: 'Được mời'
            }
        },
    },
    {
        tabBarOptions: {
            upperCaseLabel: false,
            labelStyle: {
                fontWeight: 'bold',
                color: '#333333',
                fontSize: verticalScale(14)
            },
            tabStyle: {
                height: (Platform.OS === 'ios') ? verticalScale(70) : verticalScale(40),
                paddingTop: (Platform.OS === 'ios') ? 30 : 0
            },
            style: {
                backgroundColor: "#FFFFFF"
            },
            indicatorStyle: {
                backgroundColor: '#29ABE2'
            }
        }
    }
)

export default AssignmentTabs;