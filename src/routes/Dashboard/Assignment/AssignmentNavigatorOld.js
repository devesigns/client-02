import {
    createStackNavigator
} from 'react-navigation';

import AssignmentTabs from './AssignmentNavigator';
import AssignmentChoreViewScene from '../../../components/Dashboard/Assignment/AssignmentChoreViewScene';
import AssignmentHouseViewScene from '../../../components/Dashboard/Assignment/AssignmentHouseViewScene';
import AssignmentMassAssignDetailScene from '../../../components/Dashboard/Assignment/AssignmentMassAssignDetailScene';

import { verticalScale } from '../../../utils';

const AssignmentStack = createStackNavigator(
    {
        AssignmentTabs: {
            screen: AssignmentTabs,
            navigationOptions: {
                title: 'Công việc được giao'
            }
        },
        AssignmentChoreViewScene: {
            screen: AssignmentChoreViewScene
        },
        AssignmentHouseViewScene: {
            screen: AssignmentHouseViewScene
        },
        AssignmentMassAssignDetailScene: {
            screen: AssignmentMassAssignDetailScene,
            navigationOptions: {
                title: 'Chi tiết lời mời'
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

AssignmentStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default AssignmentStack;