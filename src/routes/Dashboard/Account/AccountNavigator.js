import {
    createStackNavigator,
} from 'react-navigation';

import AccountMainScene from '../../../components/Dashboard/Account/AccountMainScene';
import AccountServiceHistoryScene from '../../../components/Dashboard/Account/AccountServiceHistoryScene';
import AccountServiceOrderDetailScene from '../../../components/Dashboard/Account/AccountServiceOrderDetailScene';
import AccountProfileScene from '../../../components/Dashboard/Account/AccountProfileScene';
import AccountChangePasswordScene from '../../../components/Dashboard/Account/AccountChangePasswordScene';
import AccountPrivacyPolicyScene from '../../../components/Dashboard/Account/AccountPrivacyPolicyScene';
import AccountInviteFriendScene from '../../../components/Dashboard/Account/AccountInviteFriendScene';

import { verticalScale } from '../../../utils';

const AccountStack = createStackNavigator(
    {
        AccountMainScene: {
            screen: AccountMainScene,
            navigationOptions: {
                title: 'Quản lý tài khoản'
            }
        },
        AccountServiceHistoryScene: {
            screen: AccountServiceHistoryScene,
            navigationOptions: {
                title: 'Lịch sử đặt dịch vụ'
            }
        },
        AccountServiceOrderDetailScene: {
            screen: AccountServiceOrderDetailScene,
            navigationOptions: {
                title: 'Chi tiết đơn hàng'
            }
        },
        AccountProfileScene: {
            screen: AccountProfileScene,
            navigationOptions: {
                title: 'Thông tin tài khoản'
            }
        },
        AccountChangePasswordScene: {
            screen: AccountChangePasswordScene,
            navigationOptions: {
                title: 'Đổi mật khẩu'
            }
        },
        AccountPrivacyPolicyScene: {
            screen: AccountPrivacyPolicyScene,
            navigationOptions: {
                title: 'Chính sách bảo mật'
            }
        },
        AccountInviteFriendScene: {
            screen: AccountInviteFriendScene,
            navigationOptions: {
                title: 'Mời bạn bè'
            }
        }
    },
    {
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#29ABE2',
                height: verticalScale(55)
            },
            headerTitleStyle: {
                color: '#FFFFFF',
                fontSize: verticalScale(18)
            },
            headerRightContainerStyle: {
                paddingRight: verticalScale(15),
            },
            headerTintColor: '#FFFFFF'
        }
    }
)

AccountStack.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default AccountStack;