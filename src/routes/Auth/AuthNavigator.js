import {
    createStackNavigator,
} from 'react-navigation';

import LoginScene from '../../components/Auth/LoginScene';


const AuthStack = createStackNavigator(
    {
        Login: {
            screen: LoginScene,
            navigationOptions: {
                header: null
            }
        }
    } 
);

export default AuthStack;
