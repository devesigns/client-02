// Controlling routing of the app

import {
    createSwitchNavigator,
    createAppContainer
} from 'react-navigation';

import AuthStack from './Auth/AuthNavigator'
import DashboardTabs from './Dashboard/DashboardNavigator'
import AuthLoadingScene from '../components/Auth/AuthLoadingScene';

// Implementation of DashboardScene, SignInScene (Auth)
// AuthLoading goes here

const AppNavigator = createSwitchNavigator(
    {   
        AuthLoading: AuthLoadingScene,
        Auth: AuthStack,
        Dashboard: DashboardTabs
    },
    {
        initialRouteName: 'AuthLoading'
    }
);

export default createAppContainer(AppNavigator);