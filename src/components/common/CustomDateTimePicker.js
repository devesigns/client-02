import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {
    CustomText
} from '../common';
import { verticalScale } from '../../utils';

class CustomDateTimePicker extends Component {
    state = {
        value: this.props.value,
        isPickerVisible: false
    }

    showDateTimePicker = () => {
        this.setState({ isPickerVisible: true });
    }

    hideDateTimePicker = () => {
        this.setState({ isPickerVisible: false });
    }
  
    handleDatePicked = (value) => {
        this.hideDateTimePicker();

        var newValue = '';
        
        if (this.props.mode === 'date') {
            newValue = value;
            this.setState({
                value: value.toLocaleDateString()
            });
        } else {
            let hour = value.getHours();
            hour = ("0" + hour).slice(-2);

            let minute = value.getMinutes();
            minute = ("0" + minute).slice(-2);

            newValue = hour + ':' + minute;

            this.setState({
                value: newValue
            });
        }
        
        this.props.onValueChange(newValue);
    };
    
    render() {
        return (
            <View style={[styles.containerStyle, this.props.style]}>
                <CustomText style={styles.labelStyle}>
                    {this.props.label}
                </CustomText>
                <TouchableOpacity 
                    style={[styles.buttonStyle, this.props.buttonStyle]}
                    onPress={this.showDateTimePicker}
                >
                    <Text style={this.state.value ? styles.buttonTextStyle : styles.buttonTextStyleNull}>
                        {this.state.value || this.props.placeholder}
                    </Text>
                </TouchableOpacity>
                <DateTimePicker
                    mode={this.props.mode}
                    isVisible={this.state.isPickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    minimumDate={this.props.minimumDate}
                />
            </View>
        );
    }
};

const styles = {
    pickerStyle: {
        marginLeft: verticalScale(-7),
        marginRight: verticalScale(-7)
    },
    buttonTextStyleNull: {
        color: '#333333',
        opacity: 0.5,
        fontSize: verticalScale(14),
        lineHeight: verticalScale(28),
        height: '100%'
    },
    buttonTextStyle: {
        color: '#111111',
        fontSize: verticalScale(14),
        lineHeight: verticalScale(28),
        // height: '100%'
    },
    buttonStyle: {
        // height: 20,
        flex: 1,
        paddingLeft: verticalScale(5),
        borderWidth: 1,
        borderColor: 'rgba(33, 33, 33, 0.5)'
    },
    labelStyle: {
        fontSize: verticalScale(10),
        // fontWeight: 'bold',
        height: verticalScale(20)
    },
    containerStyle: {
        height: verticalScale(55),
        flexDirection: 'column',
        justifyContent: 'center'
    }
};

export { CustomDateTimePicker };