import React from 'react';
import { View, StatusBar, Platform } from 'react-native';

// https://stackoverflow.com/questions/42599850/how-to-prevent-layout-from-overlapping-with-ios-status-bar
// here, we add the spacing for iOS
// and pass the rest of the props to React Native's StatusBar

const CustomStatusBar = (props) => {
    const height = (Platform.OS === 'ios') ? 20 : 0;
    const { backgroundColor } = props;

    return (
        <View style={{ height, backgroundColor, zIndex: 999 }}>
            <StatusBar { ...props } />
        </View>
    );
}

export { CustomStatusBar };