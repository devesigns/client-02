import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { verticalScale } from '../../utils';

const styles = StyleSheet.create({
  cardContainer: {
    marginLeft: verticalScale(15),
    marginRight: verticalScale(15),
    marginTop: verticalScale(15),
    marginBottom: verticalScale(5),
    position: 'relative',
    padding: verticalScale(10),
    shadowColor: '#333333',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 3,
    elevation: 3,
    backgroundColor: '#FFFFFF',
    borderRadius: verticalScale(5)
  }
});

const CardTouchable = (props) => {

    return (
        <TouchableOpacity style={ [styles.cardContainer, props.style] }   onPress={ props.onPress }>
            { props.children }
        </TouchableOpacity>
    );
}

export { CardTouchable };
