import React from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import {
    verticalScale
} from '../../utils';

const CustomButtonFooter = (props) => {
    return (
        <TouchableOpacity
            style={[
                {
                    width: '100%', 
                    justifyContent: 'flex-end', 
                    position: 'absolute',
                    bottom: 0,
                    backgroundColor: 'red',
                    padding: verticalScale(15),
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#29ABE2'
                },
                props.containerStyle
            ]}
            onPress={props.onPress}
        >
            <Text
                style={[
                    {
                        textTransform: 'capitalize',
                        fontWeight: 'bold',
                        color: '#FFFFFF'
                    },
                    props.textStyle
                ]}
            >
                {props.title}
            </Text>
        </TouchableOpacity>
    )
}

export { CustomButtonFooter };