import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';
import { CustomIcon } from '../common';
import { verticalScale } from '../../utils';

const renderLabel = (label) => {
    return (
        <Text style={styles.labelStyle}>
            {label}
        </Text>
    )
}

const CustomCheckbox = (props) => {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.label ? renderLabel(props.label) : null}
            <TouchableOpacity 
                style={[styles.checkboxStyle, props.checkboxStyle]}
                onPress={props.onPress}
            >
                <Text>
                    {props.checked ? <CustomIcon name='check' /> : ''}
                </Text>
            </TouchableOpacity>
        </View>
     
    );
};

const styles = {
    labelStyle: {
        fontSize: verticalScale(14),
        fontWeight: 'bold',
        width: '100%',
        marginBottom: verticalScale(10)
    },
    checkboxStyle: {
        width: verticalScale(24),
        height: verticalScale(24),
        borderColor: '#333333',
        borderWidth: 1,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: verticalScale(10)
    },
    containerStyle: {

    }
}

export { CustomCheckbox };