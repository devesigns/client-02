import React from 'react';
import { StyleSheet, Text } from 'react-native';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../utils';

const styles = StyleSheet.create({
    textStyle: {
        fontSize: verticalScale(14),
        color: "#333333",
        flexDirection: 'column'
    }
});

const CustomText = (props) => {
    return (
        <Text
            style={[styles.textStyle, props.style]}
            onPress={props.onPress}
            numberOfLines={props.numberOfLines}
            ellipsizeMode={'tail'}
        >
            {props.children}
        </Text>
    );
}

export { CustomText };
