import React from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import {
    CustomImage
} from '.';

const styles = StyleSheet.create({
    standAvatar: {
        backgroundColor: '#F2F2F2'
    }
});

const CustomAvatar = (props) => {
    let borderStyle = {};

    if (props.round && props.size) {
        borderStyle = {
            borderRadius: props.size / 2
        }
    }

    return (
        <View
            style={props.containerStyle}
        >
            <CustomImage
                style={[styles.standAvatar, {
                    height: props.size ? props.size : '100%',
                    width: props.size ? props.size : '100%',
                    ...borderStyle
                }]}
                source={{ uri: props.url }}
                resizeMode='cover'
            />
        </View>
    );
}

export { CustomAvatar };
