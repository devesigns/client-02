import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { verticalScale } from '../../utils';

const CustomSelectBox = (props) => {
    if (props.checked) {
        styles.touchStyle = {
            backgroundColor: '#29ABE2'
        }
    } else {
        styles.touchStyle = {
            borderWidth: 0.5
        }
    }

    return (
        <TouchableOpacity
            // style={styles.touchStyle}
            onPress={props.onPress}
            style={[styles.containerStyle, props.style, styles.touchStyle]}
        >
            <Text style={[styles.boxStyle, props.checked ? {color: '#FFFFFF', borderWidth: 0} : null,
            props.valid ? {color: '#FF0000', borderColor: '#ff0000'}: null]}>
                {props.textDate}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    containerStyle: {
        // flexWrap: 'wrap',
        flexDirection: 'row',
        // alignSelf: 'flex-start',
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50
    },
    boxStyle: {
        fontSize: verticalScale(10),
        textAlign: 'center',
        padding: verticalScale(5),
        flex: 1,
    }
}

export { CustomSelectBox };
