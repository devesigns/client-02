import React from 'react';
import { StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    iconStyle: {
        color: '#333333',
        maxWidth: '100%'
    }
});

const CustomIcon = (props) => {
    return (
        <Icon style={[styles.iconStyle, props.style]} name={props.name} size={props.size}/>
    );
}

export { CustomIcon };
