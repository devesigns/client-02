import React, { Component } from 'react';
import {
    ActivityIndicator,
    View,
    Image
} from 'react-native';
// import FastImage from 'react-native-fast-image';
import {
    verticalScale
} from '../../utils';

class CustomImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        }
    }

    UNSAFE_componentWillMount() {
        if (!this.props.source.uri) {
            this.setState({
                loaded: true
            })
        }
    }
    
    render() {
        return (
            <View
                style={[
                    this.props.style,
                    {
                        overflow: 'hidden'
                    },
                    !this.state.loaded ?
                    {
                        backgroundColor: '#F2F2F2'
                    }
                    :
                    null
                ]}
            >
                {
                    !this.state.loaded ?
                    <View
                        style={{
                            flex: 1,
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <ActivityIndicator color='rgba(255, 255, 255, 0.8)' animating={true} />
                    </View>
                    :
                    null
                }
                <Image
                    onLoadEnd={(event) => {
                        this.setState({
                            loaded: true
                        })
                    }}
                    source={this.props.source}
                    style={{
                        height: '100%',
                        width: '100%',
                        zIndex: 999
                    }}
                    resizeMode={this.props.resizeMode}
                />
            </View>
        )
    }
}

export { CustomImage };
