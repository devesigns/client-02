import React from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { verticalScale } from '../../utils';

const renderLabel = (label) => {
    return (
        <Text style={styles.labelStyle}>
            {label}
        </Text>
    )
}

const renderIcon = (customSize) => {
    return (
        <Icon style = { styles.editIcon } name="edit" size={customSize ? customSize : 14} />
    )
}

const CustomTextInput = (props) => {
    return (
      <View style={props.containerStyle}>
        {props.label ? renderLabel(props.label) : null}
        <View style={[styles.editSection, props.editSectionStyle]}>
            <TextInput
                style={[styles.inputStyle, props.inputStyle]}
                value={props.value}
                placeholder={props.placeholder}
                secureTextEntry={props.secureTextEntry}
                onChangeText={props.onChangeText}
                editable={props.editable}
                keyboardType={props.keyboardType}
                maxLength={props.maxLength}
                multiline={props.multiline}
                autoFocus={props.autoFocus}
            />
            { props.showIcon ? renderIcon(props.inputStyle.fontSize + 2) : null }
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
    inputStyle: {
        flex: 1,
        padding: 0,
        paddingBottom: verticalScale(5)
    },
    labelStyle: {
        fontSize: verticalScale(11),
        fontWeight: 'bold',
        width: '100%',
    },
    editSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        width: '100%',
        paddingBottom: 0,
        marginBottom: 0,
        borderBottomWidth: 0.5,
        borderColor: 'rgba(33, 33, 33, 0.3)'
    },
    editIcon: {
        alignItems: 'flex-end',
        paddingTop: verticalScale(5)
    }
});

export { CustomTextInput };