import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { verticalScale } from '../../utils';

const styles = StyleSheet.create({
  cardColumn: {
    paddingLeft: verticalScale(5),
    paddingRight: verticalScale(5),
    width: '100%'
  }
});

const CardColumn = (props) => {
    return (
      <View style={[styles.cardColumn, props.style]}>
        {props.children}
      </View>
    );
}

export { CardColumn };
