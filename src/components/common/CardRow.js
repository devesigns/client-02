import React from 'react';
import { StyleSheet, View } from 'react-native';
import { verticalScale } from '../../utils';

const styles = StyleSheet.create({
  cardRow: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: verticalScale(3)
  }
});

const CardRow = (props) => {

    return (
      <View style={[styles.cardRow, props.style]}>
        {props.children}
      </View>
    );
}

export { CardRow };
