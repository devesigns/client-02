// How to style native android picker
// https://stackoverflow.com/questions/38921492/how-to-style-the-standard-react-native-android-picker/39141949#39141949
//
// ^
// It's inconvenient, so I'll just define my own custom picker

import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    CustomText,
    CustomModal,
    CustomIcon,
    CardRow,
    CustomDivider
} from '../common';
import { verticalScale } from '../../utils';

class CustomPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: '',
            selectedLabel: '',
            selectListModalVisible: false
        }
    }

    UNSAFE_componentWillMount() {
        if (this.props.value) {
            for (i = 0; i < this.props.options.length; i++) {
                if (this.props.options[i].value === this.props.value) {
                    this.setState({
                        selectedLabel: this.props.options[i].label,
                        selectedValue: this.props.value
                    });

                    break;
                }     
            }
        } else {
            for (i = 0; i < this.props.options.length; i++) {
                if (this.props.options[i].value == '') {
                    this.setState({
                        selectedLabel: this.props.options[i].label
                    });

                    break;
                }
            }
        }
    }

    renderLabel(label) {
        return (
            <Text style={styles.labelStyle}>
                {label}
            </Text>
        )
    }

    renderSelectListItem({item, index}) {
        if (!item.value) {
            return;
        }

        return (
            <TouchableOpacity
                key={index.toString()}
                style={{width: '100%'}}
                onPress={() => {
                    this.setState({
                        selectedValue: item.value,
                        selectedLabel: item.label,
                        selectListModalVisible: false
                    })
                    this.props.onValueChange(item.value);
                }}
            >
                <CardRow
                    style={{}}
                >
                    <CustomText>
                        {item.label}
                    </CustomText>
                </CardRow>
                {
                    index < this.props.options.length - 1 ?
                    <CustomDivider />
                    :
                    null
                }
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View
                style={[styles.containerStyle, this.props.containerStyle]}
            >
                <View
                    style={{paddingTop: verticalScale(5), paddingBottom: verticalScale(5), justifyContent: 'center'}}
                >
                    {this.props.label ? renderLabel(this.props.label) : null}
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                selectListModalVisible: true
                            })
                        }}
                    >
                        <CustomText
                            style={this.props.selectedLabelStyle}
                        >
                            {this.state.selectedLabel}
                        </CustomText>
                    </TouchableOpacity>
                    <CustomIcon
                        name='chevron-down'
                        size={verticalScale(14)}
                        style={{
                            position: 'absolute',
                            right: 0
                        }}
                    />
                </View>
                {
                    this.props.enabled == false ?
                    null
                    :
                    <CustomModal
                        headerText={this.props.headerText}
                        visible={this.state.selectListModalVisible}
                        onRequestClose={() => this.setState({selectListModalVisible: false})}
                        hideConfirm
                        containerStyle={{maxHeight: verticalScale(600)}}
                    >
                        {(() => {
                            return this.props.options.map((item, index) => {
                                return this.renderSelectListItem({item, index})
                            })
                        })()}
                    </CustomModal>
                }
            </View>
        )
    }
};

const styles = {
    labelStyle: {
        fontSize: verticalScale(14),
        fontWeight: 'bold',
        height: verticalScale(20)
    },
    containerStyle: {
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgba(33, 33, 33, 0.1)'
    }
};

export { CustomPicker };