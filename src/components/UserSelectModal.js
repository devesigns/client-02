import React, { Component } from 'react';
import {
    TextInput,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {
    getUserByPhone
} from '../actions';
import {
    CustomModal,
    CardRow,
    CardColumn,
    CustomDivider,
    CustomIcon,
    CustomAvatar,
    CustomText
} from './common';
import {
    verticalScale
} from '../utils';

class UserSelectModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userSelectModalVisible: false,
            newPhoneInput: ''
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            userSelectModalVisible: nextProps.visible
        })
    }

    checkIfUserIsSelected(userId) {
        if (!this.props.selectedUser) {
            return false;
        }

        if (this.props.selectedUser == userId) {
            return true;
        }

        try {
            for (i = 0; i < this.props.selectedUser.length; i++) {
                if (this.props.selectedUser[i] == userId) {
                    return true;
                }
            }
        } catch {
            return false;
        }

        return false;
    }

    checkIfUserExistsInContact(userPhone) {
        for (let i = 0; i < this.props.usersInContact.length; i++) {
            if (userPhone == this.props.usersInContact[i].user.cellphone) {
                return true;
            }
        }

        return false;
    }

    renderUserItem = ({item, index}) => {
        let user = item.user;

        return (
            <TouchableOpacity
                key={index.toString()}
                style={{width: '100%'}}
                onPress={() => {
                    this.props.onUserSelect(item);
                }}
            >
                <CardRow
                    style={{alignItems: 'center'}}
                >
                    <CardColumn
                        style={{width: '20%'}}
                    >
                        <CustomAvatar
                            size={verticalScale(50)}
                            round
                            url={user.profileImage}
                        />
                    </CardColumn>
                    <CardColumn
                        style={{width: '80%'}}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(18)}}
                        >
                            {user.fullname}
                        </CustomText>
                        <CustomText>
                            {user.cellphone}
                        </CustomText>
                    </CardColumn>
                    {
                        this.checkIfUserIsSelected(user.id) ?
                        <CustomIcon name='check' size={verticalScale(24)} style={{color: '#51FFC0', position: 'absolute', right: 0}} />
                        :
                        null
                    }
                </CardRow>
                <CustomDivider />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <CustomModal
                headerText={'Chọn người phụ trách'}
                visible={this.state.userSelectModalVisible}
                onRequestClose={this.props.onRequestClose}
                hideConfirm={this.props.hideConfirm != null ? this.props.hideConfirm : true}
                onConfirm={() => {
                    if (this.state.newPhoneInput) {
                        alert('Hãy thêm người dùng mới vào danh bạ trước!')
                    } else {
                        this.props.onConfirm();
                    }
                }}
                containerStyle={{maxHeight: verticalScale(600)}}
            >
                {(() => {
                    return this.props.usersInContact.map((item, index) => {
                        return this.renderUserItem({item, index})
                    })
                })()}
                <CardRow
                    style={{paddingBottom: verticalScale(15), alignItems: 'center'}}
                >
                    <CardColumn
                        style={{width: '20%', justifyContent: 'center'}}
                    >
                        <CustomIcon name='plus-circle' size={verticalScale(50)} style={{color: '#F2F2F2'}} />
                    </CardColumn>
                    <CardColumn
                        style={{width: '80%', justifyContent: 'center'}}
                    >
                        <TextInput
                            value={this.state.newPhoneInput}
                            placeholder={'Thêm SĐT mới...'}
                            style={{paddingLeft: 0, fontSize: verticalScale(18)}}
                            onChangeText={(value) => {
                                this.setState({
                                    newPhoneInput: value
                                })
                            }}
                            keyboardType='numeric'
                            maxLength={12}
                        />
                    </CardColumn>
                    {
                        this.state.newPhoneInput ?
                        <TouchableOpacity
                            onPress={() => {
                                if (this.checkIfUserExistsInContact(this.state.newPhoneInput)) {
                                    alert('Người dùng đã có trong danh bạ!')
                                } else {
                                    this.props.getUserByPhone(this.props.userToken, this.state.newPhoneInput);
                                }
                                
                                this.setState({
                                    newPhoneInput: ''
                                })
                            }}
                            style={{position: 'absolute', right: 0}}
                        >
                            <CustomText
                                style={{color: '#29ABE2'}}
                            >
                                Thêm
                            </CustomText>
                        </TouchableOpacity>
                        :
                        null
                    }
                </CardRow>
            </CustomModal>
        )
    }
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        usersInContact: user.usersInContact
    }
}

export default connect(mapStateToProps, {
    getUserByPhone
})(UserSelectModal);