import React from 'react';
import {
    ActivityIndicator,
    StatusBar,
    StyleSheet,
    View
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import {
    connect
} from 'react-redux';
import {
    authReducerUpdate,
    notificationRegisterFCM
} from '../../actions';

class AuthLoadingScene extends React.Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        // this._bootstrapAsync();
    }

    retrieveTCMToken = async (userToken) => {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            console.log(fcmToken);
            this.props.notificationRegisterFCM(userToken, fcmToken);
        } else {
            alert('Error: Device token not found')
        }
    }
    
    componentDidMount() {
        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh((fcmToken) => {
            // Update new token to server on refresh
            this.props.notificationRegisterFCM(this.props.userToken, fcmToken);
        });

        this._bootstrapAsync();
    }

    // Fetch the token from storage then navigate to our appropriate place
    _bootstrapAsync = async () => {
        const tokenButler = await AsyncStorage.getItem('tokenButler');
        
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        if (tokenButler) {
            console.log('User token', tokenButler);
            this.props.authReducerUpdate('userToken', tokenButler);
            this.retrieveTCMToken(tokenButler);
            this.props.navigation.navigate('Dashboard');
        } else {
            this.props.navigation.navigate('Auth');
        }
    };

    // Render any loading content that you like here
        render() {
            return (
                <View style={styles.container}>
                    <ActivityIndicator />
                    <StatusBar barStyle="default" />
                </View>
            );
        }
    }

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.token.userToken
    }
}

export default connect(null, {
    authReducerUpdate,
    notificationRegisterFCM
})(AuthLoadingScene);
