import _ from 'lodash';
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ScrollView,
    Button,
    TextInput,
    TouchableHighlight,
    Alert,
    KeyboardAvoidingView,
    Platform
} from 'react-native';
import {
    Card,
    CardRow,
    CardColumn,
    CustomIcon,
    CustomText,
    CustomDivider,
    CustomStatusBar,
    CustomImage
} from '../common';
import { connect } from 'react-redux';
import {
    login,
    certificate,
    verifyPhone,
    registerAccount,
    authReducerUpdate,
    resetPassword,
    checkUser,
} from '../../actions';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // position: 'absolute',
        // bottom: 0
    },
    columnFirst: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    columnSecond: {
        width: '90%',
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingVertical: verticalScale(10),
        flexDirection: 'row'
    },
    signupText: {   
        color: '#29ABE2'
    },
    iconSize: verticalScale(24),
    textInputStyle: {
        paddingVertical: verticalScale(5),
        fontSize: verticalScale(14)
    },
    errorMessage: {
        color: "#ff0000",
        fontSize: verticalScale(14)
    },
});


class LoginScene extends Component {

    constructor (props) {
        super(props);
        this.state = {
            name: '',
            nameValid: true,
            phone: '',
            phoneValid: true,
            password: '',
            passwordValid: true,
            confirmPass: '',
            confirmPassValid: true,
            contentType: 'sign-in',
            codeVerify: '',
            codeVerifyValid: true
        }
    }

    UNSAFE_componentWillMount() {

    }

    checkValidForm = () => {
        let arrCheck = [
            this.state.name,
            this.state.phone,
            this.state.password,
            this.state.confirmPass
        ]
        let count = 0;
        for (let i = 0; i < arrCheck.length; i++) {
            if (arrCheck[i] === ''){
                switch (i) {
                    case 0: this.setState({nameValid: false});
                        break;
                    case 1: this.setState({phoneValid: false});
                        break;
                    case 2: this.setState({passwordValid: false});
                        break;
                    case 3: this.setState({confirmPassValid: false});
                        break;
                    default: break;
                }
            } else {
                count++;
            }
        }
        if (count === 4) {
            if (this.state.phone) {
                this.props.checkUser(this.state.phone);
            } 
        }
       
    }


    renderSignInContent() {
        return (
            <View>
                <Card
                    style={{
                        paddingTop: verticalScale(5),
                        paddingBottom: verticalScale(5),
                    }}
                >
                    <CardRow
                        style={{
                            paddingBottom: 0
                        }}
                    >
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="mobile" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Số điện thoại"
                                autoCapitalize="none"
                                onSubmitEditing={(event) => this.refs.passwordInput.focus()}
                                keyboardType="numeric"
                                returnKeyType={"next"}
                                onChangeText={(phone) =>
                                    this.setState({ phone : phone, phoneValid: true })
                                }
                            />
                            {this.state.phoneValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập số điện thoại
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CustomDivider 
                        containerStyle={{marginTop: 0, marginBottom: verticalScale(5)}}
                    />
                    <CardRow
                        style={{
                            paddingBottom: 0
                        }}
                    >
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="lock" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn 
                            style={{
                                width: '60%'
                            }}
                        >
                            <TextInput style={styles.textInputStyle}
                                ref='passwordInput'
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Mật khẩu"
                                // returnKeyType="go"
                                // ref={(input) => this.passwordInput = input}
                                secureTextEntry
                                onChangeText={(password) => 
                                    this.setState({ password : password, passwordValid: true })
                                }
                            />
                            {this.state.passwordValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập mật khẩu
                                </CustomText>
                            ) : null}
                        </CardColumn>
                        <CardColumn style={{width: '30%', justifyContent: 'center'}}>
                            <CustomText
                                style={{color : '#FF8383', alignItems: 'center', textAlign: 'right', fontSize: verticalScale(13) }}
                                onPress={() => this.setState({contentType: 'input-phone'})}
                            >
                                Quên mật khẩu?
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                </Card>
                <Card style={{ padding: 0 }}>
                    <Button title="Đăng nhập" onPress={() => this.signInAsync()} />
                </Card>
                <View style={styles.signupTextCont}>
                    <CustomText 
                        
                    >
                        Chưa có tài khoản?
                    </CustomText>
                    <TouchableHighlight onPress={() =>  this.setState({contentType: 'register'})}>
                        <CustomText style={styles.signupText}>
                            &nbsp;Đăng ký ngay.
                        </CustomText>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    renderSignUpContent() {
        return (
            <View>
                <Card>
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="user" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Tên liên hệ"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onChangeText={(name) => this.setState({ name: name, nameValid : true })}
                            />
                            {this.state.nameValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập tên đăng nhập
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CustomDivider 
                        containerStyle={{marginTop: 0, marginBottom: verticalScale(5)}}
                    />
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="mobile" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Số điện thoại"
                                autoCapitalize="none"
                                onSubmitEditing={() => this.passwordInput.focus()}
                                keyboardType="numeric"
                                returnKeyType="next"
                                onChangeText={(phone) =>
                                    this.setState({ phone: phone, phoneValid: true })
                                }
                            />
                            {this.state.phoneValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập số điện thoại
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CustomDivider 
                        containerStyle={{marginTop: 0, marginBottom: verticalScale(5)}}
                    />
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="lock" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Mật khẩu"
                                returnKeyType="go"
                                ref={(input) => this.passwordInput = input}
                                secureTextEntry
                                onChangeText={(password) => 
                                    this.setState({ password: password, passwordValid: true })
                                }
                            />
                            {this.state.passwordValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập mật khẩu
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="lock" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Xác nhận mật khẩu"
                                secureTextEntry
                                onChangeText={(confirmPass) =>
                                    this.setState({confirmPass : confirmPass, confirmPassValid: true})
                                }
                            />
                            {this.state.confirmPassValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    {this.state.confirmPass === ''? 
                                    '* Vui lòng nhập lại mật khẩu':
                                    '* Nhập lại mật khẩu chưa chính xác'}
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CardRow style={{ }}>
                        <CardColumn style={{ width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                            <CustomText  style={{color: '#1E90FF'}}
                            >
                                Khi đăng ký, hãy chắc chắn rằng bạn đã đồng ý với các
                            </CustomText>
                            <CustomText
                                style={{ fontWeight: 'bold', color: '#1E90FF' }}
                            >
                                điều khoản và chính sách của myButler.
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                </Card>
                <Card style={{ padding: 0 }}>
                    <Button title="Đăng ký" onPress={() => 
                        this.checkValidForm()
                    } />
                </Card>
                <View style={styles.signupTextCont}>
                    <TouchableHighlight onPress={() =>  this.setState({contentType: 'sign-in'})}>
                        <CustomText style={styles.signupText}>
                            Quay lại trang đăng nhập.
                         </CustomText>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    renderVerifyContent() {
        return (
            <View>
                <Card>
                    <CardRow>
                        <CardColumn style={{ width: '100%' }}>
                            <CustomText style={styles.headerStyle}>
                                Chào mừng bạn đến với myButler!
                                    </CustomText>
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={{ width: '100%' }}>
                            <CustomText>
                                Chúng tôi đã gửi cho bạn một tin nhắn SMS
                                chứa mã số xác nhận số điện thoại. Chỉ cần xác nhận
                                xong là việc đăng ký sẽ hoàn tất!
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={{ width: '30%' }} />
                        <CardColumn style={{ width: '40%' }}>
                            <TextInput style={styles.textInputStyle}
                                placeholder="0000"
                                autoCapitalize="none"
                                keyboardType="numeric"
                                returnKeyType="next"
                                maxLength={4}
                                onChangeText={(value) =>
                                    this.setState({ codeVerify: value })
                                }
                                value={this.state.codeVerify}
                                letterSpacing={20}
                            />
                        </CardColumn>
                        <CardColumn style={{ width: '30%' }} />
                    </CardRow>
                </Card>
                <Card style={{ padding: 0 }}>
                    <Button title="Xác nhận" onPress={() => this.confirmCode()} />
                </Card>
                <View style={[styles.signupTextCont, {flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}]}>
                    <View
                        style={{flexDirection: 'row'}}
                    >
                        <CustomText style={styles.textStyle}>
                            Chưa nhận được mã xác nhận?&nbsp;
                        </CustomText>
                        <TouchableHighlight onPress={() => this.props.certificate(this.state.phone)}>
                            <CustomText style={styles.signupText}>
                                Gửi lại.
                            </CustomText>
                        </TouchableHighlight>
                    </View>
                    <TouchableHighlight 
                        onPress={() => this.setState({contentType: 'sign-in'})}
                        style={{paddingTop: verticalScale(5)}}
                    >
                        <CustomText style={styles.signupText}>
                            Quay lại trang đăng nhập.
                         </CustomText>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    renderPhoneContent() {
        return(
            <View>
                <Card
                    style={{paddingTop: verticalScale(5), paddingBottom: verticalScale(5)}}
                >
                    <CardRow
                        style={{paddingBottom: 0}}
                    >
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="mobile" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Số điện thoại"
                                autoCapitalize="none"
                                keyboardType="numeric"
                                returnKeyType={"next"}
                                onChangeText={(phone) =>
                                     this.setState({ phone : phone, phoneValid: true })
                                }
                            />
                            {this.state.phoneValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập số điện thoại
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={{ width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                            <CustomText style={{color: '#1E90FF'}}>
                                Vui lòng nhập số điện thoại để đổi lại mật khẩu.
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                </Card>
                <Card style={{ padding: 0 }}>
                    <Button title="Gửi mã"
                        onPress={() => 
                            this.sendCodeReset()
                        } 
                    />
                </Card>
                <View style={styles.signupTextCont}>
                    <TouchableHighlight onPress={() =>  this.setState({contentType: 'sign-in'})}>
                        <CustomText style={styles.signupText}>
                            Quay về trang đăng nhập
                         </CustomText>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    renderResetPassContent() {
        return (
            <View>
                <Card>
                    <CardRow>
                        <CardColumn>
                            <CustomText style={{color: '#1E90FF', textAlign: 'center'}}>
                                Nhập mã xác nhận
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn>
                            <TextInput style={{textAlign: 'center'}}
                                placeholder="0000"
                                autoCapitalize="none"
                                keyboardType="numeric"
                                maxLength={4}
                                value={this.state.codeVerify}
                                letterSpacing={20}
                                onChangeText={(codeVerify) => 
                                    this.setState({ codeVerify: codeVerify, codeVerifyValid : true })
                                }
                            />
                            {this.state.codeVerifyValid == false ? (
                                <CustomText style={{color : '#FF8383', textAlign: 'center'}}>
                                    * Vui lòng nhập mã xác nhận
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CustomDivider 
                        containerStyle={{marginTop: 0, marginBottom: verticalScale(5)}}
                    />
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="lock" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Mật khẩu mới"
                                secureTextEntry
                                value={this.state.password}
                                onChangeText={(password) => 
                                    this.setState({ password: password, passwordValid: true })
                                }
                            />
                            {this.state.passwordValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    * Vui lòng nhập mật khẩu mới
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="lock" size={styles.iconSize} />
                        </CardColumn>
                        <CardColumn style={styles.columnSecond}>
                            <TextInput style={styles.textInputStyle}
                                underlineColorAndroid="rgba(0,0,0,0)"
                                placeholder="Xác nhận mật khẩu mới"
                                secureTextEntry
                                value={this.state.confirmPass}
                                onChangeText={(confirmPass) =>
                                    this.setState({confirmPass : confirmPass, confirmPassValid: true})
                                }
                            />
                            {this.state.confirmPassValid == false ? (
                                <CustomText style={styles.errorMessage}>
                                    {this.state.confirmPass === ''? 
                                    '* Vui lòng nhập lại mật khẩu mới':
                                    '* Mật khẩu mới chưa chính xác'}
                                </CustomText>
                            ) : null}
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        <CardColumn style={{ width: '100%', justifyContent: 'center', alignItems: 'center'}}>
                            <CustomText style={{color: '#1E90FF', textAlign: 'center'}}
                            >
                                Chúng tôi sẽ gửi mã xác nhận cho bạn, vui lòng nhập mã và mật khẩu mới để đổi mật khẩu.
                            </CustomText>
                        </CardColumn>
                    </CardRow>
                </Card>
                <Card style={{ padding: 0 }}>
                    <Button title="Đổi mật khẩu" onPress={() => 
                        this.resetPassword()
                    } />
                </Card>
                <View style={styles.signupTextCont}>
                    <TouchableHighlight onPress={() =>  this.setState({contentType: 'sign-in'})}>
                        <CustomText style={styles.signupText}>
                            Quay lại trang đăng nhập.
                         </CustomText>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    renderModalContent() {
        if (this.state.contentType == 'sign-in') {
            return (
                <View>
                    {this.renderSignInContent()}
                </View>
            )
        } else if (this.state.contentType == 'register') {
            return (
                <View>
                    {this.renderSignUpContent()}
                </View>
            )
        } else if (this.state.contentType == 'verify') {
            return (
                <View>
                    {this.renderVerifyContent()}
                </View>
            )
        } else if (this.state.contentType == 'input-phone') {
            return (
                <View>
                    {this.renderPhoneContent()}
                </View>
            )
        } else if (this.state.contentType == 'reset-password') {
            return (
                <View>
                    {this.renderResetPassContent()}
                </View>
            )
        }
    }

    renderNotification = (message) => {
        Alert.alert(
            'Thông báo',
            message,
            [{text: "OK"}],
            { cancelable: true });
    }
    
    UNSAFE_componentWillReceiveProps(nextProps) {
        // case login
        if (!_.isEmpty(nextProps.token.loginStatus)) {
            if (nextProps.token.loginStatus.code == 200) {
                this.props.navigation.navigate('Dashboard');
            } else if (nextProps.token.loginStatus.code == 400) {
                this.renderNotification('Đăng nhập thất bại, vui lòng kiểm tra lại thông tin.');
            }

            this.props.authReducerUpdate('loginStatus', {});
            return;
        }
        
        if (!_.isEmpty(nextProps.token.certificateStatus)) {
            if (nextProps.token.certificateStatus.code == 200) {
                if(this.state.contentType === 'register') {
                    // case register
                    this.setState({contentType: 'verify'});
                } else if (this.state.contentType === 'input-phone') {
                    this.setState({contentType:'reset-password'});
                }
            } else if (nextProps.token.certificateStatus.code == 400) {
                this.setState({codeVerify: ''})
                this.renderNotification(
                    'Gửi mã xác nhận không thành công.'
                    );
            }
        
            this.props.authReducerUpdate('certificateStatus', {});
            return;
        }
        
        if (!_.isEmpty(nextProps.token.verifyStatus)) {
            if (nextProps.token.verifyStatus.code == 200) {
                this.props.registerAccount(this.state.name, this.state.phone, this.state.password);
            } else if (nextProps.token.verifyStatus.code == 400) {
                this.renderNotification('Mã xác nhận không đúng.');
                this.setState({
                    codeVerify: ''
                })
            }

            this.props.authReducerUpdate('verifyStatus', {});
            return;
        }

        if (!_.isEmpty(nextProps.token.resetPassStatus)) {
            if (nextProps.token.resetPassStatus.code == 200) {
                this.setState({contentType: 'sign-in'})
                this.renderNotification('Đổi mật khẩu thành công, vui lòng đăng nhập lại.');
            } else if (nextProps.token.resetPassStatus.code == 400) {
                this.renderNotification('Mã xác nhận không đúng, vui lòng điền lại thông tin.');
                this.setState({
                    codeVerify: '',
                    password: '',
                    confirmPass: ''
                })
            }

            this.props.authReducerUpdate('resetPassStatus', {});
            return;
        }

        if (!_.isEmpty(nextProps.token.isExists)) {
            if (nextProps.token.isExists.code == 200) {
                this.renderNotification('Số điện thoại này đã được đăng ký, vui lòng nhập số khác');
            } else if (nextProps.token.isExists.code == 400) {
                this.signUpAsync();              
            }

            this.props.authReducerUpdate('isExists', {});
            return;
        }

        if (!_.isEmpty(nextProps.token.registerStatus)) {
            if (nextProps.token.registerStatus.code == 200) {
                this.props.login(this.state.phone, this.state.password);
            } else if (nextProps.token.registerStatus.code == 400) {
                this.renderNotification('Có lỗi xảy ra trong quá trình đăng ký, vui lòng thử lại.');
            }

            this.props.authReducerUpdate('registerStatus', {});
            return;
        }
    }
  
    signInAsync = () => {
        if(this.state.phone === '' || this.state.password  === '') {
            if (this.state.phone === '') {
                this.setState({phoneValid: false});
            }
            if (this.state.password  === '') {
                this.setState({passwordValid: false});
            }
        } else {
            this.props.login(this.state.phone, this.state.password);
        }
    };

    signUpAsync = () => {
        if (this.state.password !== this.state.confirmPass) {
            this.setState({confirmPassValid: false})      
        } else {
            if (this.state.password.length < 4) {
                alert('Mật khẩu phải có ít nhất 4 ký tự!')
            } else {
                this.props.certificate(this.state.phone);
            }
        }
 
    }

    confirmCode = () => {
        const code = this.state.codeVerify;
        this.props.verifyPhone(this.state.phone, code);
    }

    sendCodeReset = () => {
        if (this.state.phone === '') {
            this.setState({phoneValid: false})
        } else {
            this.props.certificate(this.state.phone);
        }
    }

    resetPassword = () => {
        if (this.state.codeVerify === '' || this.state.password === '' || this.state.confirmPass === '') {
            if (this.state.codeVerify === '') {
                this.setState({codeVerifyValid: false});
            } 
            if (this.state.password === '') {
                this.setState({passwordValid: false});
            }
            if (this.state.confirmPass === '') {
                this.setState({confirmPassValid: false});
            }
        } else {
            if (this.state.password !==  this.state.confirmPass) {
                this.setState({confirmPassValid: false});
            } else {
                this.props.resetPassword(this.state.codeVerify, this.state.phone, this.state.password);
            }
        }
    }

    render() {
        return (
            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.select({ios: 'padding', android: null})}
                keyboardVerticalOffset={0}
            >
                <CustomStatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
                <CustomImage
                    style={{
                        width: '100%', 
                        height: '100%',
                        position: 'absolute',
                        top: 0
                    }}
                    source={require('../../../assets/img/auth.png')}
                    resizeMode={'cover'}
                />
                <View
                    style={{
                        width: '100%',
                        height: verticalScale(55),
                        backgroundColor: '#FFFFFF',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <CustomImage
                        style={{width: verticalScale(100), height: verticalScale(55)}}
                        source={require('../../../assets/img/logo.png')}
                        resizeMode={'contain'}
                    />
                </View>
                {/* This is good */}
                {/* https://stackoverflow.com/questions/52539528/how-to-position-a-view-at-the-bottom-of-a-scrollview */}
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1,
                        flexDirection: 'column'
                    }}
                >
                    <View
                        style={{
                            marginTop: 'auto'
                        }}
                    >
                        {this.renderModalContent()}
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        token: auth.token,
    }
}

export default connect(mapStateToProps, {
    login,
    certificate,
    verifyPhone,
    registerAccount,
    authReducerUpdate,
    resetPassword,
    checkUser,
})(LoginScene);