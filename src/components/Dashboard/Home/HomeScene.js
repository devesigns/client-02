import React, { Component } from 'react';
import {
    View
} from 'react-native';
import {
    CustomText,
    Card,
    CardTouchable,
    CustomStatusBar,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    assignmentInvitationFetch,
    usersInContactFetch,
    getUserProfile,
    // notificationFetch
} from '../../../actions';
import {
    verticalScale,
} from '../../../utils';

class HomeScene extends Component {
    static navigationOptions = {
        header: null
    }

    UNSAFE_componentWillMount() {
        this.props.usersInContactFetch(this.props.userToken);
        this.props.getUserProfile(this.props.userToken);
        
        this.props.assignmentInvitationFetch(this.props.userToken);
        // this.props.notificationFetch(this.props.userToken);
        setInterval(() => {
            this.props.assignmentInvitationFetch(this.props.userToken);
            // this.props.notificationFetch(this.props.userToken);
        }, 30000);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.usersInContact.length - this.props.usersInContact.length == 50) {
            this.props.usersInContactFetch(this.props.userToken, nextProps.usersInContact.length);
        }
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <CustomStatusBar backgroundColor="#FFFFFF" barStyle="dark-content" />
                <View
                    style={{width: '100%', height: verticalScale(55), backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center'}}
                >
                    <CustomImage
                        style={{width: verticalScale(100), height: verticalScale(55)}}
                        source={require('../../../../assets/img/logo.png')}
                        resizeMode={'contain'}
                    />
                </View>
                <View
                    style={{justifyContent: 'center', alignItems: 'center', flex: 1}}
                >
                    <Card
                        style={{
                            borderRadius: 5,
                            backgroundColor: '#29ABE2',
                            width: '80%',
                            paddingTop: verticalScale(30),
                            paddingBottom: verticalScale(30),
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <CustomText
                            style={{fontWeight: 'bold', fontSize: verticalScale(16), color: '#FFFFFF'}}
                        >
                            Xin chào!
                        </CustomText>
                        <CustomText
                            style={{fontWeight: 'bold', fontSize: verticalScale(16), color: '#FFFFFF'}}
                        >
                            Tôi có thể giúp gì cho bạn?
                        </CustomText>
                    </Card>
                    <CustomImage 
                        style={{width: '60%', height: '50%'}}
                        source={require('../../../../assets/img/home-illustration.png')}
                        resizeMode={'contain'}
                    />
                    <View
                        style={{width: '80%', flexDirection: 'row', justifyContent: 'space-between'}}
                    >
                        <CardTouchable
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate('Chore');
                            }}
                        >
                            <CustomText
                                style={{textAlign: 'center'}}
                            >
                                Bạn có thể lên kế hoạch làm việc nhà
                            </CustomText>
                        </CardTouchable>
                        <CardTouchable
                            style={styles.buttonStyle}
                            onPress={() => {
                                this.props.navigation.navigate('Service');
                            }}
                        >
                            <CustomText
                                style={{textAlign: 'center'}}
                            >
                                Hoặc thuê dịch vụ hỗ trợ
                            </CustomText>
                        </CardTouchable>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    buttonStyle: {
        width: '49%',
        paddingTop: verticalScale(20),
        paddingBottom: verticalScale(20),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: verticalScale(5),
        marginLeft: 0,
        marginRight: 0,
        marginTop: verticalScale(-5),
        marginBottom: 0
    }
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        usersInContact: user.usersInContact
    }
}

export default connect(mapStateToProps, {
    assignmentInvitationFetch,
    usersInContactFetch,
    getUserProfile,
    // notificationFetch
})(HomeScene);