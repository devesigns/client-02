import React, { Component } from 'react';
import { 
    ScrollView,
    View,
    TextInput,
    TouchableOpacity
} from 'react-native';
import {
    CustomText,
    Card,
    CardRow,
    CardColumn,
    CustomTextInput,
    CardTouchable,
    CustomIcon,
    CustomDivider,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';

class AssignmentHouseViewScene extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenTitle')
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            houseForm: { ...this.props.houseForm }
        }
    }

    UNSAFE_componentWillMount() {
        this.props.navigation.setParams({
            screenTitle: this.props.houseForm.name
        })
    }

    renderImage() {
        return (
            <TouchableOpacity
                
            >
                <CustomImage
                    style={{width: '100%', height: verticalScale(200)}}
                    source={{uri: this.state.houseForm.image}}
                />
            </TouchableOpacity> 
        )
    }

    renderImagePlaceholder() {
        return (
            <View style={{height: verticalScale(200), width: '100%', backgroundColor: '#F2F2F2', justifyContent: 'center', alignItems: 'center'}}>
                <CustomText
                    style={{fontWeight: 'bold'}}
                >
                    Không có hình ảnh minh họa.
                </CustomText>
            </View>
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    {/* First part of the form, house img and name */}
                    <View
                        style={styles.headerContainerStyle}
                    >
                        {this.state.houseForm.image ? this.renderImage() : this.renderImagePlaceholder()}
                        <CardRow
                            style={{padding: verticalScale(10)}}
                        >
                            <CustomTextInput
                                placeholder="Tên nhà..."
                                inputStyle={{fontSize: verticalScale(18), fontWeight: 'bold', color: '#333333'}}
                                containerStyle={{width: '100%'}}
                                value={this.state.houseForm.name}
                                editable={false}
                            />
                        </CardRow>
                    </View>
                    {/* Second part, a card that contains  address, administrators and note */}
                    <Card
                        style={{paddingTop: 0, paddingBottom: 0}}
                    >
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center'}} name="map-marker" size={verticalScale(30)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    placeholder={'Địa chỉ nhà'}
                                    value={this.state.houseForm.address}
                                    maxLength={150}
                                    multiline
                                    editable={false}
                                    style={styles.textInputStyle}
                                />
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginBottom: 0, marginTop: 0}} />
                        <CardRow
                            style={[styles.cardRowStyle, {borderBottomWidth: 0}]}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{ textAlign: 'center'}} name="pencil" size={verticalScale(20)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    placeholder={'Ghi chú'}
                                    value={this.state.houseForm.note}
                                    editable={false}
                                    style={styles.textInputStyle}
                                />
                            </CardColumn>
                        </CardRow>
                    </Card>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerContainerStyle: {
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    columnFirst: {
        width: '10%'
    },
    columnSecond: {
        width: '90%'
    },
    cardRowStyle: {
        alignItems: 'center',
        paddingTop: verticalScale(5),
        paddingBottom: verticalScale(5)
    },
    textInputStyle: {
        color: '#333333',
        fontSize: verticalScale(14),
        padding: 0
    }
}

const mapStateToProps = ({ assignment }) => {
    return {
        houseForm: assignment.houseForm
    }
}

export default connect(mapStateToProps, {

})(AssignmentHouseViewScene);