import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TouchableWithoutFeedback,
    FlatList,
    TouchableOpacity,
    RefreshControl,
    LayoutAnimation,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import {
    Card,
    CardRow,
    CardColumn,
    CustomCheckbox,
    CustomText,
    CustomIcon,
    CustomStatusBar,
    CustomImage
} from '../../common';
import { 
    assignmentFetchFullSingle,
    assignmentReducerUpdate,
    assignmentSingleChoreStatusUpdate,
    assignmentRoomStatusUpdate,
    assignmentManagementCancel,
    houseReducerUpdate,
    notificationLoadingStatusUpdate
} from '../../../actions';
import {
    verticalScale,
    formatDate
} from '../../../utils';
import {
    slideAnimationConfig,
    fadeAnimationConfig
} from '../../../const';

class DateContentCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expandControlRoomList: { ...this.props.expandControlRoomList }
        }
    }

    UNSAFE_componentWillMount() {
        // LayoutAnimation.configureNext(fadeAnimationConfig);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            expandControlRoomList: { ...nextProps.expandControlRoomList }
        })
    }

    renderRoomChoreItem = ({item, index}) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.onRoomChoreClick(item)}
            >
                <CardRow
                    style={{
                        paddingTop: verticalScale(5),
                        alignItems: 'center'
                    }}
                >
                    <View
                        style={{
                            paddingRight: verticalScale(5)
                        }}
                    >
                        <CustomCheckbox
                            checked={item.status == 'done'}
                            onPress={() => {
                                let alertTitle = 'Xác nhận hoàn thành';
                                let alertDesc = 'Bạn xác nhận hoàn thành công việc này?';

                                if (item.status == 'done') {
                                    alertTitle = 'Xác nhận hủy hoàn thành';
                                    alertDesc = 'Bạn xác nhận hủy hoàn thành công việc này?';
                                }

                                Alert.alert(
                                    alertTitle,
                                    alertDesc,
                                    [
                                        {
                                            text: 'Quay lại',
                                            onPress: () => console.log('Cancel Pressed'),
                                            style: 'cancel',
                                        },
                                        {
                                            text: 'Xác nhận',
                                            onPress: () => this.props.onChoreCompleteCheckboxClick(item, index)
                                        },
                                    ],
                                    {
                                        cancelable: false
                                    },
                                );
                            }}
                        />
                    </View>
                    <View
                        style={{
                            flex: 1
                        }}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(14)}}
                        >
                            {item.name}
                        </CustomText>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                    >
                        {
                            item.note || item.images.length > 0 ?
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="exclamation" size={verticalScale(19)} style={{color: '#FF8383'}} />
                            </View>
                            :
                            null
                        }
                        {
                            item.priority ?
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="flag" size={verticalScale(17)} style={{color: '#FF8383'}} />
                                </View>
                            :
                            null
                        }
                    </View>
                </CardRow>
            </TouchableOpacity>
        )
    }

    renderRoomItem = ({item, index}) => {
        let prioritizedChores = [];
        let normalChores = [];

        for (i in item.chores) {
            if (item.chores[i].priority) {
                prioritizedChores.push(item.chores[i]);
            } else {
                normalChores.push(item.chores[i]);
            }
        }

        let sortedChores = [ ...prioritizedChores, ...normalChores ];

        return (
            <CardRow
                style={{
                    alignItems: 'center'
                }}
            >
                <View
                    style={{
                        flexDirection: 'column',
                        width: '100%'
                    }}
                >
                    <TouchableOpacity
                        style={{
                            width: '100%',
                            marginBottom: verticalScale(5),
                            flexDirection: 'row',
                            paddingTop: verticalScale(5),
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            this.props.onRoomExpandToggle(item.id);
                        }}
                    >
                        <View
                            style={{
                                paddingRight: verticalScale(5),
                                fontWeight: 'bold'
                            }}
                        >
                            <CustomCheckbox
                                checked={item.completed}
                                onPress={() => {
                                    let alertTitle = 'Xác nhận hoàn thành';
                                    let alertDesc = 'Bạn xác nhận hoàn thành toàn bộ công việc trong phòng này?';

                                    if (item.completed) {
                                        alertTitle = 'Xác nhận hủy hoàn thành';
                                        alertDesc = 'Bạn xác nhận hủy hoàn thành toàn bộ công việc trong phòng này?';
                                    }

                                    Alert.alert(
                                        alertTitle,
                                        alertDesc,
                                        [
                                            {
                                                text: 'Quay lại',
                                                onPress: () => console.log('Cancel Pressed'),
                                                style: 'cancel',
                                            },
                                            {
                                                text: 'Xác nhận',
                                                onPress: () => this.props.onRoomCompleteCheckboxClick(item, index)
                                            },
                                        ],
                                        {
                                            cancelable: false
                                        },
                                    );
                                }}
                            />
                        </View>
                        <View
                            style={{
                                flex: 1
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold'
                                }}
                            >
                                {item.name}
                            </CustomText>
                        </View>
                        {
                            this.state.expandControlRoomList[item.id].expanded ?
                            <CustomIcon
                                name='minus-circle'
                                size={verticalScale(24)}
                            />
                            :
                            <CustomIcon
                                name='plus-circle'
                                size={verticalScale(24)}
                            />
                        }
                    </TouchableOpacity>
                    {
                        this.state.expandControlRoomList[item.id].expanded ?
                        <FlatList
                            data={sortedChores}
                            renderItem={this.renderRoomChoreItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        :
                        null
                    }
                </View>
            </CardRow>
        )
    }

    render() {        
        item = this.props.item;

        return (
            <Card>
                <CustomText
                    style={{
                        marginTop: verticalScale(5),
                        marginBottom: verticalScale(5),
                        color: 'rgba(33, 33, 33, 0.3)',
                        fontSize: verticalScale(14)
                    }}
                >
                    {item.date}
                </CustomText>
                {
                    item.rooms.length > 0 ?
                    <FlatList
                        data={item.rooms}
                        renderItem={this.renderRoomItem}
                        extraData={this.state.expandControlRoomList}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    :
                    <CustomText>
                        Không có công việc.
                    </CustomText>
                }           
            </Card>
        )
    }
}

class AssignmentAssignedScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('sceneTitle')
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            viewMode: 'daily',
            filteredHouseForm: {},
            refreshing: false,
            // An object holding all id of displayed rooms
            // From there we will decide whether one room is expanded or not
            expandControlRoomList: {}
        }

        this.houseIdList = [];
    }

    UNSAFE_componentWillMount() {
        this.props.assignmentFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
    }

    setViewMode(viewMode) {
        if (this.state.viewMode == viewMode) {
            return;
        }

        this.setState({
            viewMode
        }, () => {
            this.props.notificationLoadingStatusUpdate(true);
            this.updateHouseData(this.props.houseForm);
        });
    }

    updateHouseData(houseData) {
        expandControlRoomList = { ...this.state.expandControlRoomList };

        if (_.isEmpty(houseData)) {
            this.props.notificationLoadingStatusUpdate(false);
            return;
        }

        if (this.state.viewMode == 'daily') {
            date = new Date();

            newRooms = {};
            filteredRooms = [];

            for (i = 0; i < houseData.rooms.length; i++) {
                filteredChores = [];

                for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                    if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                        // Do sth when the due time of weekly and monthly chores
                        // equals to the current date

                        if (houseData.rooms[i].chores[j].repeatList.indexOf(date.getDay()) > -1 || houseData.rooms[i].chores[j].repeatList.indexOf(date.getDate()) > -1) {
                            // If today is included in weekly or monthly repeat
                            // do nothing
                        } else {
                            // Else continue
                            continue;
                        }
                    }

                    filteredChores.push({
                        arrayId: j,
                        roomArrayId: i,
                        ...houseData.rooms[i].chores[j]
                    });
                }

                if (filteredChores.length < 1) {
                    continue;
                }

                filteredRooms.push({
                    arrayId: i,
                    ...houseData.rooms[i],
                    chores: filteredChores
                })
            }

            if (filteredRooms.length > 0) {
                dateKey = formatDate(date);

                newRooms[dateKey] = filteredRooms;
                expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                for (i = 0; i < filteredRooms.length; i++) {
                    if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                        // Do nothing
                    } else {
                        expandControlRoomList[dateKey][filteredRooms[i].id] = {
                            expanded: false
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        } else if (this.state.viewMode == 'weekly') {
            date = new Date();
            day = date.getDay();

            newRooms = {};

            for (t = day + 1; t < 7; t++) {
                filteredRooms = [];

                for (i = 0; i < houseData.rooms.length; i++) {
                    filteredChores = [];

                    for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                        if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                            if (houseData.rooms[i].chores[j].repeat == 'daily') {
                                // If it's daily chore, nothing to do here, continue
                                continue;
                            }

                            // If it's monthly chore, check if any due date
                            // equals the currently processed day
                            // Notice here that:
                            // getDate returns (1 - 31)
                            // getDay returns (0 - 6)
                            // Pretty BS but oh well
                            tDate = date.getDate() + (t - day);
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(tDate) > -1) {
                                filteredChores.push({
                                    arrayId: j,
                                    roomArrayId: i,
                                    ...houseData.rooms[i].chores[j]
                                });
                            }
                        } else {
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(t) > -1) {
                                filteredChores.push({
                                    arrayId: j,
                                    roomArrayId: i,
                                    ...houseData.rooms[i].chores[j]
                                });
                            }
                        }     
                    }

                    if (filteredChores.length < 1) {
                        continue;
                    }

                    filteredRooms.push({
                        arrayId: i,
                        ...houseData.rooms[i],
                        chores: filteredChores
                    })
                }

                if (filteredRooms.length > 0) {
                    let dateClone = new Date(date.getTime());
                    dateClone.setDate(dateClone.getDate() + (t - day));
                    dateKey = formatDate(dateClone);

                    newRooms[dateKey] = filteredRooms;
                    expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                    for (i = 0; i < filteredRooms.length; i++) {
                        if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                            // Do nothing
                        } else {
                            expandControlRoomList[dateKey][filteredRooms[i].id] = {
                                expanded: false
                            }
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        } else {
            date = new Date();
            day = date.getDay();
            dateInMonth = date.getDate();

            newRooms = {};

            for (t = dateInMonth + (6 - day) + 1; t < 32; t++) {
                filteredRooms = [];

                for (i = 0; i < houseData.rooms.length; i++) {
                    filteredChores = [];

                    for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                        if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                            // If it's daily or weekly chore, nothing to do here, continue
                            continue;
                        } else {
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(t) > -1) {
                                filteredChores.push({
                                    arrayId: j,
                                    roomArrayId: i,
                                    ...houseData.rooms[i].chores[j]
                                });
                            }
                        }     
                    }

                    if (filteredChores.length < 1) {
                        continue;
                    }

                    filteredRooms.push({
                        arrayId: i,
                        ...houseData.rooms[i],
                        chores: filteredChores
                    })
                }

                if (filteredRooms.length > 0) {
                    let dateClone = new Date(date.getTime());
                    dateClone.setDate(t);
                    dateKey = formatDate(dateClone);

                    newRooms[dateKey] = filteredRooms;
                    expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                    for (i = 0; i < filteredRooms.length; i++) {
                        if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                            // Do nothing
                        } else {
                            expandControlRoomList[dateKey][filteredRooms[i].id] = {
                                expanded: false
                            }
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        }

        this.setState({
            filteredHouseForm: filteredHouseData,
            expandControlRoomList
        }, () => {
            this.props.notificationLoadingStatusUpdate(false);
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.houseForm.id) {
            if (this.props.navigation.getParam('sceneTitle') != nextProps.houseForm.name) {
                nextProps.navigation.setParams({
                    sceneTitle: nextProps.houseForm.name,
                    choreAddButtonActive: nextProps.houseForm.rooms.length
                })
            }

            this.updateHouseData(nextProps.houseForm);
        }

        if (nextProps.fullHouseRefetch.active) {
            this.props.assignmentFetchFullSingle(this.props.userToken, nextProps.fullHouseRefetch.targetId);
        }

        if (nextProps.crossFullHouseRefetch.active) {
            this.props.houseReducerUpdate('fullHouseRefetch', nextProps.crossFullHouseRefetch);
        }
    }

    renderDateContent = ({item, index}) => {
        return (
            <DateContentCard
                item={item}
                index={index}
                expandControlRoomList={this.state.expandControlRoomList[item.date]}
                onRoomChoreClick={(chore) => {
                    this.props.assignmentReducerUpdate('choreForm', chore);
                    this.props.navigation.navigate('AssignmentChoreViewScene');
                }}
                onRoomExpandToggle={(roomId) => {
                    expandControlRoomList = { ...this.state.expandControlRoomList };
                    expandControlRoomList[item.date][roomId].expanded = !expandControlRoomList[item.date][roomId].expanded;

                    LayoutAnimation.configureNext(slideAnimationConfig);

                    this.setState({
                        expandControlRoomList
                    })
                }}
                onChoreCompleteCheckboxClick={(item, index) => {
                    newStatus = 'done';
                    if (item.status == 'done') {
                        newStatus = 'doing';
                    }

                    this.props.assignmentReducerUpdate('selectedRoomArrayId', item.roomArrayId);
                    this.props.assignmentReducerUpdate('selectedChoreArrayId', item.arrayId);
                    this.props.assignmentSingleChoreStatusUpdate(this.props.userToken, item.id, newStatus);
                }}
                onRoomCompleteCheckboxClick={(item, index) => {
                    newStatus = !item.completed;

                    this.props.assignmentReducerUpdate('selectedRoomArrayId', item.arrayId);
                    this.props.assignmentRoomStatusUpdate(this.props.userToken, item.id, newStatus);
                }}
            />
        )
    }

    renderContent() {
        dateArray = _.map(this.state.filteredHouseForm.rooms, (val, uid) => {
            return {
                rooms: val,
                date: uid
            }
        })

        if (dateArray.length > 0) {
            return (   
                <FlatList
                    data={dateArray}
                    renderItem={this.renderDateContent}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                />
            )
        } else {
            return (
                <View
                    style={{
                        marginTop: 30,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <CustomText>
                        Không có công việc.
                    </CustomText>
                </View>
            )
        }
    }

    renderIllustration() {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    paddingTop: verticalScale(160)
                }}
            >
                <CustomImage 
                    style={{
                        width: verticalScale(200),
                        height: verticalScale(200)
                    }}
                    source={require('../../../../assets/img/chore-illustration-01.png')}
                    resizeMode={'contain'}
                />
            </View>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                });
            }, 500);
        })

        this.props.assignmentFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                {
                    (this.props.houseForm.rooms && this.props.houseForm.rooms.length > 0) ?
                    <View
                        style={styles.choreTabContainerStyle}
                    >
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('daily')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'daily' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'daily' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong ngày
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('weekly')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'weekly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'weekly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong tuần
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('monthly')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'monthly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'monthly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong tháng
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    :
                    null
                }
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {
                        (this.props.houseForm.rooms && this.props.houseForm.rooms.length > 0) ?
                        this.renderContent()
                        :
                        this.renderIllustration()
                    }
                    <View
                        style={{height: verticalScale(30)}}
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    choreTabContainerStyle: {
        flexDirection: 'row',
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    choreTabStyle: {
        width: '33.33%',
        padding: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        height: verticalScale(40)
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    }
}

const mapStateToProps = ({ auth, assignment }) => {
    return {
        userToken: auth.token.userToken,
        selectedHouseId: assignment.houses[assignment.selectedHouseArrayId].id,
        houseForm: assignment.houseForm,
        fullHouseRefetch: assignment.fullHouseRefetch,
        crossFullHouseRefetch: assignment.crossFullHouseRefetch,
    }
}

export default connect(mapStateToProps, {
    assignmentFetchFullSingle,
    assignmentReducerUpdate,
    assignmentSingleChoreStatusUpdate,
    assignmentRoomStatusUpdate,
    assignmentManagementCancel,
    houseReducerUpdate,
    notificationLoadingStatusUpdate
})(AssignmentAssignedScene);