import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View,
    ScrollView,
    TouchableOpacity,
    FlatList,
    TextInput
} from 'react-native';
import { 
    Card,
    CustomDivider,
    CardRow,
    CardColumn,
    CustomTextInput,
    CustomPicker,
    CustomCheckbox,
    CustomSelectBox,
    CustomModal,
    CustomText,
    CustomIcon,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    
} from '../../../actions';
import {
    weekRepeat,
    monthRepeat
} from '../../../const';
import {
    verticalScale
} from '../../../utils';

class AssignmentChoreViewScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenTitle')
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            choreForm: { ...props.choreForm },
            weekRepeat,
            monthRepeat
        }
    }

    UNSAFE_componentWillMount() {
        this.props.navigation.setParams({
            screenTitle: this.props.choreForm.name
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps) {

    }

    renderRoomItem = ({item}) => {
        return (
            <CardRow>
                <CardColumn
                    style={[styles.columnFirst, {
                        justifyContent: 'center',
                        alignItems: 'center'
                    }]}
                >
                    <CustomText
                        style={{
                            fontSize: verticalScale(16)
                        }}
                    >
                        -
                    </CustomText>
                </CardColumn>
                <CardColumn
                    style={[styles.columnSecond, {
                        justifyContent: 'center'
                    }]}
                >
                    <CustomText
                        style={{
                            fontSize: verticalScale(16)
                        }}
                    >
                        {item.name}
                    </CustomText>
                </CardColumn>
            </CardRow>
        )
    }

    renderImage() {
        return this.state.choreForm.images.map((item, index) => {
            return (
                <TouchableOpacity
                    key={'chore-image-' + index}
                >
                    <CustomImage
                        style={{
                            width: '100%',
                            height: verticalScale(300)
                        }}
                        source={{uri: item.image}}
                    />
                </TouchableOpacity>
            )
        })
    }

    renderWeekRepeatListItem(item) {
        return (
            <CustomSelectBox
                textDate={item.label} 
                checked={this.state.choreForm.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: '13%'}}
                onPress={null}
            />
        )
    }

    renderMonthRepeatListItem({item}) {
        return (
            <CustomSelectBox
                textDate={item.label + 1} 
                checked={this.state.choreForm.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: verticalScale(30), marginRight: verticalScale(5)}}
                onPress={null}
            />
        )
    }

    renderRepeatSelectList() {        
        if (this.state.choreForm.repeat == 'weekly') {
            return (
                <CardRow
                    style={{justifyContent: 'space-between', marginTop: verticalScale(16)}}
                >
                    {
                        this.state.weekRepeat.map((item) => this.renderWeekRepeatListItem(item))
                    }
                </CardRow>
            )
        } else if (this.state.choreForm.repeat == 'monthly') {
            return (
                <CardRow
                    style={{marginTop: verticalScale(10)}}
                >
                    <FlatList
                        data={this.state.monthRepeat}
                        renderItem={this.renderMonthRepeatListItem.bind(this)}
                        extraData={this.state.choreForm.repeatList}
                        horizontal
                        keyExtractor={(item, index) => index.toString()}
                    />
                </CardRow>
            )
        }
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    {/* First part of the chore form */}
                    <Card
                        style={{paddingBottom: 0}}
                    >
                        <CustomTextInput
                            inputStyle={{fontSize: verticalScale(18), fontWeight: 'bold', color: '#333333'}}
                            containerStyle={{width: '100%', paddingTop: verticalScale(5), paddingBottom: verticalScale(5)}}
                            editSectionStyle={{paddingBottom: verticalScale(5)}}
                            value={this.state.choreForm.name}
                            editable={false}
                        />
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center', marginTop: verticalScale(5)}} name="clock-o" size={verticalScale(28)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <CustomPicker
                                     options={[
                                        {
                                            label: 'Hằng ngày',
                                            value: 'daily'
                                        },
                                        {
                                            label: 'Hằng tuần',
                                            value: 'weekly'
                                        },
                                        {
                                            label: 'Hằng tháng',
                                            value: 'monthly'
                                        }
                                    ]}
                                    value={this.state.choreForm.repeat}
                                    containerStyle={{width: '50%'}}
                                    enabled={false} 
                                />
                                {
                                    this.state.choreForm.repeat == 'daily' ?
                                    null
                                    :
                                    this.renderRepeatSelectList()
                                }
                                <CardRow
                                    style={{paddingTop: verticalScale(10), paddingBottom: verticalScale(5)}}
                                >
                                    <CustomText
                                        style={{paddingRight: verticalScale(10)}}
                                    >
                                        Ưu tiên công việc này
                                    </CustomText>
                                    <CustomCheckbox
                                        checked={this.state.choreForm.priority}
                                    />
                                </CardRow>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider />
                        <CardRow
                            style={null}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center', marginTop: verticalScale(5)}} name="sticky-note" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    value={this.state.choreForm.note}
                                    placeholder={'Ghi chú cho công việc này...'}
                                    style={{paddingLeft: 0}}
                                    editable={false}
                                    style={{color: '#333333', fontSize: verticalScale(14), paddingTop: 0}}
                                />
                            </CardColumn>
                        </CardRow>
                    </Card>
                    {/* Last part, sample image */}
                    <Card
                        style={{marginBottom: verticalScale(60)}}
                    >
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={[styles.columnFirst, {
                                    paddingBottom: verticalScale(5)
                                }]}
                            >
                                <CustomIcon style={{textAlign: 'center'}} name="camera" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <CustomText
                                    style={{fontSize: 16, fontWeight: 'bold', paddingTop: verticalScale(5)}}
                                >
                                    Ảnh minh họa
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: 0, marginBottom: 0}} />
                        <CardRow>
                            <View
                                style={{position: 'relative', width: '100%', marginTop: verticalScale(10)}}                   
                            >
                                {this.state.choreForm.images.length > 0 ? this.renderImage() : <CustomText>Không có hình ảnh minh họa.</CustomText>}
                            </View>
                        </CardRow>
                    </Card>
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    columnFirst: {
        width: '10%'
    },
    columnSecond: {
        width: '90%'
    }
}

const mapStateToProps = ({ assignment }) => {
    return {
        choreForm: assignment.choreForm
    }
}

export default connect(mapStateToProps, {
    
})(AssignmentChoreViewScene);