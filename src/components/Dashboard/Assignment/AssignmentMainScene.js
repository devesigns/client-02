import React, { Component } from 'react';
import { 
    View,
    FlatList,
    ScrollView,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import {
    CustomText,
    CardTouchable,
    CardRow,
    CardColumn,
    CustomModal,
    CustomIcon,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    assignmentReducerUpdate,
    assignmentManagementCancel,
    assignmentFetch
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';

class AssignmentMainScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedHouseId: '',
            houseManagementCancelModalVisible: false,
            refreshing: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.assignmentFetch(this.props.userToken);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {

    }

    renderItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    this.props.assignmentReducerUpdate('selectedHouseArrayId', index);
                    this.props.navigation.navigate('AssignmentAssignedScene');
                }}
                style={{padding: 0, paddingBottom: verticalScale(10)}}
            >
                <CustomImage
                    style={{width: '100%', height: verticalScale(150), backgroundColor: '#333333'}}
                    source={{uri: item.image ? item.image : 'https://mybutlervietnam.s3-ap-southeast-1.amazonaws.com/house/default/house_dribbble.jpg'}}
                />
                <CardRow
                    style={[styles.cardRowStyle, {paddingBottom: 0}]}
                >
                    <CardColumn
                        style={{width: '85%'}}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(18), fontWeight: 'bold', paddingTop: verticalScale(5)}}
                        >
                            {item.name ? item.name : 'Nhà chưa đặt tên'}
                        </CustomText>
                        <CustomText
                            numberOfLines={1}
                        >
                            {item.address ? item.address : 'Địa chỉ...'}
                        </CustomText>
                    </CardColumn>
                    <CardColumn
                        style={[
                            styles.toolColumnStyle, 
                            {
                                width: '15%',
                                paddingTop: verticalScale(5),
                                alignItems: 'center'
                            }
                        ]}
                    >
                        <TouchableOpacity
                            style={styles.toolIconContainerStyle}
                            onPress={() => {
                                this.props.assignmentReducerUpdate('selectedHouseArrayId', index);
                                this.setState({
                                    houseManagementCancelModalVisible: true,
                                    selectedHouseId: item.id
                                });
                            }}
                        >
                            <CustomIcon style={styles.roomEditIcon} name="sign-out" size={verticalScale(24)} />
                        </TouchableOpacity>
                    </CardColumn>
                </CardRow>
            </CardTouchable>
        )
    }

    renderHouseManagementCancelModal() {
        return (
            <CustomModal
                headerText={'Dừng quản lý công việc'}
                visible={this.state.houseManagementCancelModalVisible}
                onRequestClose={() => this.setState({houseManagementCancelModalVisible: false})}
                onConfirm={() => {
                    this.setState({houseManagementCancelModalVisible: false});
                    this.props.assignmentManagementCancel(this.props.userToken, this.state.selectedHouseId);
                }}
            >
                <CustomText>
                    Bạn có chắc là muốn hủy nhận tất cả các công việc thuộc căn nhà này không?
                </CustomText>
            </CustomModal>
        )
    }

    renderContent() {
        return (
            <FlatList
                data={this.props.houses}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderIllustration() {
        return (
            <CustomText
                style={{
                    paddingTop: 30,
                    paddingHorizontal: verticalScale(15),
                    textAlign: 'center'
                }}
            >
                Bạn chưa nhận công việc nào cả.
            </CustomText>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                })
            }, 500);
        })

        this.props.assignmentFetch(this.props.userToken);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >  
                    {
                        (this.props.houses.length > 0) ?
                        this.renderContent()
                        :
                        this.renderIllustration()
                    }
                    <View
                        style={{width: '100%', height: verticalScale(30)}}
                    />
                </ScrollView>
                {this.renderHouseManagementCancelModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    },
    cardRowStyle: {
        paddingTop: verticalScale(5),
        paddingLeft: verticalScale(5),
        paddingRight: verticalScale(5)
    }
}

const mapStateToProps = ({ auth, assignment }) => {
    return {
        userToken: auth.token.userToken,
        houses: assignment.houses
    }
}

export default connect(mapStateToProps, {
    assignmentReducerUpdate,
    assignmentManagementCancel,
    assignmentFetch
})(AssignmentMainScene);