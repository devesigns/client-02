import React, { Component } from 'react';
import { 
    View,
    FlatList,
    TouchableWithoutFeedback,
    ScrollView,
} from 'react-native';
import {
    CardRow,
    CardColumn,
    CustomCheckbox,
    CardTouchable,
    CustomText,
    CustomIcon
} from '../../common';
import { connect } from 'react-redux';
import {
    assignmentReducerUpdate
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';

class AssignmentMassAssignDetailScene extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedUserId: '',
            viewMode: 'daily'
        }
    }

    UNSAFE_componentWillMount() {

    }

    UNSAFE_componentWillReceiveProps(nextProps) {

    }

    setViewMode(viewMode) {
        this.setState({
            viewMode
        })
    }
    
    renderTaskItem = ({item, index}) => {
        if (this.state.viewMode != item.repeat) {
            return;
        }

        return (
            <CardTouchable
                onPress={() => {
                    this.props.assignmentReducerUpdate('choreForm', item);
                    this.props.navigation.navigate('AssignmentChoreViewScene');
                }}
            >
                <CardRow
                    style={{paddingTop: verticalScale(5)}}
                >
                    <CardColumn
                        style={{width: '70%'}}
                    >
                        <CustomText>
                            {item.name}&nbsp;
                            <CustomText
                                style={{
                                    fontSize: verticalScale(12),
                                    fontStyle: 'italic'
                                }}
                            >
                                {item.roomName}
                            </CustomText>
                        </CustomText>
                    </CardColumn>
                    <CardColumn
                        style={{width: '30%', flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 0, alignItems: 'center'}}
                    >
                        {
                            item.note || item.image ?
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="exclamation" size={verticalScale(19)} style={{color: '#FF8383'}} />
                            </View>
                            :
                            null
                        }
                        <View
                            style={styles.toolIconContainerStyle}
                        >
                            <CustomIcon name="flag" size={verticalScale(17)} style={item.priority ? {color: '#FF8383'} : null} />
                        </View>
                    </CardColumn>
                </CardRow>
            </CardTouchable>
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <View
                    style={styles.choreTabContainerStyle}
                >
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('daily')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'daily' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'daily' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng ngày
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('weekly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'weekly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'weekly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tuần
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('monthly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'monthly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'monthly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tháng
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <ScrollView>
                    <FlatList
                        data={this.props.detail.tasks}
                        renderItem={this.renderTaskItem}
                        extraData={this.state}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
            </View>
        )
    }
}

const styles = {
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    },
    choreTabContainerStyle: {
        flexDirection: 'row',
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    choreTabStyle: {
        width: '33.33%',
        padding: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        height: verticalScale(40)
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
}

const mapStateToProps = ({ auth, assignment }) => {
    return {
        detail: assignment.massAssignDetail
    }
}

export default connect(mapStateToProps, {
    assignmentReducerUpdate
})(AssignmentMassAssignDetailScene);