import _ from 'lodash';
import React, { Component } from 'react';
import {
    View
} from 'react-native';
import { connect } from 'react-redux';
import {
    assignmentReducerUpdate
} from '../../../actions';

class InvitationNotiTabBarIcon extends Component {
    constructor(props) {
        super(props);
        this.count = 0;
    }

    render() {
        if (_.isEmpty(this.props.invitations)) {
            return null;
        }

        return (
            <View
                style={{
                    height: 7,
                    width: 7,
                    position: 'absolute',
                    top: 0,
                    right: -3,
                    backgroundColor: '#FF8383',
                    borderRadius: 7 / 2,
                }}
            />
        )
    }
}

const mapStateToProps = ({ assignment }) => {
    return {
        invitations: assignment.invitations
    }
}

export default connect(mapStateToProps, {
    assignmentReducerUpdate
})(InvitationNotiTabBarIcon);
