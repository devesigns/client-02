import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    RefreshControl,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {
    Card,
    CardTouchable,
    CardRow,
    CardColumn,
    CustomText,
    CustomDivider,
    CustomIcon
} from '../../common';
import { 
    assignmentInvitationFetch,
    assignmentInvitationStatusUpdate,
    assignmentReducerUpdate,
    houseFetch
} from '../../../actions';
import {
    weekRepeat
} from '../../../const';
import {
    verticalScale,
    formatDateAndTime
} from '../../../utils';

class AssignmentInvitedScene extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.assignmentInvitationFetch(this.props.userToken);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        nextInvitationListLength = Object.keys(nextProps.invitations.length).length;
        currentInvitationListLength = Object.keys(this.props.invitations).length;
        if (nextInvitationListLength - currentInvitationListLength == 50) {
            this.props.serviceOrderFetch(this.props.userToken, nextInvitationListLength);
        }

        if (nextProps.houseManagementAccepted) {
            this.props.houseFetch(this.props.userToken);
            this.props.assignmentReducerUpdate('houseManagementAccepted', false);
        }
    }

    getRoomString(rooms) {
        roomString = '';
        for (i = 0; i < rooms.length; i++) {
            roomString += rooms[i].name;
            
            if (i < rooms.length - 1) {
                roomString += ', ';
            }
        }

        return roomString;
    }

    renderRepeatList(repeat, repeatList) {
        if (repeat == 'daily') {
            return null;
        }

        if (repeat == 'weekly') {
            return (
                <CustomText
                    style={{fontSize: verticalScale(12)}}
                >
                    &nbsp;-&nbsp;
                    {
                        repeatList.map((val, index) => {
                            repeatListItem = weekRepeat[val].label;

                            if (index != repeatList.length - 1) {
                                repeatListItem += ', ';
                            }

                            return repeatListItem;
                        })
                    }
                </CustomText>
            )
        }

        if (repeat == 'monthly') {
            return (
                <CustomText
                    style={{fontSize: verticalScale(12)}}
                >
                    &nbsp;-&nbsp;
                    {
                        repeatList.map((val, index) => {
                            repeatListItem = val;

                            if (index != repeatList.length - 1) {
                                repeatListItem += ', ';
                            }

                            return repeatListItem;
                        })
                    }
                </CustomText>
            )
        }

        return null;
    }

    renderToolSection(invitationId) {
        return (
            <CardColumn
                style={{width: '40%', paddingRight: 0}}
            >
                <CardRow
                    style={{justifyContent: 'flex-end', marginBottom: 0}}
                >
                    <TouchableOpacity
                        onPress={() => this.props.assignmentInvitationStatusUpdate(this.props.userToken, invitationId, 'denied')}
                    >
                        <CustomIcon name="close" size={verticalScale(32)} style={{color: '#FF8383', paddingLeft: verticalScale(15)}} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.assignmentInvitationStatusUpdate(this.props.userToken, invitationId, 'accepted')}
                    >
                        <CustomIcon name="check" size={verticalScale(32)} style={{color: '#51FFC0', paddingLeft: verticalScale(15)}} />
                    </TouchableOpacity>
                </CardRow>
            </CardColumn>
        )
    }

    renderSingleChoreInvitation(item, index) {
        return (
            <Card>
                <CardRow
                    
                >
                    <CustomText
                        
                    >
                        Từ <CustomText style={{fontWeight: 'bold'}}>{item.fromUser.fullname}</CustomText>, gửi ngày <CustomText>{formatDateAndTime(item.createdAt)}</CustomText>
                    </CustomText>
                </CardRow>
                <CustomDivider />
                <CardRow
                    style={{
                        flexWrap: 'wrap'
                    }}
                >
                    <CustomText
                        style={{fontSize: verticalScale(18)}}
                    >
                        Phụ trách&nbsp;
                    </CustomText>
                    <CustomText
                        style={{
                            fontSize: verticalScale(18),
                            fontWeight: 'bold'
                        }}
                        numberOfLines={2}
                    >
                        {item.detail.tasks[0].name}
                    </CustomText>
                </CardRow>
                <CardRow>
                    <CustomText>
                        {this.getRoomString(item.detail.rooms)}
                    </CustomText>
                </CardRow>
                <CardRow>
                    <CustomText>
                        <CustomText style={{fontWeight: 'bold'}}>{item.detail.house.name}</CustomText>, {item.detail.house.address}
                    </CustomText>
                </CardRow>
                <CardRow
                    style={{paddingTop: verticalScale(15), marginBottom: 0}}
                >
                    <CardColumn
                        style={{width: '60%', paddingLeft: 0}}
                    >
                        <CardRow
                            style={{marginBottom: 0}}
                        >
                            <CustomIcon name="flag" size={verticalScale(17)} style={[{paddingRight: verticalScale(15)}, item.detail.tasks[0].priority ? {color: '#FF8383'} : null]} />
                            <CustomIcon name="repeat" size={verticalScale(17)} style={{paddingRight: verticalScale(5)}} />
                            <CustomText style={{fontSize: verticalScale(12)}}>{repeatOptions[item.detail.tasks[0].repeat]}</CustomText>
                            {this.renderRepeatList(item.detail.tasks[0].repeat, item.detail.tasks[0].repeatList)}    
                        </CardRow>
                    </CardColumn>
                    {this.renderToolSection(item.id)}
                </CardRow>
            </Card>
        )
    }

    renderMassAssignInvitation(item) {
        return (
            <Card>
                <CardRow
                    
                >
                    <CustomText
                        
                    >
                        Từ <CustomText style={{fontWeight: 'bold'}}>{item.fromUser.fullname}</CustomText>, gửi ngày <CustomText>{formatDateAndTime(item.createdAt)}</CustomText>
                    </CustomText>
                </CardRow>
                <CustomDivider />
                <CardRow>
                    <CustomText
                        style={{fontSize: verticalScale(18)}}
                    >
                        Phụ trách&nbsp;
                    </CustomText>
                    <CustomText
                        style={{fontSize: verticalScale(18), fontWeight: 'bold'}}
                    >
                        nhiều công việc
                    </CustomText>
                </CardRow>
                <CardRow>
                    <CustomText>
                        tại&nbsp;
                    </CustomText>
                    <CustomText
                        style={{fontWeight: 'bold'}}
                    >
                        {item.detail.house.name}
                    </CustomText>
                </CardRow>
                <CardRow
                    style={{paddingTop: verticalScale(15), marginBottom: 0}}
                >
                    <CardColumn
                        style={{width: '60%', paddingLeft: 0, justifyContent: 'flex-end'}}
                    >
                        <CardRow
                            style={{marginBottom: 0}}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.assignmentReducerUpdate('massAssignDetail', item.detail);
                                    this.props.navigation.navigate('AssignmentMassAssignDetailScene');
                                }}
                            >
                                <CustomText
                                    style={{color: '#29ABE2'}}
                                >
                                    Xem chi tiết
                                </CustomText>
                            </TouchableOpacity>
                        </CardRow>
                    </CardColumn>
                    {this.renderToolSection(item.id)}
                </CardRow>
            </Card>
        )
    }

    renderHouseAssignInvitation(item, index) {
        return (
            <Card>
                <CardRow
                    
                >
                    <CustomText
                        
                    >
                        Từ <CustomText style={{fontWeight: 'bold'}}>{item.fromUser.fullname}</CustomText>, gửi ngày <CustomText>{formatDateAndTime(item.createdAt)}</CustomText>
                    </CustomText>
                </CardRow>
                <CustomDivider />
                <CardRow>
                    <CustomText
                        style={{fontSize: verticalScale(18)}}
                    >
                        Quản lý&nbsp;
                    </CustomText>
                    <CustomText
                        style={{fontSize: verticalScale(18), fontWeight: 'bold'}}
                    >
                        {item.detail.house.name}
                    </CustomText>
                </CardRow>
                {
                    item.detail.house.address ?
                    <CardRow>
                        <CustomText>
                            {item.detail.house.address}
                        </CustomText>
                    </CardRow>
                    :
                    null
                }
                {
                    item.detail.house.note ?
                    <CardRow>
                        <CustomText
                            style={{fontStyle: 'italic'}}
                        >
                            {item.detail.house.note}
                        </CustomText>
                    </CardRow>
                    :
                    null
                }
                <CardRow
                    style={{
                        paddingTop: verticalScale(15),
                        marginBottom: 0
                    }}
                >
                    <CardColumn
                        style={{
                            width: '60%',
                            paddingLeft: 0,
                            justifyContent: 'flex-end'
                        }}
                    >
                        <CardRow
                            style={{marginBottom: 0}}
                        >
                            <CustomIcon name='home' size={verticalScale(17)} style={{paddingRight: verticalScale(5)}} />
                            <CustomText style={{fontSize: verticalScale(12)}}>{item.detail.house.totalRoom} phòng</CustomText>                    
                        </CardRow>
                    </CardColumn>
                    {this.renderToolSection(item.id)}
                </CardRow>
            </Card>
        )
    }

    renderInvitationItem = ({item, index}) => {
        invitation = item.invitation;
        invitation.id = item.id;

        if (invitation.type == 'receive_task') {
            if (invitation.detail.tasks.length == 1) {
                return this.renderSingleChoreInvitation(invitation, index);
            };

            if (invitation.detail.tasks.length > 1) {
                return this.renderMassAssignInvitation(invitation, index);
            };
        }

        if (invitation.type == 'manage_house') {
            if (invitation.detail.house) {
                return this.renderHouseAssignInvitation(invitation, index);
            }
        }
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                })
            }, 500);
        })

        this.props.assignmentInvitationFetch(this.props.userToken);
    }

    render() {
        console.log(this.props.invitations);
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {
                        _.isEmpty(this.props.invitations) ?
                        <CustomText
                            style={{textAlign: 'center', paddingTop: 30}}
                        >
                            Hiện bạn chưa nhận được lời mời nào cả!  
                        </CustomText>
                        :
                        <FlatList
                            data={this.props.invitations}
                            renderItem={this.renderInvitationItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    }
                    <View
                        style={{height: verticalScale(30)}}
                    />
                </ScrollView>
            </View>
        )
    }
}

const repeatOptions = {
    'daily': 'Hằng ngày',
    'weekly': 'Hằng tuần',
    'monthly': 'Hằng tháng'
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    }
}

const mapStateToProps = ({ auth, assignment }) => {
    invitations = _.toArray(assignment.invitations);

    return {
        userToken: auth.token.userToken,
        invitations,
        houseManagementAccepted: assignment.houseManagementAccepted
    }
}

export default connect(mapStateToProps, {
    assignmentInvitationFetch,
    assignmentInvitationStatusUpdate,
    assignmentReducerUpdate,
    houseFetch
})(AssignmentInvitedScene);