import _ from 'lodash';
import React, { Component } from 'react';
import {
    View,
    ScrollView,
    RefreshControl,
    TouchableWithoutFeedback,
    FlatList,
    TouchableOpacity,
    LayoutAnimation,
    Animated
} from 'react-native';
import { connect } from 'react-redux';
import {
    Card,
    CardRow,
    CardColumn,
    CustomDivider,
    CustomText,
    CustomIcon,
    CustomAvatar,
    CustomCheckbox,
    CustomImage
} from '../../common';
import {
    houseFetchFullSingle,
    houseReducerUpdate,
    notificationLoadingStatusUpdate
} from '../../../actions';
import {
    verticalScale,
    formatDate
} from '../../../utils';
import {
    slideAnimationConfig,
    fadeAnimationConfig
} from '../../../const';

class DateContentCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expandControlRoomList: { ...this.props.expandControlRoomList }
        }
    }

    UNSAFE_componentWillMount() {
        // LayoutAnimation.configureNext(fadeAnimationConfig);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            expandControlRoomList: { ...nextProps.expandControlRoomList }
        })
    }

    renderRoomChoreItem = ({item, index}) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.onRoomChoreClick(item)}
            >
                <CardRow
                    style={{paddingTop: verticalScale(5)}}
                >
                    
                    <CardColumn
                        style={{width: '7%', paddingLeft: 0}}
                    >
                        {
                            (item.status == 'done') ?
                            <CustomIcon name="check" size={verticalScale(16)} style={{color: '#51FFC0'}} />
                            :
                            null
                        }
                    </CardColumn>
                    <CardColumn
                        style={{width: '53%', paddingLeft: 0}}
                    >
                        <CustomText
                            style={{}}
                            numberOfLines={2}
                        >
                            {item.name}
                        </CustomText>
                    </CardColumn>
                    <CardColumn
                        style={{
                            width: '40%',
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            paddingRight: 0,
                            alignItems: 'center'
                        }}
                    >
                        
                        {
                            item.butler ?
                            <View
                                style={[styles.toolIconContainerStyle, {flexDirection: 'row'}]}
                            >
                                <CustomAvatar
                                    size={verticalScale(18)}
                                    round
                                    url={item.butler ? item.butler.profileImage : null}
                                />
                                <View
                                    style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
                                >
                                    {(() => {
                                        if (item.status == 'init' && item.butler) {
                                            return (
                                                <CustomText
                                                    style={{fontSize: verticalScale(10)}}
                                                >
                                                    &nbsp;(chờ)
                                                </CustomText>    
                                            )
                                        }

                                        if (item.status == 'denied') {
                                            return (
                                                <CustomText
                                                    style={{color : '#FF8383', fontSize: verticalScale(10)}}
                                                >
                                                    &nbsp;(từ chối)
                                                </CustomText>    
                                            )
                                        }

                                        return;
                                    })()}
                                </View>
                            </View>
                            :
                            null
                        }
                        {
                            item.priority ?
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="flag" size={verticalScale(16)} style={{color: '#FF8383'}} />
                            </View>
                            :
                            null
                        }
                        <View
                            style={styles.toolIconContainerStyle}
                        >
                            <CustomIcon style={styles.roomEditIcon} name="pencil" size={verticalScale(18)} />
                        </View>
                    </CardColumn>
                </CardRow>
            </TouchableOpacity>
        )
    }

    renderRoomItem = ({item, index}) => {
        let prioritizedChores = [];
        let normalChores = [];
        let roomName = item.name;

        for (i in item.chores) {
            if (item.chores[i].priority) {
                prioritizedChores.push(item.chores[i]);
            } else {
                normalChores.push(item.chores[i]);
            }
        }

        let sortedChores = [ ...prioritizedChores, ...normalChores ];

        return (
            <CardRow>
                <CardColumn
                    style={{
                        paddingLeft: 0,
                        paddingRight: 0,
                        justifyContent: 'center'
                    }}
                >
                    <TouchableOpacity
                        style={{
                            width: '100%',
                            marginBottom: verticalScale(5),
                            flexDirection: 'row',
                            paddingTop: verticalScale(5),
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            this.props.onRoomExpandToggle(item.id);
                        }}
                    >
                        {
                            (item.completed) ?
                            <CardColumn
                                style={{width: '7%', paddingLeft: 0}}
                            >
                                <CustomIcon name="check" size={verticalScale(16)} style={{color: '#51FFC0'}} />
                            </CardColumn>
                            :
                            null
                        }
                        <CustomText
                            style={{width: '90%', fontWeight: 'bold'}}
                        >
                            {item.name}
                        </CustomText>
                        {
                            this.state.expandControlRoomList[item.id].expanded ?
                            <CustomIcon
                                name='minus-circle'
                                size={verticalScale(24)}
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}  
                            />
                            :
                            <CustomIcon
                                name='plus-circle'
                                size={verticalScale(24)}
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}  
                            />
                        }
                    </TouchableOpacity>
                    {
                        this.state.expandControlRoomList[item.id].expanded ?
                        <View>
                            <FlatList
                                data={sortedChores}
                                renderItem={({item, index}) => {
                                    let modifiedItem = {
                                        ...item,
                                        roomName
                                    }
                                    return this.renderRoomChoreItem({item: modifiedItem, index});
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                            <TouchableOpacity
                                onPress={() => this.props.onRoomChoreCreate(item.name, index)}
                            >
                                <CardRow
                                    style={{
                                        marginTop: verticalScale(10)
                                    }}
                                >
                                    <CardColumn
                                        style={{width: '7%', paddingLeft: 0}}
                                    />
                                    <CustomText
                                        style={{
                                            width: '90%',
                                            paddingLeft: 0,
                                            color: '#29ABE2'
                                        }}
                                    >
                                        Thêm công việc mới...
                                    </CustomText>
                                </CardRow>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                    }
                </CardColumn>
            </CardRow>
        )
    }

    render() {
        item = this.props.item;

        return (
            <Card>
                <CustomText
                    style={{
                        marginTop: verticalScale(5),
                        marginBottom: verticalScale(5),
                        color: 'rgba(33, 33, 33, 0.3)',
                        fontSize: verticalScale(14)
                    }}
                >
                    {item.date}
                </CustomText>
                {
                    item.rooms.length > 0 ?
                    <FlatList
                        data={item.rooms}
                        renderItem={this.renderRoomItem}
                        extraData={this.state.expandControlRoomList}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    :
                    <CustomText>
                        Không có công việc.
                    </CustomText>
                }           
            </Card>
        )
    }

}

class HouseChoreMainScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('houseName'),
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                navigation.getParam('toggleHeaderMenu')();
                            } catch {

                            }
                        }}
                        style={{
                            paddingLeft: verticalScale(15)
                        }}
                    >
                        {
                            navigation.getParam('menuOpen') ?
                            <CustomIcon name='times' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                            :
                            <CustomIcon name='plus-circle' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                        }
                    </TouchableOpacity>
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            viewMode: 'daily',
            filteredHouseForm: {},
            refreshing: false,
            // An object holding all id of displayed rooms
            // From there we will decide whether one room is expanded or not
            expandControlRoomList: {}
        }

        this.houseIdList = [];
        this.menuOpacityInput = new Animated.Value(0);
        this.menuOpen = false;
        this.menuTimeout = null;
    }

    UNSAFE_componentWillMount() {
        this.props.houseFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
        this.props.navigation.setParams({
            toggleHeaderMenu: () => {
                this.toggleHeaderMenu();

                this.menuTimeout = setTimeout(() => {
                    this.toggleHeaderMenu();
                }, 3000)
            }
        })
    }

    toggleHeaderMenu = () => {
        clearTimeout(this.menuTimeout);

        this.menuOpen = !this.menuOpen;
        let nextOpacity = this.menuOpen ? 1 : 0;

        Animated.timing(
            this.menuOpacityInput,
            {
                toValue: nextOpacity,
                duration: 300,
                useNativeDriver: true
            }
        ).start();

        this.props.navigation.setParams({
            menuOpen: this.menuOpen
        })
    }

    setViewMode(viewMode) {
        if (this.state.viewMode == viewMode) {
            return;
        }

        this.setState({
            viewMode
        }, () => {
            this.props.notificationLoadingStatusUpdate(true);
            this.updateHouseData(this.props.houseForm);
        });
    }

    updateHouseData(houseData) {
        expandControlRoomList = { ...this.state.expandControlRoomList };
        if (_.isEmpty(houseData)) {
            this.props.notificationLoadingStatusUpdate(false);
            return;
        }

        if (this.state.viewMode == 'daily') {
            date = new Date();

            newRooms = {};
            filteredRooms = [];

            for (i = 0; i < houseData.rooms.length; i++) {
                filteredChores = [];

                for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                    if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                        // Do sth when the due time of weekly and monthly chores
                        // equals to the current date

                        if (houseData.rooms[i].chores[j].repeatList.indexOf(date.getDay()) > -1 || houseData.rooms[i].chores[j].repeatList.indexOf(date.getDate()) > -1) {
                            // If today is included in weekly or monthly repeat
                            // do nothing
                        } else {
                            // Else continue
                            continue;
                        }
                    }

                    filteredChores.push(houseData.rooms[i].chores[j]);
                }

                if (filteredChores.length < 1) {
                    continue;
                }

                filteredRooms.push({
                    ...houseData.rooms[i],
                    chores: filteredChores
                })
            }

            if (filteredRooms.length > 0) {
                dateKey = formatDate(date);

                newRooms[dateKey] = filteredRooms;
                expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                for (i = 0; i < filteredRooms.length; i++) {
                    if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                        // Do nothing
                    } else {
                        expandControlRoomList[dateKey][filteredRooms[i].id] = {
                            expanded: false
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        } else if (this.state.viewMode == 'weekly') {
            date = new Date();
            day = date.getDay();

            newRooms = {};

            for (t = day + 1; t < 7; t++) {
                filteredRooms = [];

                for (i = 0; i < houseData.rooms.length; i++) {
                    filteredChores = [];

                    for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                        if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                            if (houseData.rooms[i].chores[j].repeat == 'daily') {
                                // If it's daily chore, nothing to do here, continue
                                continue;
                            }

                            // If it's monthly chore, check if any due date
                            // equals the currently processed day
                            // Notice here that:
                            // getDate returns (1 - 31)
                            // getDay returns (0 - 6)
                            // Pretty BS but oh well
                            tDate = date.getDate() + (t - day);
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(tDate) > -1) {
                                filteredChores.push(houseData.rooms[i].chores[j]);
                            }
                        } else {
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(t) > -1) {
                                filteredChores.push(houseData.rooms[i].chores[j]);
                            }
                        }     
                    }

                    if (filteredChores.length < 1) {
                        continue;
                    }

                    filteredRooms.push({
                        ...houseData.rooms[i],
                        chores: filteredChores
                    })
                }

                if (filteredRooms.length > 0) {
                    let dateClone = new Date(date.getTime());
                    dateClone.setDate(dateClone.getDate() + (t - day));
                    dateKey = formatDate(dateClone);

                    newRooms[dateKey] = filteredRooms;
                    expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                    for (i = 0; i < filteredRooms.length; i++) {
                        if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                            // Do nothing
                        } else {
                            expandControlRoomList[dateKey][filteredRooms[i].id] = {
                                expanded: false
                            }
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        } else {
            date = new Date();
            day = date.getDay();
            dateInMonth = date.getDate();

            newRooms = {};

            for (t = dateInMonth + (6 - day) + 1; t < 32; t++) {
                filteredRooms = [];

                for (i = 0; i < houseData.rooms.length; i++) {
                    filteredChores = [];

                    for (j = 0; j < houseData.rooms[i].chores.length; j++) {
                        if (houseData.rooms[i].chores[j].repeat != this.state.viewMode) {
                            // If it's daily or weekly chore, nothing to do here, continue
                            continue;
                        } else {
                            if (houseData.rooms[i].chores[j].repeatList.indexOf(t) > -1) {
                                filteredChores.push(houseData.rooms[i].chores[j]);
                            }
                        }     
                    }

                    if (filteredChores.length < 1) {
                        continue;
                    }

                    filteredRooms.push({
                        ...houseData.rooms[i],
                        chores: filteredChores
                    })
                }

                if (filteredRooms.length > 0) {
                    let dateClone = new Date(date.getTime());
                    dateClone.setDate(t);
                    dateKey = formatDate(dateClone);

                    newRooms[dateKey] = filteredRooms;
                    expandControlRoomList[dateKey] = expandControlRoomList[dateKey] ? expandControlRoomList[dateKey] : {};
                    for (i = 0; i < filteredRooms.length; i++) {
                        if (expandControlRoomList[dateKey][filteredRooms[i].id]) {
                            // Do nothing
                        } else {
                            expandControlRoomList[dateKey][filteredRooms[i].id] = {
                                expanded: false
                            }
                        }
                    }
                }
            }

            filteredHouseData = {
                ...houseData,
                rooms: newRooms
            }
        }

        this.setState({
            filteredHouseForm: filteredHouseData,
            expandControlRoomList
        }, () => {
            this.props.notificationLoadingStatusUpdate(false);
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.houseForm.id) {
            this.updateHouseData(nextProps.houseForm);
        }
    }

    renderDateContent = ({item, index}) => {
        return (
            <DateContentCard
                item={item}
                index={index}
                expandControlRoomList={this.state.expandControlRoomList[item.date]}
                onRoomChoreClick={(chore) => {
                    this.props.houseReducerUpdate('isChoreCreate', false);
                    this.props.houseReducerUpdate('choreForm', chore);
                    this.props.navigation.navigate('HouseChoreFormScene', {
                        roomName: chore.roomName
                    });
                }}
                onRoomExpandToggle={(roomId) => {
                    expandControlRoomList = { ...this.state.expandControlRoomList };
                    expandControlRoomList[item.date][roomId].expanded = !expandControlRoomList[item.date][roomId].expanded;

                    LayoutAnimation.configureNext(slideAnimationConfig);

                    this.setState({
                        expandControlRoomList
                    })
                }}
                onRoomChoreCreate={(roomName, roomArrayId) => {
                    this.props.houseReducerUpdate('isChoreCreate', true);
                    this.props.houseReducerUpdate('selectedRoomArrayId', roomArrayId);
                    this.props.navigation.navigate('HouseChoreFormScene', {
                        roomName,
                        viewMode: this.state.viewMode
                    })
                }}
            />
        )
    }

    renderContent() {
        dateArray = _.map(this.state.filteredHouseForm.rooms, (val, uid) => {
            return {
                rooms: val,
                date: uid
            }
        })

        if (dateArray.length > 0) {
            return (   
                <FlatList
                    data={dateArray}
                    renderItem={this.renderDateContent}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                />
            )
        } else {
            return (
                <View
                    style={{
                        marginTop: 30,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <CustomText>
                        Không có công việc.
                    </CustomText>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('HouseRoomScene');
                        }}
                    >
                        <CustomText
                            style={{
                                color: '#29ABE2',
                                padding: verticalScale(10)
                            }}
                        >
                            Thêm việc mới trong trang quản lý chi tiết
                        </CustomText>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    renderIllustration() {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    flex: 1,
                    paddingTop: verticalScale(160)
                }}
            >
                <CustomImage 
                    style={{
                        width: verticalScale(150),
                        height: verticalScale(100),
                        position: 'absolute',
                        top: verticalScale(0),
                        right: verticalScale(10),
                    }}
                    source={require('../../../../assets/img/chore-tip-01.png')}
                    resizeMode={'contain'}
                />
                <CustomImage 
                    style={{
                        width: verticalScale(200),
                        height: verticalScale(200)
                    }}
                    source={require('../../../../assets/img/chore-illustration-01.png')}
                    resizeMode={'contain'}
                />
            </View>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                });
            }, 500);
        })

        this.props.houseFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                {
                    (this.props.houseForm.rooms && this.props.houseForm.rooms.length > 0) ?
                    <View
                        style={styles.choreTabContainerStyle}
                    >
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('daily')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'daily' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'daily' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong ngày
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('weekly')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'weekly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'weekly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong tuần
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => this.setViewMode('monthly')}
                        >
                            <View
                                style={[styles.choreTabStyle, this.state.viewMode == 'monthly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                            >
                                <CustomText
                                    style={[styles.choreTabTextStyle, this.state.viewMode == 'monthly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                                >
                                    Trong tháng
                                </CustomText>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    :
                    null
                }
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {
                        (this.props.houseForm.rooms && this.props.houseForm.rooms.length > 0) ?
                        this.renderContent()
                        :
                        this.renderIllustration()
                    }
                    <View
                        style={{height: verticalScale(30)}}
                    />
                </ScrollView>
                <Animated.View
                    style={{
                        position: 'absolute',
                        top: verticalScale(5),
                        right: verticalScale(15),
                        backgroundColor: '#FFFFFF',
                        paddingHorizontal: verticalScale(10),
                        shadowColor: '#333333',
                        shadowOffset: { width: 0, height: 3 },
                        shadowOpacity: 1.0,
                        shadowRadius: 3,
                        elevation: 3,
                        borderRadius: verticalScale(5),
                        opacity: this.menuOpacityInput,
                        transform: [
                            {
                                translateX: this.menuOpacityInput.interpolate({
                                    inputRange: [0, 0.1, 1],
                                    outputRange: [9999, 0, 0]
                                })
                            }
                        ]
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.toggleHeaderMenu();
                            this.props.navigation.navigate('HouseRoomScene', {
                                addRoom: true
                            });
                        }}
                        style={{
                            paddingVertical: verticalScale(10),
                            flexDirection: 'row',
                            alignItems: 'center',
                            borderBottomColor: 'rgba(33, 33, 33, 0.3)',
                            borderBottomWidth: 0.5
                        }}
                    >
                        <CustomIcon name='plus-circle' size={verticalScale(18)} style={{width: verticalScale(24)}} />
                        <CustomText
                            style={{
                                paddingLeft: verticalScale(5)
                            }}
                        >
                            Thêm phòng
                        </CustomText>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.toggleHeaderMenu();
                            this.props.navigation.navigate('HouseMassAssignScene');
                        }}
                        style={{
                            paddingVertical: verticalScale(10),
                            flexDirection: 'row',
                            alignItems: 'center',
                            borderBottomColor: 'rgba(33, 33, 33, 0.3)',
                            borderBottomWidth: 0.5
                        }}
                    >
                        <CustomIcon name='users' size={verticalScale(16)} style={{width: verticalScale(24)}} />
                        <CustomText
                            style={{
                                paddingLeft: verticalScale(5)
                            }}
                        >
                            Phân công
                        </CustomText>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.toggleHeaderMenu();
                            this.props.navigation.navigate('HouseMassDeleteScene');
                        }}
                        style={{
                            paddingVertical: verticalScale(10),
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                    >
                        <CustomIcon name='trash-o' size={verticalScale(18)} style={{width: verticalScale(24)}} />
                        <CustomText
                            style={{
                                paddingLeft: verticalScale(5)
                            }}
                        >
                            Xóa
                        </CustomText>
                    </TouchableOpacity>
                </Animated.View>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerRightContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    choreTabContainerStyle: {
        flexDirection: 'row',
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    choreTabStyle: {
        width: '33.33%',
        padding: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        height: verticalScale(40)
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    }
}

const mapStateToProps = ({ auth, house }) => {
    return {
        userToken: auth.token.userToken,
        houseForm: house.houseForm,
        selectedHouseId: house.selectedHouseArrayId >= 0 ? house.houses[house.selectedHouseArrayId].id : null,
    }
}

export default connect(mapStateToProps, {
    houseFetchFullSingle,
    houseReducerUpdate,
    notificationLoadingStatusUpdate
})(HouseChoreMainScene);