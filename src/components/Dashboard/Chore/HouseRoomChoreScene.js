import React, { Component } from 'react';
import { 
    View,
    FlatList,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Text,
    ScrollView
} from 'react-native';
import {
    Card,
    CardRow,
    CardColumn,
    CustomCheckbox,
    CardTouchable,
    CustomModal,
    CustomText,
    CustomIcon,
    CustomButtonFooter
} from '../../common';
import { connect } from 'react-redux';
import {
    choreCreate,
    choreUpdate,
    choreDelete,
    houseReducerUpdate
} from '../../../actions/HouseActions';
import {
    verticalScale
} from '../../../utils';

class HouseRoomChoreScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('bulkDeleteMode') ? 'Xóa công việc' : 'Quản lý công việc' + ` - ${navigation.getParam('roomName')}`,
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    {
                        navigation.getParam('bulkDeleteMode')
                        ?
                        <TouchableOpacity
                            onPress={() => {
                                navigation.getParam('toggleBulkDeleteMode')();
                            }}
                            style={styles.headerRightButtonStyle}
                        >
                            <Text
                                style={{fontWeight: 'bold', color: '#FFFFFF'}}
                            >
                                HỦY
                            </Text>
                        </TouchableOpacity>
                        :
                        <View
                            style={styles.headerRightContainerStyle}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    try {
                                        navigation.getParam('toggleBulkDeleteMode')();
                                    } catch {
                                        console.log('Too fast!');
                                    }
                                }}
                                style={styles.headerRightButtonStyle}
                            >
                                <CustomIcon name='trash' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    try {
                                        navigation.getParam('choreCreate')();
                                    } catch {
                                        console.log('Too fast!');
                                    }
                                }}
                                style={styles.headerRightButtonStyle}
                            >
                                <CustomIcon name='plus-circle' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            choreDeleteModalVisible: false,
            bulkDeleteMode: false,
            bulkDeleteSelectedArray: [],
            viewMode: 'daily',
            selectedChoreId: ''
        }

        this.props.navigation.setParams({
            choreCreate: () => {
                this.props.houseReducerUpdate('isChoreCreate', true);
                this.props.navigation.navigate('HouseChoreFormScene', {
                    roomName: this.props.navigation.getParam('roomName')
                });
            },
            toggleBulkDeleteMode: () => {
                if (this.props.chores.length > 0) {
                    this.setState({
                        bulkDeleteMode: !this.state.bulkDeleteMode,
                        bulkDeleteSelectedArray: []
                    }, () => {
                        this.props.navigation.setParams({
                            bulkDeleteMode: this.state.bulkDeleteMode
                        })
                    })
                }
            },
        });
    }

    setViewMode(viewMode) {
        this.setState({
            viewMode
        })
    }

    getAvailableChores() {
        availableChores = this.props.chores.filter((val) => {
            return val.repeat == this.state.viewMode;
        });

        return availableChores;
    }

    renderDeleteCheckBox(item, index) {
        return (
            <CardColumn
                style={{width: '10%'}}
            >
                <CustomCheckbox
                    checked={this.state.bulkDeleteSelectedArray.indexOf(item.id) > -1}
                    onPress={() => {
                        if (this.state.bulkDeleteMode) {
                            if (this.state.bulkDeleteSelectedArray.indexOf(item.id) > -1) {
                                newArray = [ ...this.state.bulkDeleteSelectedArray ];
                                index = newArray.indexOf(item.id);

                                if (index > -1) {
                                    newArray.splice(index, 1);
                                }

                                this.setState({
                                    bulkDeleteSelectedArray: newArray
                                })
                            } else {
                                this.setState({
                                    bulkDeleteSelectedArray: [ ...this.state.bulkDeleteSelectedArray, item.id ]
                                });
                            }
                        }
                    }}
                />
            </CardColumn>
        )
    }
    

    renderItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    if (!this.state.bulkDeleteMode) {
                        this.props.houseReducerUpdate('isChoreCreate', false);
                        this.props.houseReducerUpdate('selectedChoreArrayId', index);
                        this.props.houseReducerUpdate('choreForm', item);
                        this.props.navigation.navigate('HouseChoreFormScene', {
                            roomName: this.props.navigation.getParam('roomName')
                        });
                    }
                }}
            >
                <CardRow
                    style={{alignItems: 'center'}}
                >
                    {
                        this.state.bulkDeleteMode ?
                        this.renderDeleteCheckBox(item, index)
                        :
                        null
                    }
                    <CardColumn
                        style={!this.state.bulkDeleteMode ? {width: '80%'} : {width: '70%'}}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(14)}}
                            numberOfLines={2}
                        >
                            {item.name}
                        </CustomText>
                    </CardColumn>
                    {
                        this.state.bulkDeleteMode ?
                        null
                        :
                        <CardColumn
                            style={[styles.toolColumnStyle, {width: '20%'}]}
                        >
                            <TouchableOpacity>
                                <View
                                    style={styles.toolIconContainerStyle}
                                >
                                    <CustomIcon name="pencil" size={verticalScale(18)} />
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        selectedChoreId: item.id,
                                        choreDeleteModalVisible: true
                                    });
                                }}
                            >
                                <View
                                    style={styles.toolIconContainerStyle}
                                >
                                    <CustomIcon name="trash" size={verticalScale(18)} />
                                </View>
                            </TouchableOpacity>
                        </CardColumn>
                    }
                </CardRow>
            </CardTouchable>
        )
    }

    renderModal() {
        return (
            <CustomModal
                headerText={'Xóa công việc'}
                visible={this.state.choreDeleteModalVisible}
                onRequestClose={() => this.setState({choreDeleteModalVisible: false})}
                onConfirm={() => {
                    this.setState({choreDeleteModalVisible: false});
                    this.props.choreDelete(this.props.userToken, [ this.state.selectedChoreId ]);
                }}
            >
                <CustomText>
                    Bạn có chắc là muốn xóa công việc này không?
                </CustomText>
            </CustomModal>
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <View
                    style={styles.choreTabContainerStyle}
                >
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('daily')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'daily' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'daily' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng ngày
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('weekly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'weekly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'weekly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tuần
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('monthly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'monthly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'monthly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tháng
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <ScrollView>
                    {
                        this.props.chores.length > 0 ?
                        <FlatList
                            data={this.getAvailableChores()}
                            renderItem={this.renderItem}
                            extraData={this.state.bulkDeleteSelectedArray}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        :
                        <Text
                            style={{textAlign: 'center', paddingTop: verticalScale(30)}}
                        >
                            Chưa có công việc nào. Tạo công việc mới trước nhé!   
                        </Text>
                    }
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                {
                    this.state.bulkDeleteMode ?
                    <CustomButtonFooter
                        title={'Xóa việc đã chọn'}
                        onPress={() => {
                            this.props.choreDelete(this.props.userToken, this.state.bulkDeleteSelectedArray);
                            this.setState({
                                bulkDeleteMode: false,
                                bulkDeleteSelectedArray: []
                            }, () => {
                                this.props.navigation.setParams({
                                    bulkDeleteMode: this.state.bulkDeleteMode
                                })
                            })
                        }}
                    />
                    :
                    null
                }
                {this.renderModal()}
            </View>
        )
    }
}

const styles = {
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    },
    choreTabContainerStyle: {
        flexDirection: 'row',
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    choreTabStyle: {
        width: '33.33%',
        padding: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        height: verticalScale(40)
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
}

const mapStateToProps = ({ auth, house }) => {
    return {
        userToken: auth.token.userToken,
        chores: house.chores,
    }
}

export default connect(mapStateToProps, {
    choreCreate,
    choreUpdate,
    choreDelete,
    houseReducerUpdate
})(HouseRoomChoreScene);