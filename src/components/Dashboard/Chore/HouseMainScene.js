import React, { Component } from 'react';
import { 
    View,
    FlatList,
    ScrollView,
    TouchableOpacity,
    RefreshControl
} from 'react-native';
import {
    CustomText,
    CardTouchable,
    CardRow,
    CardColumn,
    CustomModal,
    CustomIcon,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    houseCreate,
    houseReducerUpdate,
    houseDelete,
    houseManagementCancel,
    houseFetch,
    houseFetchFullSingle
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';

class HouseMainScene extends Component {

    static navigationOptions = (props) => {
        return {
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                // props.navigation.getParam('houseReducerUpdate')('fullHouseActive', true);
                                props.navigation.getParam('houseCreate')();
                            } catch {
                                console.log('Too fast!');
                            }
                        }}
                        style={styles.headerRightButtonStyle}
                    >
                        <CustomIcon name='plus-circle' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            houseDeleteModalVisible: false,
            refreshing: false
        }
        
        this.props.navigation.setParams({
            houseCreate: () => {
                this.props.houseCreate(this.props.userToken);
            },
            houseReducerUpdate: (props, value) => {
                this.props.houseReducerUpdate(props, value);
            }
        })
    }

    UNSAFE_componentWillMount() {
        this.props.houseFetch(this.props.userToken);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        // Whenever there is a change to props,
        // we will check if there's a new house form (meaning the new house is created)
        if (nextProps.newHouseReady) {
            this.props.houseReducerUpdate('isHouseCreate', true);
            this.props.houseReducerUpdate('newHouseReady', false);
            this.props.houseReducerUpdate('selectedHouseArrayId', nextProps.houses.length - 1)
            this.props.navigation.navigate('HouseFormScene');
        }

        if (nextProps.fullHouseRefetch.active) {
            this.props.houseFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
        }
    }

    renderItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    this.props.houseReducerUpdate('isHouseCreate', false);
                    this.props.houseReducerUpdate('selectedHouseArrayId', index);
                    this.props.navigation.navigate('HouseChoreMainScene', {
                        houseName: item.name
                    });
                }}
                style={{padding: 0, paddingBottom: verticalScale(10)}}
            >
                <CustomImage
                    style={{width: '100%', height: verticalScale(150), backgroundColor: '#333333'}}
                    source={{uri: item.image ? item.image : 'https://mybutlervietnam.s3-ap-southeast-1.amazonaws.com/house/default/house_dribbble.jpg'}}
                />
                <CardRow
                    style={[
                        styles.cardRowStyle,
                        {
                            paddingBottom: 0,
                            alignItems: 'center'
                        }
                    ]}
                >
                    <View
                        style={{
                            flexDirection: 'column',
                            flex: 1
                        }}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(18), fontWeight: 'bold'}}
                        >
                            {item.name ? item.name : 'Nhà chưa đặt tên'}
                        </CustomText>
                        <CustomText
                            numberOfLines={1}
                        >
                            {item.address ? item.address : 'Địa chỉ...'}
                        </CustomText>
                    </View>
                    <View
                        style={[styles.toolColumnStyle]}
                    >
                        <TouchableOpacity
                            style={styles.toolIconContainerStyle}
                            onPress={() => {
                                this.props.houseReducerUpdate('selectedHouseArrayId', index);
                                this.props.navigation.navigate('HouseFormScene', {
                                    houseName: item.name
                                });
                            }}
                        >
                            <CustomIcon style = {styles.roomEditIcon} name="pencil" size={verticalScale(24)} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.toolIconContainerStyle}
                            onPress={() => {
                                this.props.houseReducerUpdate('selectedHouseArrayId', index);
                                this.setState({
                                    houseDeleteModalVisible: true
                                });
                            }}
                        >
                            {
                                this.props.userProfile.id == item.owner ?
                                <CustomIcon style={styles.roomEditIcon} name="trash" size={verticalScale(24)} />
                                :
                                <CustomIcon style={styles.roomEditIcon} name="sign-out" size={verticalScale(24)} />
                            }
                        </TouchableOpacity>
                    </View>
                </CardRow>
                <CardRow
                    style={[styles.cardRowStyle, {paddingTop: 0}]}
                >
                    
                </CardRow>
            </CardTouchable>
        )
    }

    renderHouseDeleteModal() {
        try {
            if (this.props.selectedHouseArrayId < 0) {
                return;
            }
            
            if (this.props.userProfile.id == this.props.houses[this.props.selectedHouseArrayId].owner) {
                return (
                    <CustomModal
                        headerText={'Xóa nhà'}
                        visible={this.state.houseDeleteModalVisible}
                        onRequestClose={() => this.setState({houseDeleteModalVisible: false})}
                        onConfirm={() => {
                            this.setState({houseDeleteModalVisible: false});
                            this.props.houseDelete(this.props.userToken, this.props.houses[this.props.selectedHouseArrayId].id);
                        }}
                    >
                        <CustomText>
                            Bạn có chắc là muốn xóa ngôi nhà này không?
                        </CustomText>
                    </CustomModal>
                )
            } else {
                return (
                    <CustomModal
                        headerText={'Dừng quản lý nhà'}
                        visible={this.state.houseDeleteModalVisible}
                        onRequestClose={() => this.setState({houseDeleteModalVisible: false})}
                        onConfirm={() => {
                            this.setState({houseDeleteModalVisible: false});
                            this.props.houseManagementCancel(this.props.userToken, this.props.houses[this.props.selectedHouseArrayId].id);
                        }}
                    >
                        <CustomText>
                            Bạn có chắc là không muốn tiếp tục quản lý căn nhà này nữa không?
                        </CustomText>
                    </CustomModal>
                )
            }
        } catch {
            return;
        }
    }

    renderContent() {
        return (
            <FlatList
                data={this.props.houses}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderIllustration() {
        return (
            <CustomText
                style={{
                    paddingTop: 30,
                    paddingHorizontal: verticalScale(15),
                    textAlign: 'center'
                }}
            >
                Bạn chưa có ngôi nhà nào cả. Hãy tạo một ngôi nhà trước nhé!
            </CustomText>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                })
            }, 500);
        })

        this.props.houseFetch(this.props.userToken);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >  
                    {
                        (this.props.houses.length > 0) ?
                        this.renderContent()
                        :
                        this.renderIllustration()
                    }
                    <View
                        style={{width: '100%', height: verticalScale(30)}}
                    />
                </ScrollView>
                {this.renderHouseDeleteModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        paddingLeft: verticalScale(20),
    },
    cardRowStyle: {
        paddingTop: verticalScale(10),
        paddingLeft: verticalScale(10),
        paddingRight: verticalScale(10),
        justifyContent: 'space-between'
    }
}

const mapStateToProps = ({ auth, house, user }) => {
    return {
        userToken: auth.token.userToken,
        userProfile: user.userProfile,
        houses: house.houses,
        newHouseReady: house.newHouseReady,
        selectedHouseArrayId: house.selectedHouseArrayId,
        fullHouseRefetch: house.fullHouseRefetch,
        selectedHouseId: house.selectedHouseArrayId >= 0 ? house.houses[house.selectedHouseArrayId].id : null,
    }
}

export default connect(mapStateToProps, {
    houseCreate,
    houseReducerUpdate,
    houseDelete,
    houseManagementCancel,
    houseFetch,
    houseFetchFullSingle
})(HouseMainScene);