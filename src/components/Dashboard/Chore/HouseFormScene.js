import _ from 'lodash';
import React, { Component } from 'react';
import { 
    ScrollView,
    View,
    TextInput,
    TouchableOpacity,
    FlatList
} from 'react-native';
import {
    CustomText,
    Card,
    CardRow,
    CardColumn,
    CustomTextInput,
    CardTouchable,
    CustomIcon,
    CustomModal,
    CustomDivider,
    CustomAvatar,
    CustomButtonFooter,
    CustomImage
} from '../../common';
import UserSelectModal from '../../UserSelectModal';
import { connect } from 'react-redux';
import {
    houseCreate,
    houseReducerUpdate,
    houseUpdate,
    houseFetchFullSingle
} from '../../../actions';
import ImagePicker from 'react-native-image-picker';
import {
    verticalScale
} from '../../../utils';

class HouseFormScene extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('houseName') ? navigation.getParam('houseName') : 'Nhà chưa đặt tên'
        }
    }

    constructor(props) {
        super(props);

        // Map managers to id list
        managerUserIdList = []
        for (i = 0; i < this.props.houseForm.managers.length; i++) {
            managerUserIdList.push(this.props.houseForm.managers[i].manager.id);
        }

        this.state = {
            houseForm: { 
                ...this.props.houseForm,
                managerUserIdList
            },
            userSelectModalVisible: false,
        }
    }

    UNSAFE_componentWillMount() {
        this.props.houseFetchFullSingle(this.props.userToken, this.props.selectedHouseId);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.houseForm.id) {
            newHouseForm = { ...nextProps.houseForm };

            newHouseForm.name = this.state.houseForm.name;
            newHouseForm.address = this.state.houseForm.address;
            newHouseForm.managers = this.state.houseForm.managers;
            newHouseForm.note = this.state.houseForm.note;
            newHouseForm.image = this.state.houseForm.image;

            if (this.state.houseForm.managerUserIdList.length > 0) {
                newHouseForm.managerUserIdList = this.state.houseForm.managerUserIdList;
            } else {
                // ---
                // Map managers to id list
                managerUserIdList = []
                for (i = 0; i < nextProps.houseForm.managers.length; i++) {
                    managerUserIdList.push(nextProps.houseForm.managers[i].manager.id);
                }

                newHouseForm.managerUserIdList = managerUserIdList;
            }

            this.setState({
                houseForm: newHouseForm,
                userSelectModalVisible: this.state.userSelectModalVisible
            })
        }

        if (nextProps.houseForm.id != this.state.houseForm.id) {
            this.setState({
                houseForm: {
                    ...this.state.houseForm,
                    ...nextProps.houseForm
                },
            })
        }
    }

    houseFormUpdate(props, value) {
        this.setState({
            houseForm: {
                ...this.state.houseForm,
                [props]: value
            }
        });
    }

    openImagePicker() {
        const imagePickerOptions = {
            title: 'Chọn ảnh',
            cancelButtonTitle: 'Quay lại',
            takePhotoButtonTitle: 'Chụp ảnh...',
            chooseFromLibraryButtonTitle: 'Chọn ảnh từ bộ sưu tập...',
            maxHeight: 600,
            maxWidth: 800
        };

        ImagePicker.showImagePicker(imagePickerOptions, (response) => {
            console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {                 
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
                this.houseFormUpdate('imageFile', response);
            }
        });
    }

    renderImage() {
        return (
            <TouchableOpacity
                onPress={this.openImagePicker.bind(this)}
            >
                <CustomImage
                    style={{width: '100%', height: verticalScale(200)}}
                    source={{uri: this.state.houseForm.imageFile ? this.state.houseForm.imageFile.uri : this.state.houseForm.image}}
                />
            </TouchableOpacity> 
        )
    }

    renderAddImageButton() {
        return (
            <View
                style={{width: '100%', height: verticalScale(200), backgroundColor: '#F2F2F2', alignItems: 'center', justifyContent: 'center'}}
            >
                <TouchableOpacity
                    style={{
                        height: verticalScale(150),
                        width: verticalScale(150),
                        borderRadius: 10,
                        borderWidth: verticalScale(5),
                        borderStyle: 'dashed',
                        alignItems: 'center',
                        justifyContent: 'center',
                        position: 'relative'
                    }}
                    onPress={this.openImagePicker.bind(this)}
                >
                    <CustomText
                        style={{fontSize: verticalScale(36), fontWeight: 'bold'}}
                    >
                        +
                    </CustomText>
                    <CustomText
                        style={{position: 'absolute', bottom: verticalScale(10)}}
                    >
                        Đăng ảnh nhà
                    </CustomText>
                </TouchableOpacity>
            </View>
        )
    }

    renderNumberOfRoomsAndChores() {
        return this.state.houseForm.totalRoom + ' phòng, ' + this.state.houseForm.totalChore + ' việc';
    }

    renderManagerItem = ({item}) => {
        console.log('RenderManagerItem', item);
        manager = item.manager;

        if (this.state.houseForm.managerUserIdList.includes(manager.id)) {
            return (
                <View
                    style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
                >
                    <CustomAvatar
                        containerStyle={{marginRight: verticalScale(3)}}
                        size={verticalScale(28)}
                        round
                        url={manager.profileImage}
                    />
                    <CustomText
                        style={{paddingLeft: verticalScale(5)}}
                    >
                        {(() => {
                            managerNameString = manager.fullname.split(' ');
                            return managerNameString[managerNameString.length - 1];
                        })()}
                    </CustomText>
                    {(() => {
                        if (item.status == 'inviting') {
                            return (
                                <CustomText>
                                    &nbsp;(chờ)
                                </CustomText>
                            )
                        }

                        if (item.status == 'denied') {
                            return (
                                <CustomText
                                    style={{color : '#FF8383'}}
                                >
                                    &nbsp;(từ chối)
                                </CustomText>    
                            )
                        }

                        return;
                    })()}
                    <View
                        style={{paddingRight: verticalScale(5)}}
                    />
                </View>
            )
        } else {
            return;
        }
    }

    render() {
        if (this.props.isHouseCreate) {
            console.log('Is creating house');
        } else {
            console.log("Is editing house");
        }

        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    {/* First part of the form, house img and name */}
                    <View
                        style={styles.headerContainerStyle}
                    >
                        {this.state.houseForm.image || this.state.houseForm.imageFile ? this.renderImage() : this.renderAddImageButton()}
                        <CardRow
                            style={{padding: verticalScale(15), paddingTop: verticalScale(15)}}
                        >
                            <CustomTextInput
                                placeholder="Tên nhà..."
                                maxLength={80}
                                showIcon
                                inputStyle={{fontSize: verticalScale(18), fontWeight: 'bold'}}
                                containerStyle={{width: '100%'}}
                                value={this.state.houseForm.name}
                                onChangeText={(value) => this.houseFormUpdate('name', value)}
                                autoFocus={this.props.isHouseCreate ? true : false}
                            />
                        </CardRow>
                    </View>

                    {/* Second part, a card that contains  address, administrators and note */}
                    <Card
                        style={{paddingTop: 0, paddingBottom: 0}}
                    >
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={styles.iconStyle} name="map-marker" size={verticalScale(30)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    placeholder={'Địa chỉ nhà'}
                                    value={this.state.houseForm.address}
                                    onChangeText={(value) => this.houseFormUpdate('address', value)}
                                    maxLength={150}
                                    multiline
                                    style={styles.textInputStyle}
                                />
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginBottom: 0, marginTop: 0}} />
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={styles.iconStyle} name="users" size={verticalScale(20)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TouchableOpacity
                                    style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}
                                    onPress={() => this.setState({userSelectModalVisible: true})}
                                >
                                    {(() => {
                                        return this.state.houseForm.managers.map((item, index) => {
                                            return this.renderManagerItem({item})
                                        })
                                    })()}
                                    <View>
                                        <CustomText
                                            style={{color: '#29ABE2', paddingLeft: verticalScale(5)}}
                                        >
                                            Chọn quản lý cùng bạn...
                                        </CustomText>
                                    </View>
                                </TouchableOpacity>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginBottom: 0, marginTop: 0}} />
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={styles.iconStyle} name="pencil" size={verticalScale(20)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    placeholder={'Ghi chú'}
                                    value={this.state.houseForm.note}
                                    onChangeText={(value) => this.houseFormUpdate('note', value)}
                                    maxLength={150}
                                    multiline
                                    style={styles.textInputStyle}
                                />
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <CardTouchable
                        onPress={() => {
                            this.props.houseReducerUpdate('rooms', this.state.houseForm.rooms);
                            this.props.navigation.navigate('HouseRoomScene');
                        }}
                    >
                        <CustomText
                            style={{fontWeight: 'bold'}}
                        >
                            Quản lý phòng
                        </CustomText>
                        <CustomText
                            style={{fontStyle: 'italic'}}
                        >
                            {this.renderNumberOfRoomsAndChores()}
                        </CustomText>
                        <CustomIcon style={styles.roomEditIcon} name="pencil" size={verticalScale(18)} />
                    </CardTouchable>
                    <View 
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title={'Lưu thông tin'}
                    onPress={() => {
                        submittedForm = { ...this.state.houseForm };
                        delete submittedForm['rooms'];

                        let imageFile = submittedForm.imageFile;
                        if (imageFile) {
                            if (imageFile.fileName) {
                                if (imageFile.fileName.endsWith('HEIC')) {
                                    imageFile.fileName = imageFile.fileName.replace('.HEIC', '.JPG');
                                } else if (imageFile.fileName.endsWith('heic')) {
                                    imageFile.fileName = imageFile.fileName.replace('.heic', '.JPG');
                                }
                            } else {
                                imageFile.fileName = 'Unnamed.JPG';
                            }

                            submittedForm.imageFile = imageFile;
                        }

                        this.props.houseUpdate(this.props.userToken, submittedForm, this.props.houseForm);
                        this.props.navigation.goBack();
                    }}
                />
                <UserSelectModal
                    visible={this.state.userSelectModalVisible}
                    selectedUser={this.state.houseForm.managerUserIdList}
                    onUserSelect={(item) => {
                        user = item.user;

                        managers = [ ...this.state.houseForm.managers ];
                        managerUserIdList = [ ...this.state.houseForm.managerUserIdList ];

                        let index = managerUserIdList.indexOf(user.id);
                        if (index > -1) {
                            managerUserIdList.splice(index, 1);
                            managers.splice(index, 1);
                        } else {
                            managerUserIdList.push(user.id);
                            managers.push({
                                manager: user
                            })
                        }

                        this.setState({
                            houseForm: {
                                ...this.state.houseForm,
                                managerUserIdList,
                                managers
                            }
                        });
                    }}
                    onRequestClose={() => this.setState({userSelectModalVisible: false})}
                    hideConfirm={false}
                    onConfirm={() => {
                        this.setState({
                            userSelectModalVisible: false
                        })
                    }}
                />
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerContainerStyle: {
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    columnFirst: {
        width: '10%'
    },
    columnSecond: {
        width: '90%'
    },
    cardRowStyle: {
        alignItems: 'center',
        paddingVertical: verticalScale(5)
    },
    roomEditIcon: {
        position: 'absolute',
        top: verticalScale(10),
        right: verticalScale(10)
    },
    textInputStyle: {
        fontSize: verticalScale(14),
        paddingTop: verticalScale(5),
        paddingBottom: verticalScale(5)
    },
    iconStyle: {
        textAlign: 'center',
        paddingTop: verticalScale(5),
        paddingBottom: verticalScale(5)
    }
}

const mapStateToProps = ({ auth, house, user }) => {
    houseForm = house.houseForm;
    if (!houseForm.managers) {
        houseForm.managers = [];
    }

    return {
        userToken: auth.token.userToken,
        isHouseCreate: house.isHouseCreate,
        houseForm,
        selectedHouseId: house.houses[house.selectedHouseArrayId].id,
        usersInContact: user.usersInContact,
    }
}

export default connect(mapStateToProps, {
    houseCreate,
    houseUpdate,
    houseReducerUpdate,
    houseFetchFullSingle
})(HouseFormScene);