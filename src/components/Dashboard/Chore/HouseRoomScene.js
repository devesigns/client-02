import React, { Component } from 'react';
import { 
    View,
    Text,
    TextInput,
    FlatList,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {
    Card,
    CardRow,
    CardColumn,
    CustomCheckbox,
    CardTouchable,
    CustomText,
    CustomModal,
    CustomIcon,
    CustomDivider,
    CustomButtonFooter
} from '../../common';
import { connect } from 'react-redux';
import {
    roomTemplateFetch,
    roomTemplateCreate,
    roomCreate,
    roomUpdate,
    roomDelete,
    houseReducerUpdate
} from '../../../actions/HouseActions';
import {
    verticalScale
} from '../../../utils';

class HouseRoomScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('bulkDeleteMode') ? 'Xóa phòng' : 'Quản lý phòng',
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    {
                        navigation.getParam('bulkDeleteMode')
                        ?
                        <TouchableOpacity
                            onPress={() => {
                                navigation.getParam('toggleBulkDeleteMode')();
                            }}
                            style={styles.headerRightButtonStyle}
                        >
                            <Text
                                style={{fontWeight: 'bold', color: '#FFFFFF'}}
                            >
                                HỦY
                            </Text>
                        </TouchableOpacity>
                        :
                        <View
                            style={styles.headerRightContainerStyle}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    try {
                                        navigation.getParam('toggleBulkDeleteMode')();
                                    } catch {
                                        console.log('Too fast!');
                                    }
                                }}
                                style={styles.headerRightButtonStyle}
                            >
                                <CustomIcon name='trash' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    try {
                                        navigation.getParam('roomCreate')();
                                    } catch {
                                        console.log('Too fast!');
                                    }
                                }}
                                style={styles.headerRightButtonStyle}
                            >
                                <CustomIcon name='plus-circle' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                            </TouchableOpacity>
                        </View>
                    }
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            selectedRoomArrayId: '',
            roomForm: {},
            roomDeleteModalVisible: false,
            bulkDeleteMode: false,
            bulkDeleteSelectedArray: [],
            roomCreateWithTemplateModalVisible: false,
            selectedTemplateRooms: [],
            roomCreateModalVisible: false
        }

        this.props.navigation.setParams({
            roomCreate: () => {
                this.setState({
                    roomCreateModalVisible: true
                })
            },
            toggleBulkDeleteMode: () => {
                if (this.props.rooms.length > 0) {
                    this.setState({
                        bulkDeleteMode: !this.state.bulkDeleteMode,
                        bulkDeleteSelectedArray: []
                    }, () => {
                        this.props.navigation.setParams({
                            bulkDeleteMode: this.state.bulkDeleteMode
                        })
                    })
                }
            },
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            rooms: nextProps.rooms,
            templateRooms: nextProps.templateRooms
        });

        if (nextProps.newRoomReady) {
            this.setState({
                roomForm: nextProps.roomForm
            })
        }
    }

    componentDidMount() {
        if (this.props.navigation.getParam('addRoom')) {
            this.setState({
                roomCreateModalVisible: true
            })
        }
    }

    renderDeleteCheckBox(item, index) {
        return (
            <CardColumn
                style={{width: '10%', paddingTop: verticalScale(5)}}
            >
                <CustomCheckbox
                    // checked={item.complete || this.state.bulkDeleteSelectedArray.indexOf(item.id) > -1}
                    checked={this.state.bulkDeleteSelectedArray.indexOf(item.id) > -1}
                    onPress={() => {
                        if (this.state.bulkDeleteMode) {
                            if (this.state.bulkDeleteSelectedArray.indexOf(item.id) > -1) {
                                newArray = [ ...this.state.bulkDeleteSelectedArray ];
                                index = newArray.indexOf(item.id);

                                if (index > -1) {
                                    newArray.splice(index, 1);
                                }

                                this.setState({
                                    bulkDeleteSelectedArray: newArray
                                })
                            } else {
                                this.setState({
                                    bulkDeleteSelectedArray: [ ...this.state.bulkDeleteSelectedArray, item.id ]
                                });
                            }
                        } 
                        // else {
                        //     newRoom = { ...item };
                        //     newRoom.complete = !newRoom.complete;

                        //     this.props.houseReducerUpdate('selectedRoomArrayId', index);
                        //     this.props.roomUpdate(newRoom, this.selectedHouseId, item.id);
                        // }
                    }}
                />
            </CardColumn>
        )
    }

    renderRoomForm(item, index) {
        return (
            // If in editting mode
            <CardRow
                style={{borderBottomWidth: 0.5, paddingBottom: verticalScale(5)}}
            >
                <CardColumn
                    style={{width: '80%'}}
                >
                    <TextInput
                        placeholder={'Tên phòng...'}
                        value={this.state.roomForm.name}
                        onChangeText={(value) => {
                            this.setState({
                                roomForm: {
                                    ...this.state.roomForm,
                                    name: value
                                },
                            })
                        }}
                        maxLength={50}
                        style={{
                            width: '100%',
                            padding: 0,
                            fontWeight: 'bold',
                            fontSize: verticalScale(16)
                        }}
                        autoFocus={true}
                    />
                </CardColumn>
                
                <CardColumn
                    style={[styles.toolColumnStyle, {width: '20%', paddingTop: verticalScale(5)}]}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.houseReducerUpdate('selectedRoomArrayId', index);
                            this.props.roomUpdate(this.props.userToken, this.state.roomForm);
                            this.setState({
                                roomForm: {}
                            })
                        }}
                    >
                        <View
                            style={styles.toolIconContainerStyle}
                        >
                            <CustomIcon style={{color: "#ADFF2F"}} name="check" size={verticalScale(18)} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                roomForm: {},
                            });
                        }}
                    >
                        <View
                            style={styles.toolIconContainerStyle}
                        >
                            <CustomIcon style={{color: "red"}} name="remove" size={verticalScale(18)} />
                        </View>
                    </TouchableOpacity>
                </CardColumn>
            </CardRow>
        )
    }
    
    renderRoomDetail(item, index) {
        return (
            // If not in editting mode, render as normal
            <CardRow>
                {
                    this.state.bulkDeleteMode ?
                    this.renderDeleteCheckBox(item, index)
                    :
                    null
                }
                <CardColumn
                    style={!this.state.bulkDeleteMode ? {width: '80%'} : {width: '70%'}}
                >
                    <CustomText
                        style={{fontSize: verticalScale(16), fontWeight: 'bold'}}
                    >
                        {item.name}
                    </CustomText>
                    <CustomText
                        style={{fontStyle: 'italic'}}
                    >
                        {item.totalChore} công việc
                    </CustomText>
                </CardColumn>
                {
                    this.state.bulkDeleteMode ?
                    null
                    :
                    <CardColumn
                        style={[styles.toolColumnStyle, {width: '20%', paddingTop: verticalScale(5)}]}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    roomForm: { ...item },
                                });
                            }}
                        >
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="pencil" size={verticalScale(18)} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    selectedRoomArrayId: index,
                                    roomDeleteModalVisible: true
                                });
                            }}
                        >
                            <View
                                style={styles.toolIconContainerStyle}
                            >
                                <CustomIcon name="trash" size={verticalScale(18)} />
                            </View>
                        </TouchableOpacity>
                    </CardColumn>
                }
            </CardRow>
        )
    }

    renderItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    if (!this.state.bulkDeleteMode) {
                        this.props.houseReducerUpdate('selectedRoomArrayId', index);
                        this.props.houseReducerUpdate('chores', item.chores);
                        this.props.navigation.navigate('HouseRoomChoreScene', {
                            roomName: item.name
                        });
                    }
                }}
            >
                {
                    item.id === this.state.roomForm.id ?
                    this.renderRoomForm(item, index)
                    :
                    this.renderRoomDetail(item, index)
                }     
            </CardTouchable>
        )
    }

    renderDeleteModal() {
        roomName = "phòng này";
        roomId = -1;
        try {
            roomName = this.props.rooms[this.state.selectedRoomArrayId].name;
            roomId = this.props.rooms[this.state.selectedRoomArrayId].id;
            houseId = this.props.rooms[this.state.selectedRoomArrayId].house;
        } catch {
            
        }

        return (
            <CustomModal
                headerText={'Xóa phòng'}
                visible={this.state.roomDeleteModalVisible}
                onRequestClose={() => this.setState({roomDeleteModalVisible: false})}
                onConfirm={() => {
                    this.setState({roomDeleteModalVisible: false});
                    this.props.roomDelete(this.props.userToken, [ roomId ]);
                }}
            >
                <CustomText>
                    Bạn có chắc là muốn xóa {roomName} không?
                </CustomText>
            </CustomModal>
        )
    }

    renderRoomCreateWithTemplateModal() {
        return (
            <CustomModal
                headerText={'Chọn phòng muốn tạo'}
                visible={this.state.roomCreateWithTemplateModalVisible}
                onRequestClose={() => {
                    this.setState({
                        selectedTemplateRooms: [],
                        roomCreateWithTemplateModalVisible: false
                    })
                }}
                onConfirm={() => {
                    this.setState({
                        roomCreateWithTemplateModalVisible: false
                    })
                    this.props.roomTemplateCreate(this.props.userToken, this.props.selectedHouseId, this.state.selectedTemplateRooms);
                }}
            >
                <FlatList
                    data={this.state.templateRooms}
                    extraData={this.state}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item}) => {
                        return (
                            <CardRow>
                                <TouchableOpacity
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        width: '100%',
                                        paddingVertical: verticalScale(5)
                                    }}
                                    onPress={() => {
                                        let selectedTemplateRooms = [ ...this.state.selectedTemplateRooms ];
                                        let index = selectedTemplateRooms.indexOf(item);

                                        if (index >= 0) {
                                            selectedTemplateRooms.splice(index, 1);
                                        } else {
                                            selectedTemplateRooms.push(item);
                                        }

                                        this.setState({
                                            selectedTemplateRooms
                                        })
                                    }}
                                >
                                    <CustomCheckbox
                                        checked={this.state.selectedTemplateRooms.includes(item)}
                                        onPress={() => {
                                            let selectedTemplateRooms = [ ...this.state.selectedTemplateRooms ];
                                            let index = selectedTemplateRooms.indexOf(item);

                                            if (index >= 0) {
                                                selectedTemplateRooms.splice(index, 1);
                                            } else {
                                                selectedTemplateRooms.push(item);
                                            }

                                            this.setState({
                                                selectedTemplateRooms
                                            })
                                        }}
                                    />
                                    <CustomText>
                                        {item}
                                    </CustomText>
                                </TouchableOpacity>
                            </CardRow>
                        )
                    }}
                />
            </CustomModal>
        )
    }

    renderRoomCreateModal() {
        return (
            <CustomModal
                hideConfirm={true}
                visible={this.state.roomCreateModalVisible}
                onRequestClose={() => this.setState({roomCreateModalVisible: false})}
            >
                <CardRow>
                    <TouchableOpacity
                        style={{
                            width: '100%'
                        }}
                        onPress={() => {
                            this.props.roomTemplateFetch(this.props.userToken);
                            this.setState({
                                selectedTemplateRooms: [],
                                roomCreateWithTemplateModalVisible: true,
                                roomCreateModalVisible: false
                            })
                        }}
                    >
                        <CustomText>
                            Tạo phòng theo mẫu có sẵn
                        </CustomText>
                    </TouchableOpacity>
                </CardRow>
                <CustomDivider />
                <CardRow>
                    <TouchableOpacity
                        style={{
                            width: '100%'
                        }}
                        onPress={() => {
                            this.setState({
                                roomCreateModalVisible: false
                            })
                            this.props.roomCreate(this.props.userToken, this.props.selectedHouseId);
                        }}
                    >
                        <CustomText>
                            Tạo phòng trống
                        </CustomText>
                    </TouchableOpacity>
                </CardRow>
            </CustomModal>
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    {
                        this.props.rooms.length > 0 ?
                        <FlatList
                            data={this.props.rooms}
                            renderItem={this.renderItem}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        :
                        <CustomText
                            style={{textAlign: 'center', paddingTop: 30}}
                        >
                            Chưa có phòng nào. Tạo phòng mới trước nhé!   
                        </CustomText>
                    }
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                {
                    this.state.bulkDeleteMode ?
                    <CustomButtonFooter
                        title={'Xóa phòng đã chọn'}
                        onPress={() => {
                            this.props.roomDelete(this.props.userToken, this.state.bulkDeleteSelectedArray);
                            this.setState({
                                bulkDeleteMode: false,
                                bulkDeleteSelectedArray: []
                            }, () => {
                                this.props.navigation.setParams({
                                    bulkDeleteMode: this.state.bulkDeleteMode
                                })
                            })
                        }}
                    />
                    :
                    null
                }
                {this.renderDeleteModal()}
                {this.renderRoomCreateModal()}
                {this.renderRoomCreateWithTemplateModal()}
            </View>
        )
    }
}

const styles = {
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(15)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    }
}

const mapStateToProps = ({ auth, house }) => {
    return {
        userToken: auth.token.userToken,
        selectedHouseArrayId: house.selectedHouseArrayId,
        selectedHouseId: house.houses[house.selectedHouseArrayId].id,
        rooms: house.rooms,
        templateRooms: house.templateRooms,
        newRoomReady: house.newRoomReady,
        roomForm: house.roomForm,
    }
}

export default connect(mapStateToProps, {
    roomTemplateFetch,
    roomCreate,
    roomTemplateCreate,
    roomUpdate,
    roomDelete,
    houseReducerUpdate
})(HouseRoomScene);