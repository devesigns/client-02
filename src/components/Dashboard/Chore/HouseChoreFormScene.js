// Image picker
// https://github.com/react-native-community/react-native-image-picker

import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View,
    ScrollView,
    TouchableOpacity,
    FlatList,
    TextInput,
    Platform
} from 'react-native';
import { 
    CustomText,
    Card,
    CardTouchable,
    CardRow,
    CardColumn,
    CustomTextInput,
    CustomPicker,
    CustomCheckbox,
    CustomSelectBox,
    CustomModal,
    CustomIcon,
    CustomAvatar,
    CustomDivider,
    CustomButtonFooter,
    CustomImage
} from '../../common';
import UserSelectModal from '../../UserSelectModal';
import ImagePicker from 'react-native-image-picker';
import CropImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import {
    choreDelete,
    choreUpdate,
    choreCreate,
    houseReducerUpdate
} from '../../../actions';
import {
    weekRepeat,
    monthRepeat
} from '../../../const';
import {
    verticalScale
} from '../../../utils';

class HouseChoreFormScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenTitle'),
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                navigation.getParam('onDeleteButtonClick')();
                            } catch {
                                console.log('Too fast!');
                            }
                        }}
                        style={styles.headerRightButtonStyle}
                    >
                        <CustomIcon name='trash' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            choreDeleteModalVisible: false,
            choreForm: { 
                ...props.choreForm,
                house: this.props.selectedHouseId,
                room: this.props.selectedRoomId,
                imagesToDelete: [],
                imagesToUpload: []
            },
            userSelectModalVisible: false,
            imagePickerModalVisible: false
        }

        this.props.navigation.setParams({
            onDeleteButtonClick: () => {
                this.setState({
                    choreDeleteModalVisible: true
                });
            }
        })
    }

    choreFormUpdate = (props, value) => {
        if (props == 'repeat') {
            let date = new Date();
            let newRepeatList = [];
            
            if (value == 'weekly') {
                newRepeatList.push(date.getDay());
            } else if (value == 'monthly') {
                newRepeatList.push(date.getDate());
            }

            this.setState({
                choreForm: {
                    ...this.state.choreForm,
                    [props]: value,
                    repeatList: newRepeatList
                }
            });
        } else {
            this.setState({
                choreForm: {
                    ...this.state.choreForm,
                    [props]: value
                }
            });
        }
    }

    UNSAFE_componentWillMount() {
        console.log('Chore Form', this.props.choreForm);

        if (this.props.isChoreCreate) {
            let repeat = this.props.navigation.getParam('viewMode');
            if (repeat) {
                this.choreFormUpdate('repeat', repeat);
            }

            this.props.navigation.setParams({
                screenTitle: `Tạo việc mới - ${this.props.navigation.getParam('roomName')}`
            });
        } else {
            this.props.navigation.setParams({
                screenTitle: this.props.choreForm.name + ` - ${this.props.navigation.getParam('roomName')}`
            });
        }

        this.props.houseReducerUpdate('isChoreBulkCreate', false);
    }

    openCamera() {
        const imagePickerOptions = {
            maxHeight: 600,
            maxWidth: 800
        };

        ImagePicker.launchCamera(imagePickerOptions, (response) => {
            console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {                 
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
                let images = [...this.state.choreForm.images];
                let imagesToUpload = [...this.state.choreForm.imagesToUpload];

                images.push({
                    image: response.uri,
                    uploadId: imagesToUpload.length
                })
                let imageToUpload = {
                    ...response,
                    uploadId: imagesToUpload.length
                };
                imageToUpload.path = imageToUpload.uri;
                imagesToUpload.push(imageToUpload); 

                this.setState({
                    choreForm: {
                        ...this.state.choreForm,
                        images,
                        imagesToUpload
                    }
                })
            }
        });
    }

    openImageGallery() {
        CropImagePicker.openPicker({
            multiple: true,
            compressImageMaxWidth: 800,
            compressImageMaxHeight: 600
        }).then((response) => {
            let images = [...this.state.choreForm.images];
            let imagesToUpload = [...this.state.choreForm.imagesToUpload];

            for (let i = 0; i < response.length; i++) {
                images.push({
                    image: response[i].path,
                    uploadId: i,
                })

                imagesToUpload.push({
                    ...response[i],
                    uploadId: i,
                });

                if (images.length == 6) {
                    break;
                }
            }

            this.setState({
                choreForm: {
                    ...this.state.choreForm,
                    images,
                    imagesToUpload
                }
            })
        });
    }

    renderImagePickerModal() {
        return (
            <CustomModal
                hideConfirm={true}
                visible={this.state.imagePickerModalVisible}
                onRequestClose={() => this.setState({imagePickerModalVisible: false})}
            >
                <CardRow>
                    <TouchableOpacity
                        style={{
                            width: '100%'
                        }}
                        onPress={() => {
                            this.setState({
                                imagePickerModalVisible: false
                            }, () => {
                                if (Platform.OS === 'ios') {
                                    setTimeout(() => {
                                        this.openCamera();
                                    }, 500);
                                } else {
                                    this.openCamera();
                                }
                            })
                        }}
                    >
                        <CustomText>
                            Chụp ảnh
                        </CustomText>
                    </TouchableOpacity>
                </CardRow>
                <CustomDivider />
                <CardRow>
                    <TouchableOpacity
                        style={{
                            width: '100%'
                        }}
                        onPress={() => {
                            this.setState({
                                imagePickerModalVisible: false
                            }, () => {
                                if (Platform.OS === 'ios') {
                                    setTimeout(() => {
                                        this.openImageGallery();
                                    }, 500);
                                } else {
                                    this.openImageGallery();
                                }
                            })
                        }}
                    >
                        <CustomText>
                            Chọn ảnh từ bộ sưu tập
                        </CustomText>
                    </TouchableOpacity>
                </CardRow>
            </CustomModal>
        )
    }

    renderImage() {
        return (
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={this.state.choreForm.images}
                extraData={this.state.choreForm.images}
                renderItem={({item, index}) => {
                    return (
                        <View
                            style={{
                                width: '100%',
                                height: verticalScale(300),
                                marginBottom: verticalScale(5)
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {                                    
                                    let newImages = [...this.state.choreForm.images];
                                    let imagesToDelete = [...this.state.choreForm.imagesToDelete];
                                    let imagesToUpload = [...this.state.choreForm.imagesToUpload];

                                    if (!this.props.isChoreCreate && item.id) {
                                        imagesToDelete.push(item.id);
                                    }

                                    if (item.uploadId != null) {
                                        let _id = 0;
                                        for (let i = 0; i < imagesToUpload.length; i++) {
                                            if (imagesToUpload[i].uploadId === item.uploadId) {
                                                _id = i;
                                                break;
                                            }
                                        }

                                        imagesToUpload.splice(_id, 1);
                                    }

                                    newImages.splice(index, 1);

                                    this.setState({
                                        choreForm: {
                                            ...this.state.choreForm,
                                            images: newImages,
                                            imagesToDelete,
                                            imagesToUpload
                                        }
                                    })
                                }}
                                style={{
                                    position: 'absolute',
                                    top: verticalScale(10),
                                    right: verticalScale(10),
                                    zIndex: 999
                                }}
                            >
                                <CustomIcon
                                    name={'times'}
                                    style={{
                                        color: '#FFFFFF'
                                    }}
                                    size={verticalScale(24)}
                                />
                            </TouchableOpacity>
                            <CustomImage
                                style={{
                                    width: '100%',
                                    height: verticalScale(300)
                                }}
                                source={{uri: item.image}}
                            />
                        </View>
                    )
                }}
            />
        )
    }

    renderAddImageButton() {
        return (
            <View
                style={{
                    paddingVertical: verticalScale(5)
                }}
            >
                <TouchableOpacity
                    style={{
                        height: verticalScale(100),
                        width: '100%',
                        borderRadius: verticalScale(5),
                        borderWidth: verticalScale(5),
                        borderStyle: 'dashed',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    onPress={() => {
                        this.setState({
                            imagePickerModalVisible: true
                        })
                    }}
                >
                    <CustomText
                        style={{fontSize: verticalScale(36), fontWeight: 'bold'}}
                    >
                        +
                    </CustomText>
                    <CustomText
                        style={{position: 'absolute', bottom: verticalScale(5)}}
                    >
                        Đăng ảnh minh họa (Số lượng: {this.state.choreForm.images.length}/6)
                    </CustomText>
                </TouchableOpacity>
            </View>
        )
    }

    renderWeekRepeatListItem(item) {
        return (
            <CustomSelectBox
                textDate={item.label} 
                checked={this.state.choreForm.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: '13%'}}
                onPress={() => {
                    index = this.state.choreForm.repeatList.indexOf(item.value);
                    newRepeatList = [ ...this.state.choreForm.repeatList ];

                    if (index > -1) {
                        newRepeatList.splice(index, 1);
                    } else {
                        newRepeatList.push(item.value);
                    }

                    this.setState({
                        choreForm: {
                            ...this.state.choreForm,
                            repeatList: newRepeatList
                        }
                    }); 
                }}
            />
        )
    }

    renderMonthRepeatListItem({item}) {
        return (
            <CustomSelectBox
                textDate={item.label} 
                checked={this.state.choreForm.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: verticalScale(40), marginRight: verticalScale(5)}}
                onPress={() => {
                    index = this.state.choreForm.repeatList.indexOf(item.value);
                    newRepeatList = [ ...this.state.choreForm.repeatList ];

                    if (index > -1) {
                        newRepeatList.splice(index, 1);
                    } else {
                        newRepeatList.push(item.value);
                    }

                    this.setState({
                        choreForm: {
                            ...this.state.choreForm,
                            repeatList: newRepeatList
                        }
                    });
                }}
            />
        )
    }

    renderRepeatSelectList() {        
        if (this.state.choreForm.repeat == 'weekly') {
            return (
                <CardRow
                    style={{justifyContent: 'space-between', marginTop: verticalScale(10)}}
                >
                    {
                        weekRepeat.map((item) => this.renderWeekRepeatListItem(item))
                    }
                </CardRow>
            )
        } else if (this.state.choreForm.repeat == 'monthly') {
            return (
                <CardRow
                    style={{marginTop: verticalScale(10)}}
                >
                    <FlatList
                        data={monthRepeat}
                        renderItem={this.renderMonthRepeatListItem.bind(this)}
                        extraData={this.state.choreForm.repeatList}
                        horizontal
                        keyExtractor={(item, index) => index.toString()}
                    />
                </CardRow>
            )
        }
    }

    renderDeleteModal() {
        choreName = "này";
        choreId = -1;
        try {
            choreName = this.state.choreForm.name;
            choreId = this.state.choreForm.id;
        } catch {
            
        }

        return (
            <CustomModal
                headerText={'Xóa công việc'}
                visible={this.state.choreDeleteModalVisible}
                onRequestClose={() => this.setState({choreDeleteModalVisible: false})}
                onConfirm={() => {
                    this.setState({choreDeleteModalVisible: false});
                    this.props.choreDelete(this.props.userToken, [ choreId ]);
                    this.props.navigation.goBack();
                }}
            >
                <CustomText>
                    Bạn có chắc là muốn xóa công việc {choreName} không?
                </CustomText>
            </CustomModal>
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    {/* First part of the chore form */}
                    <Card
                        style={{}}
                    >
                        <CustomTextInput
                            value={this.state.choreForm.name}
                            placeholder={'Tên công việc'}
                            maxLength={80}
                            showIcon
                            inputStyle={{fontSize: verticalScale(18), fontWeight: 'bold'}}
                            containerStyle={{width: '100%', paddingTop: verticalScale(5), paddingBottom: verticalScale(5)}}
                            editSectionStyle={{paddingBottom: verticalScale(5)}}
                            onChangeText={(value) => {
                                this.choreFormUpdate('name', value);
                            }}
                        />
                        <CardRow>
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center', marginTop: verticalScale(5)}} name="clock-o" size={verticalScale(28)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <CustomPicker
                                    value={this.state.choreForm.repeat}
                                    options={[
                                        {
                                            label: 'Loại công việc...',
                                            value: ''
                                        },
                                        {
                                            label: 'Hằng ngày',
                                            value: 'daily'
                                        },
                                        {
                                            label: 'Hằng tuần',
                                            value: 'weekly'
                                        },
                                        {
                                            label: 'Hằng tháng',
                                            value: 'monthly'
                                        }
                                    ]}
                                    containerStyle={{width: '50%', marginTop: verticalScale(5)}}
                                    onValueChange={(value) => {
                                        this.choreFormUpdate('repeat', value);
                                    }}
                                />
                                {
                                    this.state.choreForm.repeat == 'daily' ?
                                    null
                                    :
                                    this.renderRepeatSelectList()
                                }
                                <CardRow
                                    style={{paddingTop: verticalScale(10), paddingBottom: verticalScale(10)}}
                                >
                                    <CustomText
                                        style={{paddingRight: verticalScale(10)}}
                                    >
                                        Ưu tiên công việc này
                                    </CustomText>
                                    <CustomCheckbox
                                        checked={this.state.choreForm.priority}
                                        onPress={() => {
                                            this.choreFormUpdate('priority', !this.state.choreForm.priority);
                                        }}
                                    />
                                </CardRow>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider 
                            containerStyle={{marginTop: 0}}
                        />
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomAvatar
                                    size={verticalScale(28)}
                                    round
                                    url={this.state.choreForm.butler ? this.state.choreForm.butler.profileImage : null}
                                />
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TouchableOpacity
                                    style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}
                                    onPress={() => this.setState({userSelectModalVisible: true})}
                                >
                                    {
                                        !_.isEmpty(this.state.choreForm.butler) ?
                                        <View
                                            style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
                                        >
                                            <CustomText>
                                                {this.state.choreForm.butler.fullname}
                                            </CustomText>
                                            {
                                                _.isEmpty(this.props.choreForm.butler) || this.state.choreForm.butler.id == this.props.choreForm.butler.id ?
                                                (() => {
                                                    if (this.state.choreForm.status == 'init') {
                                                        return (
                                                            <CustomText>
                                                                &nbsp;(chờ)
                                                            </CustomText>    
                                                        )
                                                    }
            
                                                    if (this.state.choreForm.status == 'denied') {
                                                        return (
                                                            <CustomText
                                                                style={{color : '#FF8383'}}
                                                            >
                                                                &nbsp;(từ chối)
                                                            </CustomText>    
                                                        )
                                                    }
            
                                                    return;
                                                })()
                                                :
                                                null
                                            }                  
                                        </View>
                                        : null
                                    }
                                    <View>
                                        <CustomText
                                            style={{color: '#29ABE2', paddingLeft: verticalScale(5)}}
                                        >
                                            Chọn người làm...
                                        </CustomText>
                                    </View>
                                </TouchableOpacity>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider />
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center'}} name="sticky-note" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <TextInput
                                    value={this.state.choreForm.note}
                                    placeholder={'Ghi chú cho công việc này...'}
                                    style={{paddingLeft: 0, fontSize: verticalScale(14)}}
                                    onChangeText={(value) => {
                                        this.choreFormUpdate('note', value);
                                    }}
                                />
                            </CardColumn>
                        </CardRow>
                    </Card>
                    {/* Last part, sample image */}
                    <Card
                        style={{marginBottom: verticalScale(60)}}
                    >
                        <CardRow
                            style={styles.cardRowStyle}
                        >
                            <CardColumn
                                style={styles.columnFirst}
                            >
                                <CustomIcon style={{textAlign: 'center'}} name="camera" size={verticalScale(18)}/>
                            </CardColumn>
                            <CardColumn
                                style={styles.columnSecond}
                            >
                                <CustomText
                                    style={{fontSize: verticalScale(16), fontWeight: 'bold'}}
                                >
                                    Ảnh minh họa
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: 0}} />
                        <CardRow>
                            <View
                                style={{position: 'relative', width: '100%'}}                                
                            >
                                {this.renderImage()}
                                {
                                    !this.state.choreForm.images || this.state.choreForm.images.length < 6 ?
                                    this.renderAddImageButton()
                                    :
                                    null
                                }
                            </View>
                        </CardRow>
                    </Card>
                </ScrollView>
                <CustomButtonFooter
                    title='Lưu thông tin'
                    onPress={() => {
                        let submittedForm = { ...this.state.choreForm };

                        if (this.props.isChoreCreate) {
                            this.props.choreCreate(this.props.userToken, submittedForm);
                        } else {
                            if (submittedForm.butler && this.props.choreForm.butler) {
                                if (submittedForm.butler.id == this.props.choreForm.butler.id) {
                                    delete submittedForm.butler;
                                }
                            }
                        
                            this.props.choreUpdate(this.props.userToken, submittedForm);
                        }

                        this.props.navigation.goBack();
                    }}
                />
                {this.renderDeleteModal()}
                <UserSelectModal
                    visible={this.state.userSelectModalVisible}
                    selectedUser={this.state.choreForm.butler ? this.state.choreForm.butler.id : null}
                    onUserSelect={(item) => {
                        butler = item.user;

                        if (!_.isEmpty(this.state.choreForm.butler)) {
                            if (butler.id == this.state.choreForm.butler.id) {
                                this.setState({
                                    choreForm: {
                                        ...this.state.choreForm,
                                        butler: null
                                    },
                                    userSelectModalVisible: false
                                });
                            } else {
                                this.setState({
                                    choreForm: {
                                        ...this.state.choreForm,
                                        butler
                                    },
                                    userSelectModalVisible: false
                                });
                            }
                        } else {
                            this.setState({
                                choreForm: {
                                    ...this.state.choreForm,
                                    butler
                                },
                                userSelectModalVisible: false
                            });
                        }
                    }}
                    onRequestClose={() => this.setState({userSelectModalVisible: false})}
                />
                {this.renderImagePickerModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    columnFirst: {
        width: '10%'
    },
    columnSecond: {
        width: '90%'
    },
    cardRowStyle: {
        paddingVertical: verticalScale(5),
        justifyContent: 'center',
        alignItems: 'center'
    }
}

const mapStateToProps = ({ auth, house, user }) => {
    choreForm = {
        house: '',
        room: '',
        name: '',
        repeat: '',
        repeatList: [],
        priority: false,
        note: '',
        images: [],
        imageFile: null,
        butler: {}
    }

    if (!house.isChoreCreate) {
        choreForm = house.choreForm;
    }

    return {
        userToken: auth.token.userToken,
        selectedHouseArrayId: house.selectedHouseArrayId,
        selectedHouseId: house.houses[house.selectedHouseArrayId] ? house.houses[house.selectedHouseArrayId].id : choreForm.house,
        selectedRoomArrayId: house.selectedRoomArrayId,
        selectedRoomId: house.rooms[house.selectedRoomArrayId] ? house.rooms[house.selectedRoomArrayId].id : choreForm.room,
        selectedChoreArrayId: house.selectedChoreArrayId,
        isChoreCreate: house.isChoreCreate,
        choreForm,
        usersInContact: user.usersInContact
    }
}

export default connect(mapStateToProps, {
    choreDelete,
    choreUpdate,
    choreCreate,
    houseReducerUpdate
})(HouseChoreFormScene);