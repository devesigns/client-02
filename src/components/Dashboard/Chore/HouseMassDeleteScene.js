import React, { Component } from 'react';
import { 
    View,
    FlatList,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Text,
    ScrollView,
    LayoutAnimation
} from 'react-native';
import {
    Card,
    CardRow,
    CardColumn,
    CustomCheckbox,
    CardTouchable,
    CustomText,
    CustomIcon,
    CustomDivider,
    CustomButtonFooter
} from '../../common';
import { connect } from 'react-redux';
import {
    choreDelete
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';
import {
    slideAnimationConfig
} from '../../../const';

class RoomCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expandControlRoomList: { ...this.props.expandControlRoomList }
        }
    }

    componentWillMount() {
        // LayoutAnimation.configureNext(fadeAnimationConfig);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            expandControlRoomList: { ...nextProps.expandControlRoomList }
        })
    }

    renderRoomChoreItem = ({item, index}) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.onRoomChoreCheckboxToggle(item.id)}
            >
                <CardRow
                    style={{
                        paddingTop: verticalScale(5)
                    }}
                >
                    <CardColumn
                        style={{width: '7%', paddingLeft: 0}}
                    >
                        <CustomCheckbox
                            checked={this.props.selectedChoreArray.includes(item.id)}
                            onPress={() => this.props.onRoomChoreCheckboxToggle(item.id)}
                        />
                    </CardColumn>
                    <CardColumn
                        style={{width: '93%'}}
                    >
                        <CustomText
                            style={{fontSize: verticalScale(14)}}
                        >
                            {item.name}
                        </CustomText>
                    </CardColumn>
                </CardRow>
            </TouchableOpacity>
        )
    }

    render() {
        let item = this.props.item;
        console.log('chore', this.props.selectedChoreArray);

        if (item.filteredChores.length > 0) {
            return (
                <Card>       
                    <CardRow
                        style={{
                            paddingTop: verticalScale(5)
                        }}
                    >
                        <CardColumn
                            style={{
                                width: '7%',
                                alignItems: 'center',
                                paddingRight: 0,
                            }}
                        >
                            <CustomCheckbox
                                checked={this.props.selectedRoomArray.includes(item.id)}
                                onPress={() => this.props.onRoomCheckboxToggle(this.props.roomArrayId)}
                            />
                        </CardColumn>
                        <CardColumn
                            style={{
                                width: '93%',
                                paddingRight: 0
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                                onPress={() => {
                                    this.props.onRoomExpandToggle(item.id);
                                }}
                            >
                                <CustomText
                                    style={{width: '90%', fontWeight: 'bold'}}
                                >
                                    {item.name}
                                </CustomText>
                                {
                                    this.state.expandControlRoomList[item.id].expanded ?
                                    <CustomIcon
                                        name='minus-circle'
                                        size={verticalScale(16)}
                                        style={{
                                            position: 'absolute',
                                            right: 0,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}  
                                    />
                                    :
                                    <CustomIcon
                                        name='plus-circle'
                                        size={verticalScale(16)}
                                        style={{
                                            position: 'absolute',
                                            right: 0,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}  
                                    />
                                }
                            </TouchableOpacity>
                        </CardColumn>
                    </CardRow>
                    <CardRow>
                        {
                            this.state.expandControlRoomList[item.id].expanded ?
                            <View>
                                <CustomDivider containerStyle={{marginTop: verticalScale(5), marginBottom: verticalScale(5)}} />
                                <FlatList
                                    data={item.filteredChores}
                                    renderItem={this.renderRoomChoreItem}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            :
                            null
                        }
                    </CardRow>
                </Card>
            )
        } else {
            return null;
        }
    }
}

class HouseMassAssignScene extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedUserId: '',
            expandControlRoomList: {},
            selectedChoreArray: [],
            selectedRoomArray: [],
            viewMode: 'daily',
        }
    }

    componentWillMount() {
        if (this.props.houseForm.id) {
            expandControlRoomList = {};
            for (i = 0; i < this.props.houseForm.rooms.length; i++) {
                expandControlRoomList[this.props.houseForm.rooms[i].id] = {
                    expanded: true
                }
            }

            this.setState({
                expandControlRoomList: expandControlRoomList
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.houseForm.id) {
            expandControlRoomList = {};
            for (i = 0; i < nextProps.houseForm.rooms.length; i++) {
                expandControlRoomList[nextProps.houseForm.rooms[i].id] = {
                    expanded: true
                }
            }

            this.setState({
                expandControlRoomList: expandControlRoomList
            })
        }
    }

    setViewMode(viewMode) {
        this.setState({
            viewMode
        })
    }
    
    renderRoomItem = ({item, index}) => {
        item.filteredChores = item.chores.filter((val, index) => {
            return val.repeat == this.state.viewMode;
        })

        return (
            <RoomCard
                item={item}
                roomArrayId={index}
                selectedChoreArray={this.state.selectedChoreArray}
                selectedRoomArray={this.state.selectedRoomArray}
                expandControlRoomList={this.state.expandControlRoomList}
                onRoomExpandToggle={(roomId) => {
                    console.log(roomId)
                    expandControlRoomList = { ...this.state.expandControlRoomList };
                    expandControlRoomList[roomId].expanded = !expandControlRoomList[roomId].expanded;

                    LayoutAnimation.configureNext(slideAnimationConfig);

                    this.setState({
                        expandControlRoomList
                    })
                }}
                onRoomChoreCheckboxToggle={(choreId) => {
                    selectedChoreArray = [ ...this.state.selectedChoreArray ];
                    selectedRoomArray = [ ...this.state.selectedRoomArray ];
                    choreIndex = selectedChoreArray.indexOf(choreId);

                    if (choreIndex >= 0) {
                        selectedChoreArray.splice(choreIndex, 1);
                        selectedRoomArray.splice(selectedRoomArray.indexOf(item.id) , 1);
                    } else {
                        selectedChoreArray.push(choreId);
                        fullRoomSelected = true;

                        for (i = 0; i < item.filteredChores.length; i++) {
                            if (selectedChoreArray.indexOf(item.filteredChores[i].id) < 0) {
                                fullRoomSelected = false;
                            }
                        }

                        if (fullRoomSelected) {
                            if (selectedRoomArray.indexOf(item.id) < 0) {
                                selectedRoomArray.push(item.id);
                            }
                        }
                    }

                    this.setState({
                        selectedChoreArray,
                        selectedRoomArray
                    })
                }}
                onRoomCheckboxToggle={() => {
                    selectedChoreArray = [ ...this.state.selectedChoreArray ];
                    selectedRoomArray = [ ...this.state.selectedRoomArray ];

                    roomIndex = selectedRoomArray.indexOf(item.id);
                    if (roomIndex >= 0) {
                        selectedRoomArray.splice(roomIndex, 1);

                        for (i = 0; i < item.filteredChores.length; i++) {
                            selectedChoreArray.splice(selectedChoreArray.indexOf(item.filteredChores[i].id), 1);
                        }
                    } else {
                        selectedRoomArray.push(item.id);

                        for (i = 0; i < item.filteredChores.length; i++) {
                            choreIndex = selectedChoreArray.indexOf(item.filteredChores[i].id);

                            if (choreIndex < 0) {
                                selectedChoreArray.push(item.filteredChores[i].id);
                            }
                        }     
                    }

                    this.setState({
                        selectedChoreArray,
                        selectedRoomArray
                    })
                }}
            />
        )
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <View
                    style={styles.choreTabContainerStyle}
                >
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('daily')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'daily' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'daily' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng ngày
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('weekly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'weekly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'weekly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tuần
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => this.setViewMode('monthly')}
                    >
                        <View
                            style={[styles.choreTabStyle, this.state.viewMode == 'monthly' ? {backgroundColor: '#FFFFFF'} : {backgroundColor: '#F2F2F2'}]}
                        >
                            <CustomText
                                style={[styles.choreTabTextStyle, this.state.viewMode == 'monthly' ? null : {color: "rgba(33, 33, 33, 0.3)"}]}
                            >
                                Hằng tháng
                            </CustomText>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <ScrollView>
                    {
                        this.props.houseForm.rooms.length > 0 ?
                        <FlatList
                            data={this.props.houseForm.rooms}
                            renderItem={this.renderRoomItem}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        :
                        <Text
                            style={{textAlign: 'center', paddingTop: verticalScale(30)}}
                        >
                            Chưa có công việc.  
                        </Text>
                    }
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title='Xóa'
                    onPress={() => {
                        this.props.choreDelete(this.props.userToken, this.state.selectedChoreArray);
                        this.props.navigation.goBack();
                    }}
                />
            </View>
        )
    }
}

const styles = {
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    toolColumnStyle: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    toolIconContainerStyle: {
        marginLeft: verticalScale(15)
    },
    choreTabContainerStyle: {
        flexDirection: 'row',
        shadowColor: '#333333',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1.0,
        shadowRadius: 3,
        elevation: 3,
        backgroundColor: '#FFFFFF'
    },
    choreTabStyle: {
        width: '33.33%',
        padding: verticalScale(10),
        alignItems: 'center',
        justifyContent: 'center',
        height: verticalScale(40)
    },
    choreTabTextStyle: {
        fontWeight: 'bold'
    },
}

const mapStateToProps = ({ auth, house }) => {
    return {
        userToken: auth.token.userToken,
        houseForm: house.houseForm
    }
}

export default connect(mapStateToProps, {
    choreDelete
})(HouseMassAssignScene);