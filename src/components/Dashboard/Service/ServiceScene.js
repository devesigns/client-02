import React, { Component } from 'react';
import { 
    View,
    FlatList,
    TouchableOpacity,
    Linking,
    TextInput,
    RefreshControl
} from 'react-native';
import { 
    CardTouchable,
    CardRow,
    CardColumn,
    CustomAvatar,
    CustomText,
    CustomDivider,
    Card,
    CustomIcon,
    CustomModal
} from '../../common';
import { connect } from 'react-redux';
import {
    serviceProviderFetch,
    serviceProviderSelect,
    userReducerUpdate,
    contactUserCreate,
    usersInContactFetch
} from '../../../actions';
import { verticalScale } from '../../../utils';
import { ScrollView } from 'react-native-gesture-handler';

class ServiceScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newContactPhone: '',
            addContactModalVisible: false,
            refreshing: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.serviceProviderFetch(this.props.userToken);
    }

    renderServiceProviderItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    this.props.serviceProviderSelect(item.id);
                    this.props.navigation.navigate('ServiceList');
                }}
            >
                <CardRow>
                    <CardColumn style={{width: '20%'}}>
                        <CustomAvatar
                            size={verticalScale(50)}
                            round
                            url={item.image}
                        />
                    </CardColumn>
                    <CardColumn style={{width: '80%', justifyContent: 'center'}}>
                        <CustomText
                            style={{fontSize: verticalScale(16), fontWeight: 'bold'}}
                        >
                            {item.title}
                        </CustomText>
                        <CustomText>{item.description}</CustomText>
                    </CardColumn>
                </CardRow>
            </CardTouchable>
        )
    }

    renderContactUserItem = ({item, index}) => {
        let user = item.user;
        
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.userReducerUpdate('contactArrayId', index);
                    this.props.userReducerUpdate('contactUserProfile', user);
                    this.props.navigation.navigate('ServiceContactUserProfileScene');
                }}
                style={{width: '100%'}}
            >
                <CardRow
                    style={{alignItems: 'center'}}
                >
                    <CardColumn style={{width: '20%'}}>
                        <CustomAvatar
                            size={verticalScale(50)}
                            round
                            url={user.profileImage}
                        ></CustomAvatar>
                    </CardColumn>
                    <CardColumn style={{ width: '80%', justifyContent: 'center' }}>
                        <CustomText
                            style={{fontSize: verticalScale(16), fontWeight: 'bold'}}
                        >
                            {user.fullname}
                        </CustomText>
                        <CustomText>
                            {user.cellphone}
                        </CustomText>
                    </CardColumn>
                    <TouchableOpacity
                        onPress={() => {
                            Linking.openURL('tel:' + user.cellphone);
                        }}
                        style={{position: 'absolute', right: 0}}
                    >
                        <CustomIcon name='phone' size={verticalScale(24)} />
                    </TouchableOpacity>
                </CardRow>
                {
                    index < this.props.usersInContact.length - 1 ?
                    <CustomDivider />
                    :
                    null
                }
            </TouchableOpacity>
        )
    }

    renderAddContactModal() {
        return (
            <CustomModal
                visible={this.state.addContactModalVisible}
                onRequestClose={() => {
                    this.setState({
                        addContactModalVisible: false
                    })
                }}
                onConfirm={() => {
                    this.props.contactUserCreate(this.props.userToken, this.state.newContactPhone);
                    this.setState({
                        addContactModalVisible: false
                    })
                }}
            >
                <CardRow
                    style={{
                        alignItems: 'center'
                    }}
                >
                    <CardColumn
                        style={{width: '20%', justifyContent: 'center'}}
                    >
                        <CustomIcon name='plus-circle' size={verticalScale(60)} style={{color: '#F2F2F2'}} />
                    </CardColumn>
                    <CardColumn
                        style={{width: '80%', justifyContent: 'center'}}
                    >
                        <TextInput
                            value={this.state.newPhoneInput}
                            placeholder={'Thêm SĐT mới...'}
                            style={{paddingLeft: 0, fontSize: verticalScale(18)}}
                            onChangeText={(value) => {
                                this.setState({
                                    newContactPhone: value
                                })
                            }}
                            keyboardType='numeric'
                            maxLength={12}
                        />
                    </CardColumn>
                </CardRow>
            </CustomModal>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                })
            }, 500);
        })

        this.props.serviceProviderFetch(this.props.userToken);
        this.props.usersInContactFetch(this.props.userToken);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    <FlatList
                        data={this.props.service.serviceProviders}
                        renderItem={this.renderServiceProviderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                    <CustomDivider containerStyle={{marginBottom: verticalScale(5), marginTop: verticalScale(20)}} />
                    <Card>
                        <CardRow
                            style={{
                                borderBottomWidth: 0.5,
                                borderColor: 'rgba(33, 33, 33, 0.3)',
                                paddingBottom: verticalScale(10),
                                marginBottom: verticalScale(15),
                                alignItems: 'center'
                            }}
                        >
                            <CustomText
                                style={{
                                    fontWeight: 'bold',
                                    fontSize: verticalScale(18),
                                }}
                            >
                                Danh bạ người thực hiện công việc
                            </CustomText>
                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 0
                                }}
                                onPress={() => {
                                    this.setState({
                                        newContactPhone: '',
                                        addContactModalVisible: true
                                    })
                                }}
                            >
                                <CustomIcon
                                    name='plus'
                                    size={verticalScale(24)}
                                />
                            </TouchableOpacity>
                        </CardRow>
                        {
                            this.props.usersInContact.length < 1 ?
                            <CustomText>
                                Hiện chưa có người trong danh bạ!
                            </CustomText>
                            :
                            null
                        }
                        <FlatList
                            data={this.props.usersInContact}
                            renderItem={this.renderContactUserItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </Card>
                </ScrollView>
                {this.renderAddContactModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    }
}

const mapStateToProps = ({ service, auth, user }) => {
    return {
        userToken: auth.token.userToken,
        service,
        usersInContact: user.usersInContact
    }
}

export default connect(mapStateToProps, {
    contactUserCreate,
    serviceProviderFetch,
    serviceProviderSelect,
    userReducerUpdate,
    usersInContactFetch
})(ServiceScene);