import React, { Component } from "react";
import {
    View,
    StyleSheet,
    TextInput,
    Alert,
    FlatList,
    ScrollView
} from "react-native";
import {
    Card,
    CardRow,
    CardColumn,
    CustomSelectBox,
    CustomDateTimePicker,
    CustomIcon,
	CustomText,
    CustomDivider,
    CustomPicker,
    CustomButtonFooter
} from "../../common";
import { connect } from "react-redux";
import { serviceOrderCreate, serviceOrder } from "../../../actions";
import {
    weekRepeat,
    monthRepeat
} from '../../../const';
import { verticalScale } from '../../../utils';
import { StackActions, NavigationActions } from 'react-navigation';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F2F2F2"
    },
    columnFirst: {
        width: "10%"
    },
    columnSecond: {
        width: "90%"
    },
    columnDate: {
        width: "50%"
    },
    styleTextInput: {
        fontSize: verticalScale(14),
        paddingLeft: 0,
        paddingBottom: verticalScale(5),
        paddingTop: verticalScale(5)
    },
    errorTextInput: {
        fontSize: verticalScale(14),
        borderColor: "#ff0000",
        borderBottomWidth: 1,
        paddingLeft: 0,
        paddingBottom: 0,
        paddingTop: 0
    },
    errorMessage: {
        color: "#ff0000",
        fontSize: verticalScale(14)
    },
    errorMessageDate: {
        borderColor: "#ff0000",
        color: "#ff0000",
        fontSize: verticalScale(14)
    },
    buttonTextStyle: {
        color: "#111111",
        fontSize: verticalScale(14),
        lineHeight: verticalScale(20)
    }
});

class ServiceDetailsScene extends Component {
    state = {
        serviceOrder: []
    };

    constructor(props) {
        super(props);
        this.state = {
            address: "",
            addressValid: true,
            date: "",
            dateValid: true,
            time: "",
            timeValid: true,
            repeat: 'none',
            repeatList: [],
            note: ""
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if(nextProps.service.serviceOrder.orderId){
            const resetAction = StackActions.reset({
                index: 1,
                actions: [
                    NavigationActions.navigate({ routeName: 'ServiceHome' }),
                    NavigationActions.navigate({ routeName: 'ServiceComplete' })
                ],
            });
            this.props.navigation.dispatch(resetAction);
        }
    }

    validate = (value, type) => {
        if (type === "date") {
            console.log(value);
            if (this.state.time !== '') {
                this.setState({
                    timeValid: true
                })
            }
            date = {
                ...this.state,
                date: value,
                dateValid: true
            };
            this.setState(date);
        }
        if (type === "time") {
            time = {
                ...this.state,
                time: value,
                timeValid: true
            };
            this.setState(time);
        }
        if (type === 'address') {
            if (value.trim() != 0) {
                address = {
                    ...this.state,
                    address: value,
                    addressValid: true
                };
                this.setState(address);
            } else {
                address = {
                    ...this.state,
                    addressValid: false
                };
                this.setState(address);
            }
        }
    };

    renderWeekRepeatListItem(item) {
        return (
            <CustomSelectBox
                textDate={item.label} 
                checked={this.state.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: '13%'}}
                onPress={() => {
                    index = this.state.repeatList.indexOf(item.value);
                    newRepeatList = [ ...this.state.repeatList ];

                    if (index > -1) {
                        newRepeatList.splice(index, 1);
                    } else {
                        newRepeatList.push(item.value);
                    }

                    this.setState({
                        repeatList: newRepeatList
                    }); 
                }}
            />
        )
    }

    renderMonthRepeatListItem({item}) {
        return (
            <CustomSelectBox
                textDate={item.label} 
                checked={this.state.repeatList.indexOf(item.value) > -1}
                valid={false}
                style={{width: verticalScale(40), marginRight: verticalScale(5)}}
                onPress={() => {
                    index = this.state.repeatList.indexOf(item.value);
                    newRepeatList = [ ...this.state.repeatList ];

                    if (index > -1) {
                        newRepeatList.splice(index, 1);
                    } else {
                        newRepeatList.push(item.value);
                    }

                    this.setState({
                        repeatList: newRepeatList
                    });
                }}
            />
        )
    }

    renderRepeatSelectList() {
        if (this.state.repeat == 'weekly') {
            return (
                <CardRow
                    style={{justifyContent: 'space-between', marginTop: verticalScale(10)}}
                >
                    {
                        weekRepeat.map((item) => this.renderWeekRepeatListItem(item))
                    }
                </CardRow>
            )
        } else if (this.state.repeat == 'monthly') {
            return (
                <CardRow
                    style={{marginTop: verticalScale(10)}}
                >
                    <FlatList
                        data={monthRepeat}
                        renderItem={this.renderMonthRepeatListItem.bind(this)}
                        extraData={this.state.repeatList}
                        horizontal
                        keyExtractor={(item, index) => index.toString()}
                    />
                </CardRow>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Card>
                        <CardRow>
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon
                                    style={{ textAlign: "center", paddingTop: verticalScale(10) }}
                                    name="map-marker"
                                    size={verticalScale(24)}
                                />
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <TextInput
                                    style={[   
                                        this.state.addressValid
                                            ? styles.styleTextInput
                                            : styles.errorTextInput
                                        ,
                                        {
                                            borderBottomWidth: 0.5
                                        }
                                    ]}
                                    placeholder={"Địa chỉ làm việc"}
                                    onChangeText={value => {
                                        this.validate(value, "address");
                                    }}
                                    value={null}
                                />
                                {this.state.addressValid == false ? (
                                    <CustomText style={styles.errorMessage}>
                                        * Địa chỉ chưa nhập
                                    </CustomText>
                                ) : null}
                                <CustomText style={{ fontSize: verticalScale(14), marginTop: verticalScale(5) }}>
                                    Vui lòng nhập chính xác địa chỉ để người giúp việc có thể
                                    tìm được nhà bạn đúng giờ
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider />
                        <CardRow>
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon
                                    style={{ textAlign: "center" }}
                                    name="calendar"
                                    size={verticalScale(24)}
                                />
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CardRow
                                    style={{marginBottom: 0}}
                                >
                                    <CardColumn style={[styles.columnDate, {paddingLeft: 0}]}>
                                        <CustomDateTimePicker
                                            mode="time"
                                            label="Thời gian bắt đầu"
                                            placeholder="00:00 AM"
                                            value={this.state.time}
                                            onValueChange={value => {
                                                this.validate(value, "time");
                                            }}
                                            buttonStyle={
                                                this.state.timeValid
                                                    ? styles.buttonTextStyle
                                                    : styles.errorMessageDate
                                            }
                                        />
                                    </CardColumn>
                                    <CardColumn style={[styles.columnDate, {paddingRight: 0}]}>
                                        <CustomDateTimePicker
                                            mode="date"
                                            label="Ngày làm việc"
                                            placeholder="MM/DD/YYYY"
                                            value={this.state.date}
                                            minimumDate={(() => {
                                                let today = new Date();
                                                let tmr = new Date();
                                                tmr.setDate(today.getDate() + 1);
                                                
                                                return tmr;
                                            })()}
                                            onValueChange={(value) => {
                                                this.validate(value, "date");
                                            }}
                                            buttonStyle={
                                                this.state.dateValid
                                                    ? styles.buttonTextStyle
                                                    : styles.errorMessageDate
                                            }
                                            style={{ flex: 1 }}
                                        />
                                    </CardColumn>
                                </CardRow>
                                <CardRow
                                    style={{marginBottom: 0}}
                                >
                                    <CardColumn style={styles.columnDate}>
                                        {this.state.timeValid == false ? (
                                            <CustomText style={styles.errorMessage}>
                                                * Vui lòng chọn thời gian bắt đầu
                                            </CustomText>
                                        ) : null}
                                    </CardColumn>
                                    <CardColumn style={styles.columnDate}>
                                        {this.state.dateValid == false ? (
                                            <CustomText style={styles.errorMessage}>
                                                * Vui lòng chọn ngày bắt đầu
                                            </CustomText>
                                        ) : null}
                                    </CardColumn>
                                </CardRow>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider />
                        <CardRow>
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon
                                    style={{ textAlign: "center" }}
                                    name="repeat"
                                    size={verticalScale(24)}
                                />
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText style={{ fontSize: verticalScale(10) }}>Lặp lại</CustomText>
                                <CardRow
                                    style={{width: '50%'}}
                                >
                                    <CustomPicker
                                        value={this.state.repeat}
                                        onValueChange={(value) => {
                                            let date = new Date();
                                            let newRepeatList = [];
                                            
                                            if (value == 'weekly') {
                                                newRepeatList.push(date.getDay());
                                            } else if (value == 'monthly') {
                                                newRepeatList.push(date.getDate());
                                            }

                                            this.setState({
                                                repeat: value,
                                                repeatList: newRepeatList
                                            })
                                        }}
                                        options={[
                                            {
                                                label: 'Không lặp lại',
                                                value: 'none'
                                            },
                                            {
                                                label: 'Hằng tuần',
                                                value: 'weekly'
                                            },
                                            {
                                                label: 'Hằng tháng',
                                                value: 'monthly'
                                            }
                                        ]}
                                        style={{width: '50%'}}
                                        itemStyle={{fontSize: 12}}
                                    />
                                </CardRow> 
                                {
                                    this.state.repeat == '' ?
                                    null
                                    :
                                    this.renderRepeatSelectList()
                                }
                            </CardColumn>
                        </CardRow>
                        <CustomDivider />
                        <CardRow style={{ alignItems: "center" }}>
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon
                                    style={{ textAlign: "center" }}
                                    name="edit"
                                    size={verticalScale(24)}
                                />
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <TextInput
                                    style={styles.styleTextInput}
                                    placeholder={"Ghi chú cho người giúp việc"}
                                    onChangeText={value => {
                                        note = {
                                            ...this.state,
                                            note: value
                                        };

                                        this.setState(note);
                                    }}
                                    value={null}
                                />
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title="Thuê dịch vụ"
                    onPress={() => {
                        if (this.state.address === "") {
                            this.setState({ addressValid: false });
                        }
                        if (this.state.date === "") {
                            this.setState({ dateValid: false });
                        }
                        if (this.state.time === "") {
                            this.setState({ timeValid: false });
                        }
                        
                        if (this.state.date !== '' && this.state.time !== '' &&
                            !this.state.address !== '') {
                                let dateString = `${this.state.date.getMonth() + 1}/${this.state.date.getDate()}/${this.state.date.getFullYear()}`;
                                const stringDate = dateString + ' ' + this.state.time;
                                try {
                                    let dateSelected = new Date(stringDate);
                                    let date = new Date(Date.now() + 6*60*60*1000);
                                    if (dateSelected.getTime() < date.getTime()) {
                                        this.setState({
                                            timeValid: false,
                                            dateValid: false
                                        });
                                        Alert.alert(
                                            "Thông báo",
                                            "Vui lòng chọn thời gian bắt đầu dịch vụ sau 6 tiếng kể từ thời điểm này",
                                            [{ text: "OK" }],
                                            { cancelable: true }
                                        )
                                    } else {
                                        let repeatListSorted = this.state.repeatList;
                                        this.setState({
                                            repeatList: repeatListSorted.sort()
                                        });
                                        let serviceOrder = {
                                            serviceList: this.props.service.selectedServiceList,
                                            detail: {
                                                ...this.state,
                                                date: dateString
                                            }
                                        };
                                        
                                        this.props.serviceOrderCreate(serviceOrder, this.props.userToken, this.props.service.selectedProvider);
                                    }
                                } catch (error) {
                                    alert(error);
                                }
                                
                            }
                    }}
                />
            </View>
        );
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        userToken: auth.token.userToken,
        service
    };
};

export default connect(
    mapStateToProps,
    {
        serviceOrder,
        serviceOrderCreate
    }
)(ServiceDetailsScene);
