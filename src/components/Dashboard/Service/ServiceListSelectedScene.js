import React, { Component } from 'react';
import {
    View,
    FlatList,
    StyleSheet,
    TextInput,
    ScrollView
} from 'react-native';
import { 
    Card,
    CardRow,
    CardColumn,
    CustomTextInput,
    CustomText,
    CustomButtonFooter
} from '../../common';
import { connect } from 'react-redux';
import {
    serviceListSelect,
    selectedServiceList
} from '../../../actions/ServiceActions';
import {
    formatPrice
} from '../../../utils/FormatUtils';
import { verticalScale } from '../../../utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    cardRowHeader: {
        paddingTop: verticalScale(5),
        paddingBottom: verticalScale(15), 
        borderBottomWidth: 0.5,
    },
    columnFirst: {
        width: '40%'
    },  
    columnSecond: {
        width: '25%'
    },  
    columnThree: {
        width: '20%'
    },  
    columnFour: {
        width: '15%',
        alignItems: 'center'
    },  
    columnSumText: {
        width: '50%',
        justifyContent: 'center', 
        alignItems: 'flex-start'
    },
    columnSumValue: {
        width: '50%',
        justifyContent: 'center', 
        alignItems: 'flex-end'
    }
});


class ServiceListSelectedScene extends Component {

    state = {
        selectedServiceList: []
    }

    UNSAFE_componentWillMount() {
        let _selectedServiceList = [ ...this.props.service.selectedServiceList ];

        for (i = 0; i < _selectedServiceList.length; i++) {
            _selectedServiceList[i].quantity = '1';
        }
        
        this.setState({
            selectedServiceList: _selectedServiceList
        });
    }

    calSummary() {
        let summaryService = 0;
        let selectedServiceList = [ ...this.state.selectedServiceList ];

        for (i = 0; i < selectedServiceList.length; i++) {
            summaryService = summaryService + (selectedServiceList[i].price * selectedServiceList[i].quantity);
        }
        summaryService = summaryService.toString();
        return summaryService;
    }

    renderItem = ({item}) => (
        <CardRow
            style={{alignItems: 'center'}}
        >
            <CardColumn style ={styles.columnFirst}>
                <CustomText style={{fontWeight: 'bold'}}>
                    {item.title}
                </CustomText>
            </CardColumn>
            <CardColumn style ={styles.columnSecond}>
                <CustomText>{formatPrice(item.price.toString(), 3)}đ</CustomText>
            </CardColumn>
            <CardColumn style ={styles.columnThree}>
                <CustomTextInput
                    value={item.quantity}
                    inputStyle={{textAlign: 'center', paddingBottom: 0}}
                    onChangeText={(value) => {
                        let updatedServiceList = [ ...this.state.selectedServiceList ];
                        for (i = 0; i < updatedServiceList.length; i++) {
                            if (updatedServiceList[i].id == item.id) {
                                updatedServiceList[i].quantity = value;
                            }
                        }

                        this.setState({
                            selectedServiceList: updatedServiceList
                        })
                    }}
                    keyboardType={'numeric'}
                />
            </CardColumn>
            <CardColumn style ={styles.columnFour}>
                <CustomText>{item.unit}</CustomText>
            </CardColumn>
        </CardRow>
    );

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Card>
                        <CardRow style={styles.cardRowHeader}>
                            <CardColumn style ={styles.columnFirst}>
                                <CustomText>Dịch vụ</CustomText>
                            </CardColumn>
                            <CardColumn style ={styles.columnSecond}>
                                <CustomText>Đơn giá</CustomText>
                            </CardColumn>
                            <CardColumn style ={styles.columnThree}>
                                <CustomText>Số lượng</CustomText>
                            </CardColumn>
                            <CardColumn style ={styles.columnFour}>
                                <CustomText>Đơn vị</CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow>
                            <FlatList
                                data={this.state.selectedServiceList}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </CardRow>
                    </Card>
                    <Card>
                        <CardRow>
                            <CardColumn style={styles.columnSumText}>
                                <CustomText style= {{fontWeight: 'bold'}}>Tổng</CustomText>
                            </CardColumn>
                            <CardColumn style={styles.columnSumValue}>
                                <CustomText style= {{color: '#ff0000',  fontWeight: 'bold'}}>{formatPrice(this.calSummary(), 3) +'đ'}</CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                </ScrollView>
                <CustomButtonFooter
                    title='Tiếp theo'
                    onPress={() => this.props.navigation.navigate('ServiceDetails') }
                />
            </View>
        )
    }
}

const mapStateToProps = ({ service }) => {
    return {
        service
    }
}

export default connect(mapStateToProps, {
    serviceListSelect,
    selectedServiceList
})(ServiceListSelectedScene);

