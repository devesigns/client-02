import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    FlatList,
} from 'react-native';
import { 
    Card,
    CardRow,
    CardColumn,
    CustomIcon,
    CustomText,
    CustomDivider,
    CustomButtonFooter,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';

import {
    verticalScale,
    formatPrice
} from '../../../utils';

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    columnFirst: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    columnSecond: {
        width: '90%' 
    },
    contentContainer: {
        paddingVertical: verticalScale(30),
        flex: 1
    },
    columnLeft: {
        width: '50%',
        alignItems: 'flex-start',
        paddingLeft: 0
    },
    columnRight: {
        width: '50%',
        alignItems: 'flex-end',
        paddingRight: 0
    }
});

import {
    serviceOrder
} from '../../../actions/ServiceActions';

import {
    weekRepeat,
    monthRepeat
} from './../../../const';

class ServiceCompleteScene extends Component {

    constructor(props){
        super(props);
    }

    state = {
        serviceOrder: [],
        selectedServiceList: []
    }
    
    calSummary() {
        let summaryService = 0;
        let selectedServiceList = [ ...this.props.service.serviceOrder.serviceList ];

        for (i = 0; i < selectedServiceList.length; i++) {
            summaryService= summaryService + (selectedServiceList[i].price * selectedServiceList[i].quantity);
        }
        summaryService = summaryService.toString();
        return summaryService;
    }

    renderRepeatList() {
        let repeatListString = '';
        let repeatList = this.props.service.serviceOrder.detail.repeatList;
        const repeat = this.props.service.serviceOrder.detail.repeat;
        if (repeat == 'daily') {
            return 'Hằng ngày';
        } else if (repeat == 'weekly') {
            if (repeatList.length == 7) {
                repeatListString = 'Cả tuần';
                return repeatListString;
            } else {
                repeatListString = 'Hằng tuần vào ';
                for (i = 0; i < repeatList.length; i++) {
                    repeatListString += weekRepeat[repeatList[i]].label;

                    if (i != repeatList.length - 1) {
                        repeatListString += ', ';
                    }
                }
            }
        } else if (repeat == 'monthly') {
            if (repeatList.length == 31) {
               repeatListString = 'Cả tháng';
               return repeatListString; 
            } else {
                repeatListString = 'Hằng tháng vào ngày ';
                for (i = 0; i < repeatList.length; i++) {
                    repeatListString += monthRepeat[repeatList[i] - 1].label;
                    if (i != repeatList.length - 1) {
                        repeatListString += ', ';
                    }
                }
            }
        } else {
            return 'Không';
        }

        return repeatListString;
    }

    renderItem = ({item}) => (
        <CardRow>
            <CardColumn style ={styles.columnLeft}>
                <CustomText style={{fontWeight: 'bold'}}>
                    {item.title}
                </CustomText>
            </CardColumn>
            <CardColumn style ={styles.columnRight}>
                <CustomText>{formatPrice(item.price.toString(), 3)} x {item.quantity} {item.unit}</CustomText>
            </CardColumn>
        </CardRow>
    );
   
    render() {
        return (
            <View style={styles.containerStyle}> 
                <ScrollView
                    style={{flex: 1}}
                >
                    <CustomImage 
                        style={{width: verticalScale(200), height: verticalScale(200), alignSelf: 'center', marginBottom: verticalScale(-30), marginTop: verticalScale(30)}}
                        source={require('../../../../assets/img/home-illustration.png')}
                        resizeMode={'contain'}
                    />
                    <Card>
                        <CustomText
                            style={{}}
                        >
                            Cảm ơn bạn đã tin tưởng!
                        </CustomText>
                        <CustomText style={{}}>
                            Giúp Việc Xanh sẽ liên hệ với bạn trong vòng 4-6h nữa để xác nhận nhu cầu công việc.
                        </CustomText>
                    </Card>
                    <Card>
                        <CardRow
                            style={{alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="map-marker" size={verticalScale(30)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    {this.props.service.serviceOrder.detail.address}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="calendar" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    Thời gian bắt đầu {this.props.service.serviceOrder.detail.time},
                                    ngày {this.props.service.serviceOrder.detail.date}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="check-square-o" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    Định kỳ: {this.renderRepeatList()}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="edit" size={verticalScale(24)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    {this.props.service.serviceOrder.detail.note ? this.props.service.serviceOrder.detail.note : 'Không có ghi chú'}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                    
                    <Card>
                        <CardRow
                            style={{borderBottomWidth: 0.5, paddingBottom: verticalScale(10), marginBottom: verticalScale(10)}}
                        >
                            <CustomText
                                style={{fontWeight: 'bold', fontSize: verticalScale(16)}}
                            >
                                {'Dịch vụ mã số #' + this.props.service.serviceOrder.orderId}
                            </CustomText>
                        </CardRow>
                        <CardRow>
                            <FlatList
                                data={this.props.service.serviceOrder.serviceList}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            >
                            </FlatList>
                        </CardRow>
                        <CustomDivider 
                            containerStyle={{marginTop: verticalScale(5)}}
                        />
                        <CardRow>
                            <CardColumn style={styles.columnLeft}>
                                <CustomText style={{fontWeight: 'bold'}}>Tổng</CustomText>
                            </CardColumn>
                            <CardColumn style={styles.columnRight}>
                                <CustomText style={{color: '#FF8383',  fontWeight: 'bold'}}>{formatPrice(this.calSummary(), 3) +'đ'}</CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <View
                        style={{width: '100%', height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title='Quay về trang chính'
                    onPress={() => this.props.navigation.navigate('ServiceHome') }
                />
            </View>
        )
    }
}

const mapStateToProps = ({ service }) => {
    return {
        service
    }
}

export default connect(mapStateToProps, {
    serviceOrder
})(ServiceCompleteScene);