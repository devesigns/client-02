import React, { Component } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {
    CustomText,
    Card,
    CardTouchable,
    CustomAvatar,
    CustomIcon,
    CustomDivider,
    CardRow,
    CardColumn,
    CustomModal
} from '../../common';
import { connect } from 'react-redux';
import {
    contactUserDelete
} from '../../../actions';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../../utils';

class ServiceContactUserProfileScene extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenTitle'),
            headerRight: (
                <View
                    style={styles.headerRightContainerStyle}
                >
                    <TouchableOpacity
                        onPress={() => {
                            try {
                                navigation.getParam('onDeleteButtonClick')();
                            } catch {
                                    console.log('Too fast!');
                                }
                        }}
                        style={styles.headerRightButtonStyle}
                    >
                        <CustomIcon name='trash' size={verticalScale(24)} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                </View>
            )
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            contactDeleteModalVisible: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.navigation.setParams({
            screenTitle: this.props.userProfile.fullname ? this.props.userProfile.fullname : 'Thông tin người dùng',
            onDeleteButtonClick: () => {
                this.setState({
                    contactDeleteModalVisible: true
                });
            }
        });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {

    }

    renderDeleteModal() {
        return (
            <CustomModal
                headerText={'Xóa công việc'}
                visible={this.state.contactDeleteModalVisible}
                onRequestClose={() => this.setState({contactDeleteModalVisible: false})}
                onConfirm={() => {
                    this.setState({contactDeleteModalVisible: false});
                    this.props.contactUserDelete(this.props.userToken, this.props.contactId);
                    this.props.navigation.goBack();
                }}
            >
                <CustomText>
                    Bạn có chắc là muốn xóa thông tin người dùng này không?
                </CustomText>
            </CustomModal>
        )
    }

    render() {
        if (!this.props.userProfile.id) {
            return null;
        }

        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    <Card>
                        <CardRow
                            style={{paddingVertical: verticalScale(30), justifyContent: 'center', alignItems: 'center'}}
                        >
                            <View
                                style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
                            >
                                <CustomAvatar
                                    size={verticalScale(120)}
                                    round
                                    url={this.props.userProfile.imageFile ? this.props.userProfile.imageFile.uri : this.props.userProfile.profileImage}
                                />
                            </View>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: verticalScale(5), marginBottom: verticalScale(5)}} />
                        <CardRow
                            style={{justifyContent: 'center', alignItems: 'center'}}
                        >
                            <CardColumn
                                style={{width: '10%'}}
                            >
                                <CustomIcon name={'mobile'} size={verticalScale(32)} />
                            </CardColumn>
                            <CardColumn
                                style={{width: '90%'}}
                            >
                                <CustomText
                                    style={{fontSize: verticalScale(16)}}
                                >
                                    {this.props.userProfile.cellphone}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: verticalScale(5), marginBottom: verticalScale(5)}} />
                        <CardRow
                            style={{justifyContent: 'center', alignItems: 'center'}}
                        >
                            <CardColumn
                                style={{width: '10%'}}
                            >
                                <CustomIcon name={'user'} size={verticalScale(24)} />
                            </CardColumn>
                            <CardColumn
                                style={{width: '90%'}}
                            >
                                <CustomText
                                    style={{fontSize: verticalScale(16)}}
                                >
                                    {this.props.userProfile.fullname}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                {this.renderDeleteModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    buttonStyle: {
        width: '49%',
        paddingTop: verticalScale(20),
        paddingBottom: verticalScale(20),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: verticalScale(5),
        marginLeft: 0,
        marginRight: 0,
        marginTop: verticalScale(-5),
        marginBottom: 0
    },
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        userProfile: user.contactUserProfile,
        contactId: user.usersInContact[user.contactArrayId] ? user.usersInContact[user.contactArrayId].id : ''
    }
}

export default connect(mapStateToProps, {
    contactUserDelete
})(ServiceContactUserProfileScene);