import React, { Component } from 'react';
import {
    View,
    FlatList,
    StyleSheet,
    Alert,
    TouchableOpacity,
    Linking,
    ScrollView
} from 'react-native';
import { 
    Card,
    CardRow,
    CardColumn,
    CustomAvatar,
    CustomCheckbox,
    CustomText,
    CustomIcon,
    CustomButtonFooter,
    CustomImage
} from '../../common';
import { connect } from 'react-redux';
import {
    serviceListFetch,
    serviceListSelect
} from '../../../actions/ServiceActions';
import { verticalScale } from '../../../utils';

const styles = StyleSheet.create({
    containter: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    }
});

class ServiceListScene extends Component {

    state = {
        serviceList: [],
        selectedServiceList: []
    }

    UNSAFE_componentWillMount() {
        this.props.serviceListFetch(this.props.service.selectedProvider, this.props.userToken);
    }

    // updating - render second
    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            serviceList: [ ...nextProps.service.serviceList ]
        });
    }

    onServiceListItemPress(index) {
        let newServiceList = [ ...this.state.serviceList ];
      //  newServiceList[index].id = index;
        for (i = 0; i < newServiceList.length; i++) {
            if (newServiceList[i].id == index) {
                newServiceList[i].select = !newServiceList[i].select;
            }
        }

        this.setState({
            serviceList: newServiceList
        });
    }

    renderItem = ({item}) => (
        <Card
            style={{padding: 0}}
        >
            <TouchableOpacity
                onPress={() => 
                    this.onServiceListItemPress(item.id)
                }
            >
                <CustomImage 
                    style={{width: '100%', height: 100}}
                    source={{uri: item.image}}
                    resizeMode={'cover'}
                />
                <CardRow
                    style={{alignItems: 'center', padding: verticalScale(10)}}
                >
                    <CardColumn style = {{width: '10%'}}>
                        <CustomCheckbox
                            checked={item.select}
                            onPress={() => this.onServiceListItemPress(item.id)}
                        />
                    </CardColumn>
                    <CardColumn style={{width: '90%'}}>
                        <CustomText
                            style={{fontSize: verticalScale(16), fontWeight: 'bold'}}
                        >
                            {item.title}
                        </CustomText>
                        <CustomText
                            numberOfLines={1}
                        >
                            {item.description}
                        </CustomText>
                    </CardColumn>
                </CardRow>
                <TouchableOpacity
                    onPress={() => {
                        if (item.link) {
                            Linking.openURL(item.link);
                        }
                    }}
                    style={{
                        paddingVertical: verticalScale(5),
                        paddingHorizontal: verticalScale(7),
                        position: 'absolute',
                        top: verticalScale(10),
                        right: verticalScale(10),
                        backgroundColor: '#FFFFFF',
                        borderRadius: verticalScale(5),
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <CustomText
                        style={{
                            color: '#29ABE2',
                            paddingRight: verticalScale(5),
                            fontSize: verticalScale(12)
                        }}
                    >
                        Xem chi tiết
                    </CustomText>
                    <CustomIcon name={'external-link'} size={verticalScale(10)} />
                </TouchableOpacity>
            </TouchableOpacity>
        </Card>
    );

    render() {
        return (
            <View style={styles.containter}>
                <ScrollView>
                    <FlatList
                        data={this.state.serviceList}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        contentContainerStyle={{
                            paddingBottom: 60
                        }}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title='Tiếp theo'
                    onPress={() => {
                        let selectedList = [];
                        let countListCheck = 0;

                        for (i = 0; i < this.state.serviceList.length; i++) {
                            if (this.state.serviceList[i].select === true) {
                                countListCheck++;
                                selectedList.push(this.state.serviceList[i]);
                            }
                        }

                        this.setState({
                            selectedServiceList: selectedList
                        });

                        if (countListCheck === 0) {
                            Alert.alert(
                                "Thông báo",
                                "Vui lòng chọn dịch vụ bạn muốn sử dụng.",
                                [
                                { text: "OK"}
                                ],
                                { cancelable: true }
                            );
                        } else {
                            this.props.serviceListSelect(selectedList);
                            this.props.navigation.navigate('ServiceListSelected')
                        }
                    }}
                />
            </View>
        )
    }
}

const mapStateToProps = ({ auth, service }) => {
    return {
        userToken: auth.token.userToken,
        service
    }
}

export default connect(mapStateToProps, {
    serviceListFetch,
    serviceListSelect
})(ServiceListScene);