import React, { Component } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import {
    CustomText,
    Card,
    CardTouchable,
    CustomAvatar,
    CustomIcon,
    CustomDivider,
    CardRow,
    CardColumn,
    CustomButtonFooter,
    CustomImage
} from '../../common';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import {
    userUpdate
} from '../../../actions';
import {
    scale,
    verticalScale,
    moderateScale
} from '../../../utils';

class AccountProfileScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userProfile: {}
        }
    }

    UNSAFE_componentWillMount() {
        this.setState({
            userProfile: {
                ...this.props.userProfile
            }
        })
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this.setState({
            userProfile: {
                ...nextProps.userProfile
            }
        })
    }

    openImagePicker() {
        const imagePickerOptions = {
            title: 'Chọn ảnh',
            cancelButtonTitle: 'Quay lại',
            takePhotoButtonTitle: 'Chụp ảnh...',
            chooseFromLibraryButtonTitle: 'Chọn ảnh từ bộ sưu tập...',
            maxHeight: 256,
            maxWidth: 256
        };

        ImagePicker.showImagePicker(imagePickerOptions, (response) => {
            console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {                 
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
                this.setState({
                    userProfile: {
                        ...this.state.userProfile,
                        imageFile: response
                    }
                })
            }
        });
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    <Card>
                        <CardRow
                            style={{paddingVertical: verticalScale(30), justifyContent: 'center', alignItems: 'center'}}
                        >
                            <View
                                style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}
                            >
                                <TouchableOpacity
                                    onPress={this.openImagePicker.bind(this)}
                                >
                                    <CustomAvatar
                                        size={verticalScale(120)}
                                        round
                                        url={this.state.userProfile.imageFile ? this.state.userProfile.imageFile.uri : this.state.userProfile.profileImage}
                                    />
                                    <View
                                        style={{flexDirection: 'row', paddingTop: verticalScale(5), justifyContent: 'center', alignItems: 'center'}}
                                    >
                                        <CustomText
                                            style={{color: '#29ABE2'}}
                                        >
                                            Đổi ảnh đại diện
                                        </CustomText>
                                        <CustomIcon name={'edit'} size={verticalScale(16)} style={{color: '#29ABE2', marginLeft: verticalScale(5)}} />
                                    </View>
                                </TouchableOpacity>
                                {/* <View
                                    style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: verticalScale(15)}}
                                >
                                    <TouchableOpacity
                                        style={{paddingHorizontal: verticalScale(5)}}
                                        onPress={() => {
                                            avatar = {
                                                fileName: 'avatar-01.png',
                                                type: 'image/png',
                                                uri: '../../../../assets/img/avatar-01.png'
                                            }

                                            temp = require('../../../../assets/img/avatar-01.png');
                                            console.log(temp);

                                            // this.setState({
                                            //     userProfile: {
                                            //         ...this.state.userProfile,
                                            //         imageFile: avatar
                                            //     }
                                            // })
                                        }}
                                    >
                                        <CustomImage 
                                            style={{width: verticalScale(50), height: verticalScale(50)}}
                                            source={require('../../../../assets/img/avatar-01.png')}
                                            resizeMode={'contain'}
                                        />
                                    </TouchableOpacity>
                                    
                                    
                                </View> */}
                            </View>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: verticalScale(5), marginBottom: verticalScale(5)}} />
                        <CardRow
                            style={{justifyContent: 'center', alignItems: 'center'}}
                        >
                            <CardColumn
                                style={{width: '10%'}}
                            >
                                <CustomIcon name={'mobile'} size={verticalScale(32)} />
                            </CardColumn>
                            <CardColumn
                                style={{width: '90%'}}
                            >
                                <CustomText
                                    style={{fontSize: verticalScale(16)}}
                                >
                                    {this.state.userProfile.cellphone}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: verticalScale(5), marginBottom: verticalScale(5)}} />
                        <CardRow
                            style={{justifyContent: 'center', alignItems: 'center'}}
                        >
                            <CardColumn
                                style={{width: '10%'}}
                            >
                                <CustomIcon name={'user'} size={verticalScale(24)} />
                            </CardColumn>
                            <CardColumn
                                style={{width: '90%'}}
                            >
                                <TextInput
                                    value={this.state.userProfile.fullname}
                                    style={{fontSize: verticalScale(16), padding: 0, borderBottomWidth: 0.5, borderColor: 'rgba(33, 33, 33, 0.3)'}}
                                    placeholder={'Tên đầy đủ...'}
                                    onChangeText={(value) => {
                                        this.setState({
                                            userProfile: {
                                                ...this.state.userProfile,
                                                fullname: value
                                            }
                                        })
                                    }}
                                />
                            </CardColumn>
                            <CustomIcon name={'pencil'} size={verticalScale(16)} style={{position: 'absolute', right: 0}} />
                        </CardRow>
                    </Card>
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title='Lưu thông tin'
                    onPress={() => {
                        submittedForm = { ...this.state.userProfile };

                        let imageFile = submittedForm.imageFile;
                        if (imageFile) {
                            if (imageFile.fileName) {
                                if (imageFile.fileName.endsWith('HEIC')) {
                                    imageFile.fileName = imageFile.fileName.replace('.HEIC', '.JPG');
                                } else if (imageFile.fileName.endsWith('heic')) {
                                    imageFile.fileName = imageFile.fileName.replace('.heic', '.JPG');
                                }
                            } else {
                                imageFile.fileName = 'Unnamed.JPG';
                            }

                            submittedForm.imageFile = imageFile;
                        }

                        this.props.userUpdate(this.props.userToken, submittedForm);
                        this.props.navigation.goBack();
                    }}
                />
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    buttonStyle: {
        width: '49%',
        paddingTop: verticalScale(20),
        paddingBottom: verticalScale(20),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: verticalScale(5),
        marginLeft: 0,
        marginRight: 0,
        marginTop: verticalScale(-5),
        marginBottom: 0
    }
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        userProfile: user.userProfile
    }
}

export default connect(mapStateToProps, {
    userUpdate
})(AccountProfileScene);