import React,  { Component } from 'react';
import {
    View,
    TextInput,
    ScrollView
} from 'react-native';
import {
    Card,
    CardRow,
    CustomDivider,
    CustomText,
    CustomButtonFooter
} from '../../common';
import { connect } from 'react-redux';
import {
    userChangePassword
} from '../../../actions';
import {
    verticalScale
} from '../../../utils';

class AccountChangePasswordScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            retypePassword: ''
        }
    }

    validatePassword() {
        if (this.state.password != this.state.retypePassword) {
            alert('Nhập lại mật khẩu mới không khớp!');
            return false;
        }

        if (this.state.password.length < 4) {
            alert('Mật khẩu phải có tối thiểu 4 ký tự!');
            return false;
        }

        return true;
    }
    
    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView>
                    <Card>
                        <CardRow>
                            <CustomText>
                                Mật khẩu mới
                            </CustomText>
                        </CardRow>
                        <CardRow
                            style={{alignItems: 'center'}}
                        >
                            <TextInput
                                secureTextEntry
                                value={this.state.password}
                                style={{fontSize: verticalScale(16), padding: 0, width: '100%'}}
                                placeholder={'********'}
                                onChangeText={(value) => {
                                    this.setState({
                                        password: value
                                    })
                                }}
                            />
                        </CardRow>
                        <CustomDivider containerStyle={{marginTop: 0, marginBottom: verticalScale(15)}} />
                        <CardRow>
                            <CustomText>
                                Nhập lại mật khẩu mới
                            </CustomText>
                        </CardRow>
                        <CardRow
                            style={{alignItems: 'center'}}
                        >
                            <TextInput
                            secureTextEntry
                                value={this.state.retypePassword}
                                style={{fontSize: verticalScale(16), padding: 0, width: '100%'}}
                                placeholder={'********'}
                                onChangeText={(value) => {
                                    this.setState({
                                        retypePassword: value
                                    })
                                }}
                            />
                        </CardRow>
                    </Card>
                    <View
                        style={{height: verticalScale(60)}}
                    />
                </ScrollView>
                <CustomButtonFooter
                    title='Lưu thông tin'
                    onPress={() => {
                        if (this.validatePassword()) {
                            this.props.userChangePassword(this.props.userToken, this.state.password);
                            this.props.navigation.goBack();
                        }
                    }}
                />
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    buttonStyle: {
        width: '49%',
        paddingTop: verticalScale(20),
        paddingBottom: verticalScale(20),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: verticalScale(5),
        marginLeft: 0,
        marginRight: 0,
        marginTop: verticalScale(-5),
        marginBottom: 0
    }
}

const mapStateToProps = ({ auth }) => {
    return {
        userToken: auth.token.userToken
    }
}

export default connect(mapStateToProps, {
    userChangePassword
})(AccountChangePasswordScene);