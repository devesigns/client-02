import React from 'react';
import {
    CustomWebView
} from '../../common';

export default AccountPrivacyPolicyScene = (props) => {
    return (
        <CustomWebView 
            uri={'https://giupviecxanh.vn/blogs/privacy-policy-my-butler/privacy-policy-my-butler'}
        />
    )
}