import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    ScrollView,
    FlatList,
    TouchableOpacity,
    Linking
} from 'react-native';
import { 
    Card,
    CardRow,
    CardColumn,
    CustomAvatar,
    
    CustomIcon,
    CustomText,
    CustomDivider,
    CustomModal
} from '../../common';
import {
    weekRepeat,
    monthRepeat,
    orderStatuses
} from '../../../const';
import {
    verticalScale
} from '../../../utils';

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    columnFirst: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    columnSecond: {
        width: '90%' 
    },
    contentContainer: {
        paddingVertical: verticalScale(30),
        flex: 1
    },
    columnLeft: {
        width: '50%',
        alignItems: 'flex-start',
        paddingLeft: 0
    },
    columnRight: {
        width: '50%',
        alignItems: 'flex-end',
        paddingRight: 0
    },
    toolItemStyle: {
        width: verticalScale(32),
        height: verticalScale(32),
        borderWidth: 0.5,
        borderColor: 'rgba(33, 33, 33, 0.3)',
        borderRadius: verticalScale(5),
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: verticalScale(10)
    },
});

import { connect } from 'react-redux';

import {
    formatPrice
} from '../../../utils/FormatUtils';

class AccountServiceOrderDetailScene extends Component {

    constructor(props) {
        super(props);

        this.state = {
            orderCancelModalVisible: false,
            orderCancelModalHideConfirm: false,
        }
    }

    UNSAFE_componentWillMount() {
        
    }

    calServiceSum() {
        let serviceSum = 0;
        let selectedServiceList = [ ...this.props.orderDetail.orderItems ];

        for (i = 0; i < selectedServiceList.length; i++) {
            serviceSum = serviceSum + (selectedServiceList[i].price * selectedServiceList[i].quantity);
        }

        serviceSum = serviceSum.toString();
        return serviceSum;
    }

    onContactButtonPress() {
        Linking.openURL('tel:1900886615');
    }

    renderOrderDeleteModal() {
        return (
            <CustomModal
                headerText={'Hủy dịch vụ'}
                visible={this.state.orderCancelModalVisible}
                onRequestClose={() => this.setState({orderCancelModalVisible: false})}
                onConfirm={() => {
                    this.props.serviceOrderCancel(this.props.orderDetail.id);
                    this.setState({orderCancelModalVisible: false});
                }}
                hideConfirm={this.state.orderCancelModalHideConfirm}
            >
                {this.state.orderCancelModalContent}
                <CardRow>
                    <CustomText
                        style={{color: '#29ABE2', fontStyle: 'italic'}}
                    >
                        Bạn chỉ có thể hủy dịch vụ khi đơn hàng vẫn còn trong tình trạng <CustomText style={{fontWeight: 'bold'}}>Đang xử lý</CustomText> và chưa được đặt quá 6 tiếng.
                        Nếu bạn vẫn muốn hủy đơn hàng này, vui lòng liên hệ với chúng tôi càng sớm càng tốt!
                    </CustomText>
                </CardRow>
            </CustomModal>
        )
    }

    renderRepeatList() {
        let repeatListString = '';
        let repeatList = this.props.orderDetail.repeatList;

        if (this.props.orderDetail.repeat == 'daily') {
            return 'Hằng ngày';
        } else if (this.props.orderDetail.repeat == 'weekly') {
            if (repeatList.length == 7) {
                repeatListString = 'Cả tuần';
                return repeatListString;
            } else {
                repeatListString = 'Hằng tuần vào ';
                for (i = 0; i < repeatList.length; i++) {
                    repeatListString += weekRepeat[repeatList[i]].label;

                    if (i != repeatList.length - 1) {
                        repeatListString += ', ';
                    }
                }
            }
        } else if (this.props.orderDetail.repeat == 'monthly') {
            if (repeatList.length == 31) {
               repeatListString = 'Cả tháng';
               return repeatListString; 
            } else {
                repeatListString = 'Hằng tháng vào ngày ';
                for (i = 0; i < repeatList.length; i++) {
                    repeatListString += monthRepeat[repeatList[i] - 1].label;

                    if (i != repeatList.length - 1) {
                        repeatListString += ', ';
                    }
                }
            }
        } else {
            return 'Không';
        }

        return repeatListString;
    }

    renderStartTime() {
        let dateStringArray = this.props.orderDetail.time.split('T');
        let date = dateStringArray[0];
        let time = dateStringArray[1].split('Z')[0];

        return (
            <CustomText>
                Lúc {time},
                ngày {date}
            </CustomText>
        )
    }

    renderServiceItem = ({item}) => (
        <CardRow>
            <CardColumn style ={styles.columnLeft}>
                <CustomText style={{fontWeight: 'bold'}}>
                    {item.name}
                </CustomText>
            </CardColumn>
            <CardColumn style ={styles.columnRight}>
                <CustomText>{formatPrice(item.price.toString(), 3)} x {item.quantity} {item.unit}</CustomText>
            </CardColumn>
        </CardRow>
    );
   
    render() {
        return (
            <View style={styles.containerStyle}> 
                <ScrollView
                    style={{flex: 1}}
                >
                    <Card>
                        <CardRow>
                            <CardColumn
                                style={{width: '60%', paddingLeft: 0, justifyContent: 'center'}}
                            >
                                <CustomText
                                    style={{fontWeight: 'bold'}}
                                >
                                    Tình trạng: 
                                </CustomText>
                                <CustomText
                                    style={[
                                        {fontWeight: 'bold'},
                                        this.props.orderDetail.status == 'canceled' ? {color: '#FF8383'} : 
                                        this.props.orderDetail.status == 'processing' ? {color: '#FFD300'} : {color: '#51FFC0'}
                                    ]}
                                >
                                    {orderStatuses[this.props.orderDetail.status]}
                                </CustomText>
                            </CardColumn>
                            <CardColumn
                                style={{width: '40%', paddingRight: 0, flexDirection: 'row', justifyContent: 'flex-end'}}
                            >
                                <TouchableOpacity
                                    onPress={this.onContactButtonPress.bind(this)}
                                    style={styles.toolItemStyle}
                                >
                                    <CustomIcon name="headphones" size={verticalScale(24)} style={{color: '#333333'}} />
                                </TouchableOpacity>
                                {
                                    this.props.orderDetail.status == 'processing' ?
                                    <TouchableOpacity
                                        onPress={() => {
                                            timeDiff = (new Date().getTime() - new Date(this.props.orderDetail.createdAt).getTime()) / (1000 * 60 * 60);

                                            if (this.props.orderDetail.status == 'processing' && timeDiff < 6) {
                                                this.setState({
                                                    selectedOrderId: this.props.orderDetail.id,
                                                    orderCancelModalVisible: true,
                                                    orderCancelModalHideConfirm: false,
                                                    orderCancelModalContent: (
                                                        <CardRow>
                                                            <CustomText>
                                                                Bạn có chắc là muốn hủy order dịch vụ này không?
                                                            </CustomText>
                                                        </CardRow>
                                                    )
                                                })
                                            } else {
                                                this.setState({
                                                    orderCancelModalVisible: true,
                                                    orderCancelModalHideConfirm: true,
                                                    orderCancelModalContent: (
                                                        <CardRow>
                                                            <CustomText>
                                                                Không thể hủy đơn hàng này!
                                                            </CustomText>
                                                        </CardRow>
                                                    )
                                                })
                                            }
                                        }}
                                        style={[styles.toolItemStyle, {borderColor: '#FF8383'}]}
                                    >
                                        <CustomIcon name="close" size={24} style={{color: '#FF8383'}} />
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <Card>
                        <CardRow
                            
                        >
                            <CustomText
                                style={{fontWeight: 'bold', fontSize: verticalScale(18)}}
                            >
                                {'Dịch vụ mã số #' + this.props.orderDetail.id}
                            </CustomText>
                        </CardRow>
                        <CardRow
                            style={{borderBottomWidth: 0.5, paddingBottom: verticalScale(10), marginBottom: verticalScale(10)}}
                        >
                            <CustomText
                                numberOfLines={1}
                            >
                                bởi <CustomText style={{color: '#29ABE2'}}>{this.props.orderDetail.providerName}</CustomText>
                            </CustomText>
                        </CardRow>
                        <CardRow>
                            <FlatList
                                data={this.props.orderDetail.orderItems}
                                renderItem={this.renderServiceItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </CardRow>
                        <CustomDivider 
                            containerStyle={{marginTop: verticalScale(5)}}
                        />
                        <CardRow>
                            <CardColumn style={styles.columnLeft}>
                                <CustomText style={{fontWeight: 'bold'}}>Tổng</CustomText>
                            </CardColumn>
                            <CardColumn style={styles.columnRight}>
                                <CustomText style={{color: '#FF8383',  fontWeight: 'bold'}}>{formatPrice(this.calServiceSum(), 3) +'đ'}</CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <Card>
                        <CardRow
                            style={{alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="map-marker" size={verticalScale(30)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    {this.props.orderDetail.address}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                            <CustomIcon name="calendar" size={verticalScale(25)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                {this.renderStartTime()}
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="check-square-o" size={verticalScale(25)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    Định kỳ: {this.renderRepeatList()}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                        <CardRow
                            style={{paddingTop: verticalScale(10), alignItems: 'center'}}
                        >
                            <CardColumn style={styles.columnFirst}>
                                <CustomIcon name="edit" size={verticalScale(25)}/>
                            </CardColumn>
                            <CardColumn style={styles.columnSecond}>
                                <CustomText>
                                    {this.props.orderDetail.note ? this.props.orderDetail.note : 'Không có ghi chú'}
                                </CustomText>
                            </CardColumn>
                        </CardRow>
                    </Card>
                    <View
                        style={{width: '100%', height: verticalScale(60)}}
                    />
                </ScrollView>
                {this.renderOrderDeleteModal()}
            </View>
        )
    }
}

const mapStateToProps = ({ serviceHistory }) => {
    console.log(serviceHistory);
    return {
        orderDetail: serviceHistory.orderDetail
    }
}

export default connect(mapStateToProps, {
    
})(AccountServiceOrderDetailScene);