import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    View,
    TouchableOpacity,
    TextInput,
    Platform
} from 'react-native';
import Communications from 'react-native-communications';
import { connect } from 'react-redux';
import {
    CustomText,
    Card,
    CardRow,
    CustomIcon,
    CustomStatusBar,
    CustomModal
} from '../../common';
import {
    getUserProfile
} from '../../../actions';
import {
    verticalScale,
} from '../../../utils';

class AccountInviteFriendScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneToIntroduce: '',
            phoneInputModalVisible: false
        }
    }

    signOutAsync = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    };

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <CustomStatusBar backgroundColor="#29ABE2" barStyle="light-content" />
                <Card
                    style={{paddingTop: verticalScale(5)}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                phoneInputModalVisible: true
                            })
                        }}
                    >
                        <CardRow>
                            <CustomIcon name='mobile' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Qua tin nhắn SMS
                            </CustomText> 
                        </CardRow>
                    </TouchableOpacity>
                </Card>
                <CustomModal
                    visible={this.state.phoneInputModalVisible}
                    onRequestClose={() => {
                        this.setState({
                            phoneInputModalVisible: false
                        })
                    }}
                    onConfirm={() => {
                        this.setState({
                            phoneInputModalVisible: false
                        })

                        let smsBody = `${this.props.userProfile.fullname} mời bạn cài đặt MyButler để quản lý việc nhà thông minh:\n\nAndroid: https://play.google.com/store/apps/details?id=com.devesigns.mybutler.client\n\niOS: https://apps.apple.com/us/app/my-butler/id1473480115`
                        
                        if (Platform.OS === 'ios') {
                            smsBody = `${this.props.userProfile.fullname} mời bạn cài đặt MyButler để quản lý việc nhà thông minh:\n\nAndroid: https://play.google.com/store/apps/details?id=com.devesigns.mybutler.client\n\niOS: https://apps.apple.com/us/app/my-butler/id1473480115`
                            Communications.textWithoutEncoding(
                                this.state.phoneToIntroduce,
                                smsBody
                            )
                        } else {
                            Communications.text(
                                this.state.phoneToIntroduce,
                                smsBody
                            )
                        }
                    }}
                >
                    <CardRow
                        style={{
                            alignItems: 'center'
                        }}
                    >
                        <TextInput
                            value={this.state.phoneToIntroduce}
                            placeholder={'SĐT bạn muốn giới thiệu'}
                            style={{
                                padding: 0,
                                fontSize: verticalScale(18),
                                paddingBottom: verticalScale(5),
                                borderBottomColor: 'rgba(33, 33, 33, 0.3)',
                                borderBottomWidth: 0.5,
                                width: '100%'
                            }}
                            onChangeText={(value) => {
                                this.setState({
                                    phoneToIntroduce: value
                                })
                            }}
                            keyboardType='numeric'
                            maxLength={12}
                        />
                    </CardRow>
                </CustomModal>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    }
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        userProfile: user.userProfile
    }
}

export default connect(mapStateToProps, {
    getUserProfile
})(AccountInviteFriendScene);