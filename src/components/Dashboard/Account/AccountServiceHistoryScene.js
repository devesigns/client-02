import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View,
    FlatList,
    ScrollView,
    TouchableOpacity,
    Alert,
    Linking,
    RefreshControl
} from 'react-native';
import {
    CustomText,
    CardTouchable,
    CardRow,
    CardColumn,
    
    CustomModal,
    CustomIcon,
    CustomDivider
} from '../../common';
import { connect } from 'react-redux';
import {
    serviceOrderFetch,
    serviceOrderCancel,
    serviceHistoryReducerUpdate
} from '../../../actions';
import {
    formatDateAndTime
} from '../../../utils/FormatUtils';
import {
    orderStatuses
} from '../../../const';
import {
    verticalScale
} from '../../../utils';

class AccountServiceHistoryScene extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderCancelModalVisible: false,
            orderCancelModalHideConfirm: false,
            selectedOrderId: '',
            refreshing: false
        }
    }

    UNSAFE_componentWillMount() {
        this.props.serviceOrderFetch(this.props.userToken);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        // Check if the last fetch has reached the end of the records
        // by comparing:
        // nextProps.orders.length - this.props.orders.length < 10 (meaning: fetching 10 orders per fetch request)
        // If the comparison returns true, it means we have fetched all of the available orders
        // ...

        nextOrderListLength = Object.keys(nextProps.orders).length;
        currentOrderListLength = Object.keys(this.props.orders).length;
        if (nextOrderListLength - currentOrderListLength == 50) {
            this.props.serviceOrderFetch(this.props.userToken, nextOrderListLength);
        }
    }

    getOrderItemNameString(orderItems) {
        orderItemNameString = '';

        for (i = 0; i < orderItems.length; i++) {
            orderItemNameString += orderItems[i].name;

            if (i != orderItems.length - 1) {
                orderItemNameString += ', ';
            }
        }

        return orderItemNameString;
    }

    onContactButtonPress() {
        Linking.openURL('tel:1900886615');
    }

    renderOrderListItem = ({item, index}) => {
        return (
            <CardTouchable
                onPress={() => {
                    this.props.serviceHistoryReducerUpdate('orderDetail', item);
                    this.props.navigation.navigate('AccountServiceOrderDetailScene');
                }}
            >
                <CardRow
                    
                >
                    <CustomText
                        
                    >
                        <CustomText style={{fontWeight: 'bold'}}>#{item.id}</CustomText>, lúc <CustomText>{formatDateAndTime(item.createdAt)}</CustomText>
                    </CustomText>
                </CardRow>
                <CustomDivider />
                <CardRow
                    
                >
                    <CustomText
                        style={{fontSize: verticalScale(18)}}
                        numberOfLines={1}
                    >
                        {this.getOrderItemNameString(item.orderItems)}
                    </CustomText>
                </CardRow>
                <CardRow
                    
                >
                    <CustomText
                        numberOfLines={1}
                    >
                        bởi <CustomText style={{color: '#29ABE2'}}>{item.providerName}</CustomText>
                    </CustomText>
                </CardRow>
                <CardRow
                    style={{paddingTop: verticalScale(15), marginBottom: 0}}
                >
                    <CustomText
                        style={[
                            {
                                fontWeight: 'bold'
                            },
                            item.status == 'canceled' ?
                            {
                                color: '#FF8383'
                            }
                            :
                            null,
                            item.status == 'processing' ?
                            {
                                color: '#FFD300'
                            }
                            :
                            {
                                color: '#51FFC0'
                            }
                        ]}
                    >
                        {orderStatuses[item.status]}
                    </CustomText>
                </CardRow>
                <View
                    style={styles.toolContainerStyle}
                >
                    <TouchableOpacity
                        onPress={this.onContactButtonPress.bind(this)}
                        style={styles.toolItemStyle}
                    >
                        <CustomIcon name="headphones" size={verticalScale(24)} style={{color: '#333333'}} />
                    </TouchableOpacity>
                    {
                        item.status == 'processing' ?
                        <TouchableOpacity
                            onPress={() => {
                                timeDiff = (new Date().getTime() - new Date(item.createdAt).getTime()) / (1000 * 60 * 60);

                                if (item.status == 'processing' && timeDiff < 6) {
                                    this.setState({
                                        selectedOrderId: item.id,
                                        orderCancelModalVisible: true,
                                        orderCancelModalHideConfirm: false,
                                        orderCancelModalContent: (
                                            <CardRow>
                                                <CustomText>
                                                    Bạn có chắc là muốn hủy order dịch vụ này không?
                                                </CustomText>
                                            </CardRow>
                                        )
                                    })
                                } else {
                                    this.setState({
                                        orderCancelModalVisible: true,
                                        orderCancelModalHideConfirm: true,
                                        orderCancelModalContent: (
                                            <CardRow>
                                                <CustomText>
                                                    Không thể hủy đơn hàng này!
                                                </CustomText>
                                            </CardRow>
                                        )
                                    })
                                }
                            }}
                            style={[styles.toolItemStyle, {borderColor: '#FF8383'}]}
                        >
                            <CustomIcon name="close" size={verticalScale(24)} style={{color: '#FF8383'}} />
                        </TouchableOpacity>
                        :
                        null
                    }
                </View>
            </CardTouchable>
        )
    }

    renderOrderDeleteModal() {
        return (
            <CustomModal
                headerText={'Hủy dịch vụ'}
                visible={this.state.orderCancelModalVisible}
                onRequestClose={() => this.setState({orderCancelModalVisible: false})}
                onConfirm={() => {
                    this.props.serviceOrderCancel(this.props.userToken, this.state.selectedOrderId);
                    this.setState({orderCancelModalVisible: false});
                }}
                hideConfirm={this.state.orderCancelModalHideConfirm}
            >
                {this.state.orderCancelModalContent}
                <CardRow>
                    <CustomText
                        style={{color: '#29ABE2', fontStyle: 'italic'}}
                    >
                        Bạn chỉ có thể hủy dịch vụ khi đơn hàng vẫn còn trong tình trạng <CustomText style={{fontWeight: 'bold'}}>Đang xử lý</CustomText> và chưa được đặt quá 6 tiếng.
                        Nếu bạn vẫn muốn hủy đơn hàng này, vui lòng liên hệ với chúng tôi càng sớm càng tốt!
                    </CustomText>
                </CardRow>
            </CustomModal>
        )
    }

    renderContent() {
        return (
            <FlatList
                data={this.props.orders}
                renderItem={this.renderOrderListItem}
                inverted
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderIllustration() {
        return (
            <CustomText
                style={{textAlign: 'center', paddingTop: 30}}
            >
                Bạn chưa có đơn hàng nào.
            </CustomText>
        )
    }

    onRefresh() {
        this.setState({
            refreshing: true
        }, () => {
            setTimeout(() => {
                this.setState({
                    refreshing: false
                })
            }, 500);
        })

        this.props.serviceOrderFetch(this.props.userToken);
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <ScrollView
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this.onRefresh.bind(this)}
                        />
                    }
                >
                    {
                        !_.isEmpty(this.props.orders) > 0 ?
                        this.renderContent()
                        :
                        this.renderIllustration()
                    }
                    <View
                        style={{width: '100%', height: 30}}
                    />
                </ScrollView>
                {this.renderOrderDeleteModal()}
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    },
    headerRightButtonStyle: {
        fontWeight: 'bold',
        color: '#FFFFFF',
        paddingLeft: verticalScale(10)
    },
    headerRightContainerStyle: {
        flexDirection: 'row'
    },
    toolContainerStyle: {
        position: 'absolute',
        flexDirection: 'row',
        bottom: verticalScale(10),
        right: verticalScale(10)
    },
    toolItemStyle: {
        width: verticalScale(32),
        height: verticalScale(32),
        borderWidth: 0.5,
        borderColor: 'rgba(33, 33, 33, 0.3)',
        borderRadius: verticalScale(5),
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: verticalScale(10)
    },
    cardRowStyle: {
        paddingTop: verticalScale(5),
        paddingLeft: verticalScale(5),
        paddingRight: verticalScale(5)
    }
}

const mapStateToProps = ({ auth, serviceHistory }) => {
    orders = _.toArray(serviceHistory.orders);

    return {
        userToken: auth.token.userToken,
        orders
    }
}

export default connect(mapStateToProps, {
    serviceOrderFetch,
    serviceOrderCancel,
    serviceHistoryReducerUpdate
})(AccountServiceHistoryScene);