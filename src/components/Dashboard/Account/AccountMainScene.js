import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    View,
    TouchableOpacity,
    Platform
} from 'react-native';
import { connect } from 'react-redux';
import {
    CustomText,
    Card,
    CardRow,
    CardColumn,
    CardTouchable,
    CustomIcon,
    CustomAvatar,
    CustomDivider,
    CustomStatusBar
} from '../../common';
import {
    getUserProfile
} from '../../../actions';
import {
    verticalScale, formatPrice
} from '../../../utils';

class AccountMainScene extends Component {
    static navigationOptions = {
        header: null
    }

    signOutAsync = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    };

    UNSAFE_componentWillReceiveProps(nextProps) {
        
    }

    render() {
        return (
            <View
                style={styles.containerStyle}
            >
                <CustomStatusBar backgroundColor="#29ABE2" barStyle="light-content" />
                <Card
                    style={{
                        marginLeft: 0,
                        marginRight: 0,
                        marginTop: 0,
                        backgroundColor: '#29ABE2',
                        paddingTop: (Platform.OS === 'ios') ? verticalScale(15) : verticalScale(5),
                        paddingBottom: verticalScale(5),
                        borderRadius: 0,
                        shadowColor: 'rgba(0, 0, 0, 0)'
                    }}
                >
                    <CardRow
                        style={{alignItems: 'center'}}
                    >
                        <CustomAvatar
                            size={verticalScale(50)}
                            round
                            url={this.props.userProfile.profileImage}
                        />
                        <CustomText
                            style={{paddingLeft: verticalScale(10), fontSize: verticalScale(18), width: '80%', fontWeight: 'bold', color: "#FFFFFF"}}
                        >
                            {this.props.userProfile.fullname}
                        </CustomText>
                    </CardRow>
                </Card>
                <Card
                    style={{paddingTop: verticalScale(15)}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('AccountProfileScene');
                        }}
                    >
                        <CardRow>
                            <CustomIcon name='user' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Thông tin tài khoản
                            </CustomText> 
                        </CardRow>
                    </TouchableOpacity>
                    <CustomDivider />
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('AccountChangePasswordScene');
                        }}
                    >
                        <CardRow>
                            <CustomIcon name='lock' size={verticalScale(24)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Đổi mật khẩu
                            </CustomText>      
                        </CardRow>
                    </TouchableOpacity>
                    <CustomDivider />
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('AccountServiceHistoryScene');
                        }}     
                    >
                        <CardRow>
                            <CustomIcon name='file-text' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Lịch sử dịch vụ
                            </CustomText>      
                        </CardRow>
                    </TouchableOpacity>
                    <CustomDivider />
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('AccountInviteFriendScene');
                        }}     
                    >
                        <CardRow>
                            <CustomIcon name='share-square-o' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Mời bạn bè
                            </CustomText>      
                        </CardRow>
                    </TouchableOpacity>
                </Card>
                <Card
                    style={{paddingTop: 15}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            alert('Tính năng sẽ xuất hiện trong những bản cập nhật sau!')
                        }}
                    >
                        <CardRow>                
                            <CustomIcon name='bell' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Thông báo
                            </CustomText> 
                        </CardRow>
                    </TouchableOpacity>
                    <CustomDivider />
                    {/* <TouchableOpacity
                                
                    >
                        <CardRow>   
                            <CustomIcon name='gear' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Cài đặt
                            </CustomText>            
                        </CardRow>
                    </TouchableOpacity> */}
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.navigate('AccountPrivacyPolicyScene');
                        }}
                    >
                        <CardRow>   
                            <CustomIcon name='book' size={verticalScale(22)} style={{paddingRight: verticalScale(10), width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%'}}
                            >
                                Chính sách bảo mật
                            </CustomText>            
                        </CardRow>
                    </TouchableOpacity>
                    <CustomDivider />
                    <TouchableOpacity
                        onPress={() => this.signOutAsync()}
                    >
                        <CardRow>                  
                            <CustomIcon name='sign-out' size={verticalScale(22)} style={{paddingRight: verticalScale(10), color: '#FF8383', width: '10%', textAlign: 'center'}} />
                            <CustomText
                                style={{fontSize: verticalScale(16), width: '90%', color: '#FF8383'}}
                            >
                                Đăng xuất
                            </CustomText>
                        </CardRow>
                    </TouchableOpacity>
                </Card>
            </View>
        )
    }
}

const styles = {
    containerStyle: {
        flex: 1,
        backgroundColor: '#F2F2F2'
    }
}

const mapStateToProps = ({auth, user}) => {
    return {
        userToken: auth.token.userToken,
        userProfile: user.userProfile
    }
}

export default connect(mapStateToProps, {
    getUserProfile
})(AccountMainScene);