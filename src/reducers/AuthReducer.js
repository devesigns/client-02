import { combineReducers } from 'redux';

import {
    AUTH_REDUCER_UPDATE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAIL,
    SEND_SMS_SUCCESS,
    SEND_SMS_FAIL,
    VERIFY_PHONE_SUCCESS,
    VERIFY_PHONE_FAIL,
    REGISTER_ACCOUNT_SUCCESS,
    REGISTER_ACCOUNT_FAIL,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAIL,
    CHECK_USER_YES,
    CHECK_USER_NO
} from '../actions/types';

const rootReducer = (state = {
    // error: null,
    loginStatus: '',
    certificateStatus: '',
    verifyStatus: '',
    registerStatus: '',
    informUser: '',
    userToken: '',
    resetPassStatus: '',
    isExists: ''
}, action) => {
    switch (action.type) {
        case AUTH_REDUCER_UPDATE: {
            newState = { ...state, [action.payload.props]: action.payload.value };
            return newState;
        }
        case AUTH_LOGIN_SUCCESS: {
            return { 
                ...state,
                loginStatus: { 
                    code: 200,
                    message: 'Dang nhap thanh cong!'
                },
                userToken: action.payload.token
            }
        }
        case AUTH_LOGIN_FAIL:
            return { ...state, loginStatus: { code: 400, message: action.payload.message } }
        case SEND_SMS_SUCCESS: 
            return { ...state, certificateStatus: {code: 200} }
        case SEND_SMS_FAIL: 
            return { ...state, certificateStatus: { code: 400, message: action.payload.message } }
        case VERIFY_PHONE_SUCCESS: 
            return { ...state, verifyStatus: {code: 200} }
        case VERIFY_PHONE_FAIL: 
            return { ...state, verifyStatus: { code: 400, message: action.payload.message } }
        case REGISTER_ACCOUNT_SUCCESS: 
            return { ...state, registerStatus: {code: 200} }
        case REGISTER_ACCOUNT_FAIL: 
            return { ...state, registerStatus: { code: 400, message: action.payload.message } }
        case RESET_PASSWORD_SUCCESS: 
            return { ...state, resetPassStatus: {code: 200} }
        case RESET_PASSWORD_FAIL: 
            return { ...state, resetPassStatus: { code: 400, message: action.payload.message } }
        case CHECK_USER_YES: 
            return { ...state, isExists: {code: 200} }
        case CHECK_USER_NO: 
            return { ...state, isExists: {code: 400} }
        default:
            return state;
    }
};

export default combineReducers({
    token: rootReducer
});