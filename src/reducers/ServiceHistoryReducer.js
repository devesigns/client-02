import _ from 'lodash';
import {
    SERVICE_HISTORY_REDUCER_UPDATE,
    SERVICE_HISTORY_FETCH,
    SERVICE_ORDER_CANCEL
} from '../actions/types';

const INITIAL_STATE = {
    orders: {},
    orderDetail: {}
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (SERVICE_HISTORY_REDUCER_UPDATE): {
            return { ...state, [action.payload.props]: action.payload.value }
        }
        case (SERVICE_HISTORY_FETCH): {
            // Considering using this approach for all similar fetches
            // Reducer will only hold the NEAREST "N" items that's returned from API
            // It's like a pagination system

            newOrders = { ...state.orders };

            for (i = 0; i < action.payload.orders.length; i++) {
                if (!_.isEqual(newOrders[action.payload.orders[i].id], action.payload.orders[i])) {
                    newOrders[action.payload.orders[i].id] = action.payload.orders[i];
                }
            }

            return {
                ...state,
                orders: newOrders
            }
        }
        case SERVICE_ORDER_CANCEL: {
            newOrders = { ...state.orders };
            newOrders[action.payload.orderForm.id] = action.payload.orderForm;

            return {
                ...state,
                orders: newOrders
            }
        }
        default: {
            return state;
        }
    }
}