import _ from 'lodash';
import {
    USER_REDUCER_UPDATE,
    USER_CONTACT_FETCH,
    CONTACT_USER_DELETE,
    CONTACT_USER_CREATE
} from '../actions/types';

const INITIAL_STATE = {
    usersInContact: [],
    userProfile: {},
    contactUserProfile: {},
    contactArrayId: -1
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (USER_REDUCER_UPDATE):
            if (_.isEqual(state[action.payload.props], action.payload.value)) {
                return state;
            }

            return { ...state, [action.payload.props]: action.payload.value }
        case (USER_CONTACT_FETCH): {
            return {
                ...state,
                usersInContact: [
                    // ...state.usersInContact,
                    ...action.payload.value
                ]
            }
        }
        case (CONTACT_USER_DELETE): {
            newContactUsers = [ ...state.usersInContact ];
            newContactUsers.splice(state.contactArrayId, 1);

            return {
                ...state,
                usersInContact: newContactUsers,
                contactUserProfile: {},
                contactArrayId: -1
            }
        }
        case (CONTACT_USER_CREATE): {
            newContactUsers = [ ...state.usersInContact ];
            newContactUsers.push(action.payload.value);

            return {
                ...state,
                usersInContact: newContactUsers
            }
        }
        default: {
            return state;
        }
    }
}