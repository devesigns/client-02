import { combineReducers } from 'redux';

import AuthReducer from './AuthReducer';
import ServiceReducer from './ServiceReducer';
import HouseReducer from './HouseReducer';
import AssignmentReducer from './AssignmentReducer';
import UserReducer from './UserReducer';
import ServiceHistoryReducer from './ServiceHistoryReducer';
import NotificationReducer from './NotificationReducer';

export default combineReducers({
    auth: AuthReducer,
    service: ServiceReducer,
    serviceHistory: ServiceHistoryReducer,
    house: HouseReducer,
    assignment: AssignmentReducer,
    user: UserReducer,
    notification: NotificationReducer
});