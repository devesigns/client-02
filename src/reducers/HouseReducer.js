import _ from 'lodash';
import {
    HOUSE_FETCH,
    HOUSE_FETCH_FULL_SINGLE,
    HOUSE_CREATE_HOUSE,
    HOUSE_REDUCER_UPDATE,
    HOUSE_UPDATE_HOUSE,
    HOUSE_DELETE_HOUSE,
    HOUSE_CREATE_ROOM,
    HOUSE_CREATE_ROOM_TEMPLATE,
    HOUSE_UPDATE_ROOM,
    HOUSE_DELETE_ROOM,
    HOUSE_CREATE_CHORE,
    HOUSE_UPDATE_CHORE,
    HOUSE_DELETE_CHORE,
    HOUSE_MASS_ASSIGN_CHORE,
    HOUSE_MANAGEMENT_CANCEL
} from '../actions/types';

const INITIAL_STATE = {
    houses: [],
    rooms: [],
    templateRooms: [],
    chores: [],
    houseForm: {},
    roomForm: {},
    choreForm: {},
    selectedHouseArrayId: -1,
    selectedRoomArrayId: -1,
    selectedChoreArrayId: -1,
    isHouseCreate: false,
    isRoomCreate: false,
    isChoreCreate: false,
    isChoreBulkCreate: false,
    newHouseReady: false,
    newRoomReady: false,
    // After updating things, we will use the below flags to trigger a refetch on affected scene
    fullHouseRefetch: {
        active: false
    },
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (HOUSE_REDUCER_UPDATE): {
            return { ...state, [action.payload.props]: action.payload.value }
        }
        case (HOUSE_FETCH): {
            // We srsly need another way to check for this
            equal = true;

            if (state.houses.length == action.payload.houses.length) {
                for (i = 0; i < state.houses.length; i++) {
                    for (key in state.houses[i]) {     
                        if (key == 'image' && state.houses[i][key] && action.payload.houses[i][key]) {
                            if (state.houses[i][key].split('?')[0] != action.payload.houses[i][key].split('?')[0]) {
                                equal = false;
                                break;
                            }
                        }

                        if (state.houses[key] != action.payload.houses[key]) {
                            equal = false;
                            break;
                        }
                    }
                }
                
                if (equal) {
                    return state;
                }
            }
            
            return {
                ...state,
                houses: action.payload.houses
            }
        }
        case (HOUSE_CREATE_HOUSE): {
            // Push the new house to houses array
            let houseForm = { ...action.payload.houseForm };
            houseForm.rooms = [];

            newHouses = [ ...state.houses, houseForm ];

            return { 
                ...state,
                houses: newHouses,
                selectedHouseArrayId: newHouses.length - 1,
                newHouseReady: true,
                houseForm: houseForm
            };
        }
        case (HOUSE_UPDATE_HOUSE): {
            // Updating the house with current selectedHouseArrayId
            newHouses = [ ...state.houses ]
            newHouses[state.selectedHouseArrayId] = { ...action.payload.houseForm }

            return {
                ...state,
                houses: newHouses,
                newHouseReady: false,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_DELETE_HOUSE): {
            // Delete the house with current selectedHouseArrayId
            newHouses = [ ...state.houses ]
            newHouses.splice(state.selectedHouseArrayId, 1);

            return {
                ...state,
                houses: newHouses,
                selectedHouseArrayId: -1
            }
        }
        case (HOUSE_FETCH_FULL_SINGLE):
            // if (fullHouseRefetch.active && fullHouseRefetch.cross) {
            //     if (fullHouseRefetch.targetId != state.houseForm.id) {
            //         return state;
            //     }
            // }

            // Push the newly-fetched house with full data into the queue
            fullHouseRefetch = {
                active: false
            }

            houseForm = action.payload.value;
            rooms = houseForm.rooms;

            return {
                ...state,
                fullHouseRefetch,
                houseForm,
                rooms
            }
        case (HOUSE_CREATE_ROOM_TEMPLATE): {
            return {
                ...state,
                // rooms: newRooms,
                houseForm: action.payload.houseForm,
                rooms: action.payload.houseForm.rooms
            }
        }
        case (HOUSE_CREATE_ROOM): {
            // Creating a new empty room
            // newRooms = [ 
            //     ...state.rooms,
            //     action.payload.roomForm,
            // ]

            return {
                ...state,
                // rooms: newRooms,
                newRoomReady: true,
                roomForm: action.payload.roomForm,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_UPDATE_ROOM): {
            // Update the room with current selectedRoomArrayId
            newRooms = [ ...state.rooms ]
            newRooms[state.selectedRoomArrayId] = { ...action.payload.roomForm }

            return {
                ...state,
                rooms: newRooms,
                newRoomReady: false,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_DELETE_ROOM): {
            // Delete the rooms with id in roomIdArray
            selectedHouseId = state.rooms[0].house;
            newRooms = [];
            
            for (i = 0; i < state.rooms.length; i++) {
                if (action.payload.roomIdArray.includes(state.rooms[i].id)) {
                    continue;
                } else {
                    newRooms.push(state.rooms[i]);
                }
            }

            return {
                ...state,
                rooms: newRooms,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_CREATE_CHORE): {
            // Creating single chore for one room and one house
            newChores = [ ...state.chores, action.payload.chores[0] ];

            return { 
                ...state,
                chores: newChores,
                fullHouseRefetch: {
                    active: true
                }
            };
        }
        case (HOUSE_UPDATE_CHORE): {
            // Updating the house with current selectedChoreArrayId
            newChores = [ ...state.chores ]

            if (state.selectedChoreArrayId >= 0) {
                newChores[state.selectedChoreArrayId] = { ...action.payload.choreForm }
            }

            return {
                ...state,
                chores: newChores,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_DELETE_CHORE): {
            // Delete the chores with id in roomIdArray
            newChores = [];
            
            for (i = 0; i < state.chores.length; i++) {
                if (action.payload.choreIdArray.includes(state.chores[i].id)) {
                    continue;
                } else {
                    newChores.push(state.chores[i]);
                }
            }

            return {
                ...state,
                chores: newChores,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_MASS_ASSIGN_CHORE): {
            return {
                ...state,
                fullHouseRefetch: {
                    active: true
                }
            }
        }
        case (HOUSE_MANAGEMENT_CANCEL): {
            newHouses = [ ...state.houses ];
            newHouses.splice(state.selectedHouseArrayId, 1);

            return {
                ...state,
                houses: newHouses,
                selectedHouseArrayId: -1
            }
        }
        default: {
            return state;
        }
    }
}