import {
    SERVICE_PROVIDER_FETCH,
    SERVICE_PROVIDER_SELECT,
    SERVICE_LIST_FETCH,
    SERVICE_LIST_SELECT,
    SERVICE_ORDER_CREATE
} from '../actions/types';

const INITIAL_STATE = {
    // list of service providers in service home page
    serviceProviders: [],
    // id of provider that user just chose
    selectedProvider: '',
    // service list of chosen provider
    serviceList: [],
    // chosen list of service 
    selectedServiceList: [],
    // information of order
    serviceOrder: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (SERVICE_PROVIDER_FETCH): {
            return { ...state, serviceProviders: action.payload };
        }
        case (SERVICE_PROVIDER_SELECT): {
            return { ...state, selectedProvider: action.payload };
        }
        case (SERVICE_LIST_FETCH): {
            return { ...state, serviceList: action.payload };
        }
        case (SERVICE_LIST_SELECT): {
            return { ...state, selectedServiceList: action.payload };
        }
        case (SERVICE_ORDER_CREATE): {
            return { ...state, serviceOrder: action.payload};
        }
        default: {
            return state;
        }
    }
}