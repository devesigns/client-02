import _ from 'lodash';
import {
    NOTIFICATION_REDUCER_UPDATE,
    NOTIFICATION_LOADING_STATUS_UPDATE
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,
    loadingStack: [],
    notifications: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (NOTIFICATION_REDUCER_UPDATE): {
            return { ...state, [action.payload.props]: action.payload.value }
        }
        case (NOTIFICATION_LOADING_STATUS_UPDATE): {
            newState = { 
                ...state
            }

            if (action.payload.loading === true) {
                newState.loadingStack.push(true);
                newState.loading = true;
            } else if (action.payload.loading === false) {
                newState.loadingStack.pop();

                if (newState.loadingStack.length == 0) {
                    newState.loading = false;
                }
            }

            return newState;

            // return {
            //     ...state,
            //     loading: action.payload.loading
            // }
        }
        default: {
            return state;
        }
    }
}