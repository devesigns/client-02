import _ from 'lodash';
import {
    ASSIGNMENT_FETCH,
    ASSIGNMENT_FETCH_FULL_SINGLE,
    ASSIGNMENT_REDUCER_UPDATE,
    ASSIGNMENT_INVITATION_FETCH,
    ASSIGNMENT_SINGLE_CHORE_STATUS_UPDATE,
    ASSIGNMENT_ROOM_STATUS_UPDATE,
    ASSIGNMENT_INVITATION_STATUS_UPDATE,
    ASSIGNMENT_MANAGEMENT_CANCEL
} from '../actions/types';

const INITIAL_STATE = {
    houses: [],
    massAssignDetail: {},
    invitations: {},
    choreForm: {},
    houseForm: {},
    selectedHouseArrayId: -1,
    selectedRoomArrayId: -1,
    selectedChoreArrayId: -1,
    crossFullHouseRefetch: {
        active: false,
    },
    fullHouseRefetch: {
        active: false
    },
    houseManagementAccepted: false,
    // invitationNotiPush: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case (ASSIGNMENT_REDUCER_UPDATE): {
            return { ...state, [action.payload.props]: action.payload.value }
        }
        case (ASSIGNMENT_FETCH): {
            return {
                ...state,
                houses: action.payload.houses
            }
        }
        case (ASSIGNMENT_FETCH_FULL_SINGLE):
            // Push the newly-fetched house with full data into the queue
            fullHouseRefetch = {
                active: false
            }

            return {
                ...state,
                houseForm: action.payload.value,
                fullHouseRefetch
            }
        case (ASSIGNMENT_SINGLE_CHORE_STATUS_UPDATE): {
            newHouseForm = { ...state.houseForm };
            houseId = action.payload.choreForm.id;

            newHouseForm.rooms[state.selectedRoomArrayId].chores[state.selectedChoreArrayId] = action.payload.choreForm;
            fullRoomComplete = true;
            for (i = 0; i < newHouseForm.rooms[state.selectedRoomArrayId].chores.length; i++) {
                if (newHouseForm.rooms[state.selectedRoomArrayId].chores[i].status != 'done') {
                    fullRoomComplete = false;
                }
            }

            if (fullRoomComplete) {
                newHouseForm.rooms[state.selectedRoomArrayId].completed = true;
            } else {
                newHouseForm.rooms[state.selectedRoomArrayId].completed = false;
            }

            // crossFullHouseRefetch = {
            //     cross: true,
            //     active: true,
            //     targetId: houseId,
            // }

            return {
                ...state,
                houseForm: newHouseForm,
                // crossFullHouseRefetch
            }
        }
        case (ASSIGNMENT_ROOM_STATUS_UPDATE): {
            newHouseForm = { ...state.houseForm };
            houseId = action.payload.roomForm.id;

            chores = [ ...newHouseForm.rooms[state.selectedRoomArrayId].chores ];
            newHouseForm.rooms[state.selectedRoomArrayId].completed = action.payload.completed;
            
            for (i = 0; i < chores.length; i++) {
                chores[i].status = action.payload.completed ? 'done' : 'doing';
            }

            newHouseForm.rooms[state.selectedRoomArrayId].chores = chores;
                
            // crossFullHouseRefetch = {
            //     cross: true,
            //     active: true,
            //     targetId: houseId,
            // }

            return {
                ...state,
                houseForm: newHouseForm,
                // crossFullHouseRefetch
            }
        }
        case (ASSIGNMENT_INVITATION_FETCH): {
            let newInvitations = { ...state.invitations };
            // let invitationNotiPush = state.invitationNotiPush;
            
            for (i = 0; i < action.payload.invitations.length; i++) {
                // if (!newInvitations[action.payload.invitations[i].id]) {
                //     invitationNotiPush = invitationNotiPush ? false : true;
                // }

                newInvitations[action.payload.invitations[i].id] = action.payload.invitations[i];
            }

            return {
                ...state,
                invitations: newInvitations,
                // invitationNotiPush
            }
        }
        case (ASSIGNMENT_INVITATION_STATUS_UPDATE):
            invitationType = state.invitations[action.payload.invitationId].invitation.type;

            newInvitations = { ...state.invitations };
            delete newInvitations[action.payload.invitationId];

            if (action.payload.status == 'accepted' && invitationType == 'receive_task') {
                return {
                    ...state,
                    houses: [
                        action.payload.houseData,
                        ...state.houses
                    ],
                    invitations: newInvitations
                }
            } else if (action.payload.status == 'accepted' && invitationType == 'manage_house') {
                return {
                    ...state,
                    invitations: newInvitations,
                    houseManagementAccepted: true
                }
            } else {
                return {
                    ...state,
                    invitations: newInvitations
                }
            }
        case (ASSIGNMENT_MANAGEMENT_CANCEL): {
            newHouses = [ ...state.houses ];
            newHouses.splice(state.selectedHouseArrayId, 1);

            return {
                ...state,
                houses: newHouses
            }
        }
        default: {
            return state;
        }
    }
}