/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
    BackHandler,
    View,
    StyleSheet,
    ActivityIndicator,
    UIManager,
    Platform
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import AppNavigator from './routes/Navigator';
import {
    setTopLevelNavigator
} from './utils';
import {
    CardRow,
    CustomText,
    CustomModal
} from '../src/components/common';
import { connect } from 'react-redux';
import {
    assignmentReducerUpdate
} from './actions';
import firebase, { Notification } from 'react-native-firebase';

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        position: "absolute",
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        justifyContent: "center",
        alignItems: "center",
        opacity: 0.5,
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        elevation: 999,
        zIndex: 999
    }
})

type Props = {};
class NetworkProvider extends Component<Props> {
    constructor(props){
        super(props);
        this.state = {
            isConnected: true
        }
    }

    UNSAFE_componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    checkMessagingPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            // user has permissions
            console.log('Messaging permission granted.');
        } else {
            // user doesn't have permission
            try {
                await firebase.messaging().requestPermission();
                // User has authorised
            } catch (error) {
                // User has rejected permissions
                alert('MyButler sẽ không thể gửi thông báo về công việc cho bạn.')
            }
        }
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this._handleConnectivityChange
        );

        NetInfo.isConnected.fetch().done((isConnected) => {
            if (isConnected) {
                this.setState({isConnected: true});
            } else {
                this.setState({isConnected: false});
            }
        });

        UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

        this.checkMessagingPermission();
        console.log('Hello there')

        // Build a channel (Android only)
        if (Platform.OS === 'android') {
            const notiChannel = new firebase.notifications.Android.Channel('mybutler-channel', 'Mybutler Channel', firebase.notifications.Android.Importance.Max)
                .setDescription('MyButler notification channel');

            // Create the channel
            firebase.notifications().android.createChannel(notiChannel);
        }

        // Listener for incoming notification
        this.removeNotificationListener = firebase.notifications().onNotification((notification: Notification) => {
            console.log(notification);

            // Process your notification as required
            const localNotification = new firebase.notifications.Notification({
                    show_in_foreground: true
                })
                .setNotificationId(notification.notificationId)
                .setTitle(notification.title)
                .setBody(notification.body);
            
            if (Platform.OS === 'android') {
                localNotification
                .android.setChannelId('mybutler-channel')
                .android.setSmallIcon('ic_stat_ic_notification')
                .android.setPriority(firebase.notifications.Android.Priority.High);
            }
    
            firebase.notifications().displayNotification(localNotification);
        });
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange

        );

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);

        this.removeNotificationDisplayedListener();
        this.removeNotificationListener();
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        // if (nextProps.invitationNotiPush) {
        //     pushNotification.localNotification('Lời mời công việc', 'Bạn có lời mời công việc mới cần xem xét!');
        //     this.props.assignmentReducerUpdate('invitationNotiPush', false);
        // }
    }

    _handleConnectivityChange = (isConnected) => {
        if (isConnected) {
            this.setState({isConnected: true});
        } else {
            this.setState({isConnected: false});
        }
    };

    handleBackButtonClick() {
        BackHandler.exitApp();
        return true;
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                {
                    this.props.loading ?
                    <View style={styles.loading} pointerEvents={'none'}>
                        <ActivityIndicator color='#29ABE2' animating={true} />
                    </View>
                    : null
                }
                <AppNavigator
                    ref={(navigatorRef) => {
                        setTopLevelNavigator(navigatorRef);
                    }}
                />
                <CustomModal
                    headerText={'Kết nối với mạng'}
                    visible={!this.state.isConnected}
                    onRequestClose={() =>
                        this.handleBackButtonClick()
                    }
                    hideConfirm={true}
                >
                    <CardRow>
                        <CustomText
                            style={{ color: '#29ABE2', fontStyle: 'italic' }}
                        >
                            Để sử dụng MyButler, hãy bật dữ liệu di động hoặc kết nối Wifi.
                        </CustomText>
                    </CardRow>
                </CustomModal>
            </View>
        );
    }
}

const mapStateToProps = ({ notification, assignment }) => {
    return {
        loading: notification.loading,
        // invitationNotiPush: assignment.invitationNotiPush
    }
}

export default connect(mapStateToProps, {
    assignmentReducerUpdate
})(NetworkProvider);
