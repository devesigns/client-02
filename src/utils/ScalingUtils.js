import { Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => {
    let scaledSize = height / guidelineBaseHeight * size;
    if (Platform.OS === 'ios') {
        scaledSize *= 0.8;
    }
    
    return scaledSize;
}
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;

export {scale, verticalScale, moderateScale};