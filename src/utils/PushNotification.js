// Disabled. Currently using FCM

import PushNotification from "react-native-push-notification";
import { PushNotificationIOS } from "react-native";

const configure = () => {
    PushNotification.configure({
        onRegister: function(token) {
            //process token
        },

        onNotification: function(notification) {
            // process the notification
            // required on iOS only
            notification.finish(PushNotificationIOS.FetchResult.NoData);
        },

        permissions: {
            alert: true,
            badge: true,
            sound: true
        },

        popInitialNotification: true,
        requestPermissions: true
    });
};

const localNotification = (title, message, onNotification = null) => {
    PushNotification.localNotification({
        autoCancel: true,
        largeIcon: "ic_launcher_round",
        smallIcon: "ic_notification",
        // bigText: "My big text that will be shown when notification is expanded",
        subText: 'Thông báo từ MyButler',
        color: "green",
        vibrate: true,
        vibration: 300,
        title,
        message,
        playSound: true,
        soundName: "default",
        onNotification: onNotification
    });
};

export { configure, localNotification };
