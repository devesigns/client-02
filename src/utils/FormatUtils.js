export const formatPrice = (value, jump) => {
    valueArray = value.toString().split('');
    formattedValue = '';

    count = 0;
    for (i = value.length - 1; i >= 0; i--) {
        count++;
        formattedValue = value[i] + formattedValue;

        if (count == jump && i != 0) {
            formattedValue = '.' + formattedValue;
            count = 0;
        }
    }

    return formattedValue;
}

// ---
weekDays = [
    'Chủ Nhật',
    'Thứ Hai',
    'Thứ Ba',
    'Thứ Tư',
    'Thứ Năm',
    'Thứ Sáu',
    'Thứ Bảy'
]

export const formatDate = (dateObj) => {
    let day = weekDays[dateObj.getDay()];
    let date = dateObj.getDate();
    let month = dateObj.getMonth() + 1;
    let year = dateObj.getFullYear();

    return day + ', ' + date + '-' + month + '-' + year;
}

export const formatDateAndTime = (dateString) => {
    let dateStringArray = dateString.split('T');
    let date = dateStringArray[0];
    let time = dateStringArray[1].split('Z')[0];

    let dateArray = date.split('-');
    let year = dateArray[0];
    let month = dateArray[1];
    let dateOfMonth = dateArray[2];

    let timeArray = time.split(':');
    let hour = timeArray[0];
    let minute = timeArray[1];

    return hour + ':' + minute + ', ' + dateOfMonth + '-' + month + '-' + year;
}