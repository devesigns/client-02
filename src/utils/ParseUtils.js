const dict = {
    'start_time': 'time',
    'house_rooms': 'rooms',
    'room_tasks': 'chores',
    'title': 'name',
    'total_tasks': 'totalChore',
    'total_rooms': 'totalRoom',
    'repeat_list': 'repeatList',
    'butler_id_list': 'butlerIdList',
    'provider_name': 'providerName',
    'total_price': 'totalPrice',
    'order_items': 'orderItems',
    'service_name': 'name',
    'profile_image': 'profileImage',
    'full_name': 'fullname',
    'created_at': 'createdAt',
    'from_user': 'fromUser',
    'room_name': 'roomName'
}

export const renameKey = (object) => {
    Object.keys(object).forEach(function(key) {
        if (typeof(object[key]) === 'object' && object[key] != null) {
            // If value of this key is an object
            object[key] = renameKey(object[key]);
        } else if (Array.isArray(object[key])) {
            // If is of an array
            // Not considering the case of array in array
            for (i = 0; i < object[key].length; i++) {
                if (typeof(object[key][i] == 'object')) {
                    object[key][i] = renameKey(object[key][i]);
                }
            }
        }

        if (dict[key]) {
            var newkey = dict[key];
            object[newkey] = object[key];
            delete object[key];
        }
    })

    return object;
}