import AsyncStorage from '@react-native-community/async-storage';
import {
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_FAIL,
    SEND_SMS_SUCCESS,
    SEND_SMS_FAIL,
    VERIFY_PHONE_SUCCESS,
    VERIFY_PHONE_FAIL,
    REGISTER_ACCOUNT_SUCCESS,
    REGISTER_ACCOUNT_FAIL,
    AUTH_REDUCER_UPDATE,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAIL,
    CHECK_USER_YES,
    CHECK_USER_NO,
    LOAD_REDUCER_UPDATE
} from './types';
import {
    notificationLoadingStatusUpdate
} from '../actions';

// import { hostname, version } from './constant';

export const authReducerUpdate = (props, value) => {
    return {
        type: AUTH_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const loadingReducerUpdate = (value) => {
    return {
        type: LOAD_REDUCER_UPDATE,
        payload: {
            value
        }
    }
}

export const login = (phone, password) => {
    return (dispatch) => {
        // Dispatch an action
        // Have to note that those functions are not actions, but action CREATORS
        // Therefore, here we dispatch their returned value, which is an action
        dispatch(notificationLoadingStatusUpdate(true));

        return  fetch( 'https://mybutler.giupviecxanh.vn/api/v1/auth/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: phone,
                    password: password

                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));
                
                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200) {
                    AsyncStorage.setItem('tokenButler', results.results.token);
                    dispatch({
                        type: AUTH_LOGIN_SUCCESS,
                        payload: {
                            token: results.results.token
                        }
                    })
                } else {
                    dispatch({
                        type: AUTH_LOGIN_FAIL,
                        payload: {
                            message: results.message
                        }
                    })
                }

            })
            .catch((error) => {
                console.log(error);
            });
    }
};

export const certificate = (phone) => {
    return (dispatch) => { 
        dispatch(notificationLoadingStatusUpdate(true));
        return  fetch('https://mybutler.giupviecxanh.vn/api/v1/auth/send-sms-token', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: phone
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200) {
                    dispatch({
                        type: SEND_SMS_SUCCESS
                    })
                } else {
                    dispatch({
                        type: SEND_SMS_FAIL,
                        payload: {
                            message: results.message
                        }
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    }
};

export const verifyPhone = (phone, token) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return  fetch('https://mybutler.giupviecxanh.vn/api/v1/auth/verify-phone-number', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: phone,
                    token: token
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200) {
                    dispatch({
                        type: VERIFY_PHONE_SUCCESS
                    })
                } else {
                    dispatch({
                        type: VERIFY_PHONE_FAIL,
                        payload: {
                            message: results.message
                        }
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    }
};

export const registerAccount = (name, phone, password) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return  fetch('https://mybutler.giupviecxanh.vn/api/v1/auth/register', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    fullname: name,
                    phone: phone,
                    password: password
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200) {
                    dispatch({
                        type: REGISTER_ACCOUNT_SUCCESS
                    })
                } else {
                    dispatch({
                        type: REGISTER_ACCOUNT_FAIL,
                        payload: {
                            message: results.message
                        }
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    }
};

export const resetPassword = (code, phone, password) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return  fetch('https://mybutler.giupviecxanh.vn/api/v1/auth/reset-password', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    verify_code: code,
                    phone: phone,
                    new_password: password
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200) {
                    dispatch({
                        type: RESET_PASSWORD_SUCCESS
                    })
                } else {
                    dispatch({
                        type: RESET_PASSWORD_FAIL,
                        payload: {
                            message: results.message
                        }
                    })
                }

            })
            .catch((error) => {
                console.error(error);
            });
    }
};

export const checkUser = (phone) => {
    return (dispatch) => { 
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user?phone=${phone}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Token 44035a46386552289dfbfd1ea5e774e6f6090df6",
            }}) 
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }
                    
                    console.log('API res', results);
                    if (results.count > 0) {
                        dispatch ({
                            type: CHECK_USER_YES
                        });    
                    } else {
                        dispatch ({
                            type: CHECK_USER_NO
                        });  
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}




