import {
    SERVICE_PROVIDER_FETCH,
    SERVICE_PROVIDER_SELECT,
    SERVICE_LIST_FETCH,
    SERVICE_LIST_SELECT,
    SERVICE_ORDER_CREATE
} from './types';
import {
    notificationLoadingStatusUpdate
} from '../actions';

export const serviceProviderFetch = (userToken) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/services/providers', {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Token ${userToken}`
            }}) 
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200 || results.results.length > 0) {
                    dispatch ({
                        type: SERVICE_PROVIDER_FETCH,
                        payload: results.results
                    });
                } else if (results.code) {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const serviceProviderSelect = (providerId) => {
    return {
        type: SERVICE_PROVIDER_SELECT,
        payload: providerId
    }
}

export const serviceListFetch = (providerId, userToken) => {
    // Call API to get service list based on provider id
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/services?providerId='+providerId, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Token ${userToken}`
            }}) 
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);

                    if (results.code === 200 || results.results.length > 0) {
                        dispatch ({
                            type: SERVICE_LIST_FETCH,
                            payload: results.results
                        });
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const serviceListSelect = (selectedServiceList) => {
    return {
        type: SERVICE_LIST_SELECT,
        payload: selectedServiceList
    }
}

export const serviceOrderCreate = (serviceOrder, userToken, providerId) => {
    // Call API to create order with "serviceOrder"
    let date = new Date(serviceOrder.detail.date + " " + serviceOrder.detail.time);
    date = date.toISOString();
    let arrItems = [];
    for (let i=0; i < serviceOrder.serviceList.length; i++)
    {
        arrItems.push({service: serviceOrder.serviceList[i].id, quantity: serviceOrder.serviceList[i].quantity });
    }
    
    return (dispatch) => { 
        dispatch(notificationLoadingStatusUpdate(true));
        return  fetch('https://mybutler.giupviecxanh.vn/api/v1/orders', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    "Authorization": `Token ${userToken}`
                },
                body: JSON.stringify({
                    address: serviceOrder.detail.address,
                    start_time: date,
                    repeat: serviceOrder.detail.repeat == 'none' ? '' : serviceOrder.detail.repeat,
                    repeat_list: serviceOrder.detail.repeatList,
                    note:serviceOrder.detail.note,
                    provider: providerId,
                    items: arrItems
                })
            })
            .then((response) => {
                console.log('API Res', response);
                if (response.status == 201) {
                    return response.json();
                } else {
                    alert('Có lỗi xảy ra, vui lòng liên hệ admin!');
                    return null;
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API res', results);
                if (results.code === 200 || results.id) {
                    serviceOrder.orderId = results.id;
                    dispatch({
                        type: SERVICE_ORDER_CREATE,
                        payload: serviceOrder
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                } 
            })
            .catch((error) => {
                console.error(error);
            });
    }
} 