import {
    SERVICE_ORDER_CANCEL,
    SERVICE_HISTORY_REDUCER_UPDATE,
    SERVICE_HISTORY_FETCH
} from './types';
import {
    renameKey
} from '../utils';
import {
    notificationLoadingStatusUpdate
} from '../actions';

export const serviceHistoryReducerUpdate = (props, value) => {
    return {
        type: SERVICE_HISTORY_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const serviceOrderFetch = (userToken, offset) => {
    // API to fetch all service orders

    // return {
    //     type: SERVICE_HISTORY_REDUCER_UPDATE,
    //     payload: {
    //         props: 'orders',
    //         value: orders
    //     }
    // }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/orders?offset=${offset}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.results.length > 0) {
                        orders = results.results;
                        for (i = 0; i < orders.length; i++) {
                            orders[i] = renameKey(orders[i]);
                        }

                        dispatch({
                            type: SERVICE_HISTORY_FETCH,
                            payload: {
                                orders
                            }
                        })
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const serviceOrderCancel = (userToken, orderId) => {
    // API to cancel the order

    data = {
        status: 'canceled'
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/orders/${orderId}`, {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`,
                    },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        alert('Có lỗi xảy ra. Vui lòng liên hệ admin!');
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        orderForm = renameKey(results);

                        dispatch({
                            type: SERVICE_ORDER_CANCEL,
                            payload: {
                                orderForm
                            }
                        })
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }

    // return {
    //     type: null,
    //     payload: null
    // }
}