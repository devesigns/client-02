import _ from 'lodash';
import {
    ASSIGNMENT_FETCH_FULL_SINGLE,
    ASSIGNMENT_REDUCER_UPDATE,
    ASSIGNMENT_SINGLE_CHORE_STATUS_UPDATE,
    ASSIGNMENT_ROOM_STATUS_UPDATE,
    ASSIGNMENT_INVITATION_FETCH,
    ASSIGNMENT_INVITATION_STATUS_UPDATE,
    ASSIGNMENT_MANAGEMENT_CANCEL,
    ASSIGNMENT_FETCH
} from './types';
import { renameKey } from '../utils';
import {
    notificationLoadingStatusUpdate
} from '../actions';

export const assignmentReducerUpdate = (props, value) => {
    return {
        type: ASSIGNMENT_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const assignmentFetch = (userToken) => {
    // API getting assigned house data
    
    // return {
    //     type: ASSIGNMENT_REDUCER_UPDATE,
    //     payload: {
    //         props: 'houses',
    //         value: houses
    //     }
    // }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/houses?butler=1', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.results.length > 0) {
                        houses = results.results;
                        for (i = 0; i < houses.length; i++) {
                            houses[i] = renameKey(houses[i]);
                        }

                        dispatch({
                            type: ASSIGNMENT_FETCH,
                            payload: {
                                houses
                            }
                        })
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const assignmentFetchFullSingle = (userToken, houseId) => {
    // API getting full data of one house (including rooms & tasks)
    // Then push the result to the reducer

    // return {
    //     type: ASSIGNMENT_FETCH_FULL_SINGLE,
    //     payload: {
    //         id: houseId,
    //         value: house
    //     }
    // }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/${houseId}/assigned-tasks`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        fullHouseForm = results.id ? renameKey(results) : renameKey(results.results);

                        dispatch({
                            type: ASSIGNMENT_FETCH_FULL_SINGLE,
                            payload: {
                                id: houseId,
                                value: fullHouseForm
                            }
                        })
                    } else if (results.code === 2000) {
                        dispatch({
                            type: ASSIGNMENT_MANAGEMENT_CANCEL,
                            payload: {
                                houseId
                            }
                        })
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const assignmentSingleChoreStatusUpdate = (userToken, choreId, status) => {
    data = {
        status
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks/${choreId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    choreForm = renameKey(results);

                    dispatch({
                        type: ASSIGNMENT_SINGLE_CHORE_STATUS_UPDATE,
                        payload: {
                            choreForm
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// Update complete status of the whole room
export const assignmentRoomStatusUpdate = (userToken, roomId, status) => {
    console.log('Update room status', roomId, status);

    data = {
        completed: status
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/rooms/${roomId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    roomForm = renameKey(results);

                    dispatch({
                        type: ASSIGNMENT_ROOM_STATUS_UPDATE,
                        payload: {
                            roomForm,
                            completed: status
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// Assignment Invitation
export const assignmentInvitationFetch = (userToken, offset = 0) => {
    // API to get assignment invitation

    // return {
    //     type: ASSIGNMENT_REDUCER_UPDATE,
    //     payload: {
    //         props: 'invitations',
    //         value: invitations
    //     }
    // }

    return (dispatch) => {
        // dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/invitations?offset=${offset}&status=pending`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    // dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    // console.log('API Res Invitation', results);
                    if (results.code === 200 || _.has(results, 'count')) {
                        try {
                            if (results.results.length > 0) {
                                invitations = results.results;

                                for (i = 0; i < invitations.length; i++) {
                                    invitations[i] = renameKey(invitations[i]);
                                }

                                dispatch({
                                    type: ASSIGNMENT_INVITATION_FETCH,
                                    payload: {
                                        invitations
                                    }
                                })
                            }
                        } catch {
                            // Do nothing
                        }
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const assignmentInvitationStatusUpdate = (userToken, invitationId, status) => {
    // return {
    //     type: ASSIGNMENT_FETCH_FULL_SINGLE,
    //     payload: {
    //         id: 0,
    //         value: house
    //     }
    // }

    data = {
        status
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/invitations/${invitationId}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    houseData = results.invitation.detail.house;

                    dispatch({
                        type: ASSIGNMENT_INVITATION_STATUS_UPDATE,
                        payload: {
                            status,
                            invitationId,
                            houseData
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// ---
// Rejecting a house (rejecting all chores which belong to that house)
export const assignmentManagementCancel = (userToken, houseId) => {
    console.log('Canceling house', houseId);

    data = {
        house: houseId
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks/deny-house-tasks`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }
                
                console.log('API Res', results);
                if (results.code === 200) {
                    dispatch({
                        type: ASSIGNMENT_MANAGEMENT_CANCEL,
                        payload: {
                            houseId
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}