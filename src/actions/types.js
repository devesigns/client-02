// Type Id for service flow
export const SERVICE_PROVIDER_FETCH = 'service_provider_fetch';
export const SERVICE_PROVIDER_SELECT = 'service_provider_select';
export const SERVICE_LIST_FETCH = 'service_list_fetch';
export const SERVICE_LIST_SELECT = 'service_list_select';
export const SERVICE_ORDER_CREATE = 'service_order_create';

// Type Id for house & chore flow
export const HOUSE_FETCH = 'house_fetch';
export const HOUSE_FETCH_FULL_SINGLE = 'house_fetch_full_single';
export const HOUSE_REDUCER_UPDATE = 'house_reducer_update';

export const HOUSE_CREATE_HOUSE = 'house_create_house';
export const HOUSE_UPDATE_HOUSE = 'house_update_house';
export const HOUSE_DELETE_HOUSE = 'house_delete_house';

export const HOUSE_CREATE_ROOM = 'house_create_room';
export const HOUSE_CREATE_ROOM_TEMPLATE = 'house_create_room_template';
export const HOUSE_UPDATE_ROOM = 'house_update_room';
export const HOUSE_DELETE_ROOM = 'house_delete_room';

export const HOUSE_CREATE_CHORE = 'house_create_chore';
export const HOUSE_UPDATE_CHORE = 'house_update_chore';
export const HOUSE_DELETE_CHORE = 'house_delete_chore';
export const HOUSE_MASS_ASSIGN_CHORE = 'house_mass_assign_chore';

export const HOUSE_MANAGEMENT_CANCEL = 'house_management_cancel';

// Type Id for assignment flow
export const ASSIGNMENT_FETCH = 'assignment_fetch';
export const ASSIGNMENT_FETCH_FULL_SINGLE = 'assignment_fetch_full_single';
export const ASSIGNMENT_REDUCER_UPDATE = 'assignment_reducer_update';
export const ASSIGNMENT_SINGLE_CHORE_STATUS_UPDATE = 'assignment_single_chore_status_update';
export const ASSIGNMENT_ROOM_STATUS_UPDATE = 'assignment_room_status_update';
export const ASSIGNMENT_INVITATION_FETCH = 'assignment_invitation_fetch';
export const ASSIGNMENT_INVITATION_STATUS_UPDATE = 'assignment_invitation_status_update';
export const ASSIGNMENT_MANAGEMENT_CANCEL = 'ASSIGNMENT_MANAGEMENT_CANCEL';

// Type Id for user management reducer
export const USER_REDUCER_UPDATE = 'user_reducer_update';
export const USER_CONTACT_FETCH = 'user_contact_fetch';
export const CONTACT_USER_DELETE = 'contact_user_delete';
export const CONTACT_USER_CREATE = 'contact_user_create';

// Type Id for service history
export const SERVICE_HISTORY_REDUCER_UPDATE = 'service_history_reducer_update';
export const SERVICE_HISTORY_FETCH = 'service_history_fetch';
export const SERVICE_ORDER_CANCEL = 'service_order_cancel';

// Type for authentication
export const AUTH_REDUCER_UPDATE = 'auth_reducer_update';
export const AUTH_LOGIN_SUCCESS = 'auth_login_success';
export const AUTH_LOGIN_FAIL = 'auth_login_fail';
export const SEND_SMS_SUCCESS = 'send_sms_success';
export const SEND_SMS_FAIL = 'send_sms_fail';
export const VERIFY_PHONE_SUCCESS ='verify_phone_success';
export const VERIFY_PHONE_FAIL ='verify_phone_fail';
export const REGISTER_ACCOUNT_SUCCESS ='register_account_success';
export const REGISTER_ACCOUNT_FAIL = 'register_account_fail';
export const RESET_PASSWORD_SUCCESS = 'reset_password_success';
export const RESET_PASSWORD_FAIL = 'reset_password_fail';
export const CHECK_USER_YES = 'check_user_yes';
export const CHECK_USER_NO = 'check_user_no';

// Type for notification
export const NOTIFICATION_REDUCER_UPDATE = 'notification_reducer_update';
export const NOTIFICATION_LOADING_STATUS_UPDATE = 'notification_loading_status_update';