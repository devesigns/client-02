import { Platform } from 'react-native';
import { renameKey } from '../utils';
import {
    HOUSE_FETCH,
    HOUSE_FETCH_FULL_SINGLE,
    HOUSE_CREATE_HOUSE,
    HOUSE_REDUCER_UPDATE,
    HOUSE_UPDATE_HOUSE,
    HOUSE_DELETE_HOUSE,
    HOUSE_CREATE_ROOM,
    HOUSE_CREATE_ROOM_TEMPLATE,
    HOUSE_UPDATE_ROOM,
    HOUSE_DELETE_ROOM,
    HOUSE_CREATE_CHORE,
    HOUSE_UPDATE_CHORE,
    HOUSE_DELETE_CHORE,
    HOUSE_MASS_ASSIGN_CHORE,
    HOUSE_MANAGEMENT_CANCEL
} from './types';
import {
    notificationLoadingStatusUpdate
} from '../actions';

export const houseReducerUpdate = (props, value) => {
    return {
        type: HOUSE_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

// House actions
export const houseFetch = (userToken) => {
    // API getting house data

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/houses', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.results.length > 0) {
                        houses = results.results;
                        for (i = 0; i < houses.length; i++) {
                            houses[i] = renameKey(houses[i]);
                        }

                        dispatch({
                            type: HOUSE_FETCH,
                            payload: {
                                houses
                            }
                        })
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const houseFetchFullSingle = (userToken, houseId, loading = true) => {
    // API getting full data of one house (including rooms & tasks)
    // Then push the result to the reducer

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/${houseId}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        fullHouseForm = results.id ? renameKey(results) : renameKey(results.results);

                        dispatch({
                            type: HOUSE_FETCH_FULL_SINGLE,
                            payload: {
                                id: houseId,
                                value: fullHouseForm
                            }
                        })
                    } else if (results.code == 1003) {
                        console.log('Resource not found.')
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const houseCreate = (userToken) => {
    console.log("Creating a new house...");

    data = {
        'name': 'Nhà mẫu'
    }

    // Call API to create a new house with user-defined template
    // API will return the new house object
    // ...

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/houses', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.id) {
                        newHouseData = renameKey(results);

                        dispatch({
                            type: HOUSE_CREATE_HOUSE,
                            payload: {
                                houseForm: newHouseData
                            }
                        })
                    } else {
                        alert('Có lỗi khi tạo nhà mới. Vui lòng thử lại!');
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const houseUpdate = (userToken, houseForm, oldHouseForm) => {
    console.log('Updating house with id ' + houseForm.id, houseForm);

    // Call API to update house with give houseId
    // ...

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));

        // Processing which managers to delete and which to add
        remainingManagerIdList = [];
        managersToDelete = [];
        managersToAdd = [];

        for (i = 0; i < houseForm.managers.length; i++) {
            isNew = true;

            for (j = 0; j < oldHouseForm.managers.length; j++) {
                if (houseForm.managers[i].id == oldHouseForm.managers[j].id) {
                    isNew = false;
                    break;
                }

                if (houseForm.managers[i].manager.id == oldHouseForm.managers[j].manager.id) {
                    isNew = false;
                    break;
                }

                isNew = true;
            }

            if (isNew) {
                managersToAdd.push(houseForm.managers[i].manager.id);
            }

            if (houseForm.managers[i].id) {
                remainingManagerIdList.push(houseForm.managers[i].id);
            }
        }

        for (i = 0; i < oldHouseForm.managers.length; i++) {
            if (!remainingManagerIdList.includes(oldHouseForm.managers[i].id)) {
                managersToDelete.push(oldHouseForm.managers[i].id);
            }
        }

        if (managersToDelete.length > 0) {
            return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/managers/delete`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    house: houseForm.id,
                    managers: managersToDelete
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log('Manager Delete API Res', results);
                if (results.code === 200) {
                    let houseUpdateData = new FormData();
                    houseUpdateData.append('name', houseForm.name);
                    houseUpdateData.append('address', houseForm.address);
                    houseUpdateData.append('note', houseForm.note);
                    
                    if (managersToAdd.length > 0) {
                        for (i = 0; i < managersToAdd.length; i++) {
                            houseUpdateData.append(`managers[${i}]`, managersToAdd[i]);
                        }
                    }

                    if (houseForm.imageFile) {
                        houseUpdateData.append('image', {
                            name: houseForm.imageFile.fileName,
                            type: houseForm.imageFile.type,
                            uri: Platform.OS === 'android' ? houseForm.imageFile.uri : houseForm.imageFile.uri.replace("file://", "")
                        });
                    }

                    return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/${houseForm.id}`, {
                            method: 'PUT',
                            headers: {
                                'Authorization': `Token ${userToken}`,
                            },
                            body: houseUpdateData
                        })
                        .then((response) => {
                            if (response.status == 500) {
                                console.log(response);
                                return null;
                            } else {
                                return response.json();
                            }
                        })
                        .then((results) => {
                            dispatch(notificationLoadingStatusUpdate(false));

                            if (!results) {
                                return;
                            }

                            console.log('API Res', results);
                            if (results.code === 200 || results.id) {
                                houseForm = results.id ? renameKey(results) : renameKey(results.results);

                                dispatch({
                                    type: HOUSE_UPDATE_HOUSE,
                                    payload: {
                                        houseForm
                                    }
                                })
                            } else {
                                alert(`${results.code}: ${results.message}`);
                            }
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        } else {
            let houseUpdateData = new FormData();
            houseUpdateData.append('name', houseForm.name);
            houseUpdateData.append('address', houseForm.address);
            houseUpdateData.append('note', houseForm.note);
            
            if (managersToAdd.length > 0) {
                for (i = 0; i < managersToAdd.length; i++) {
                    houseUpdateData.append(`managers[${i}]`, managersToAdd[i]);
                }
            }

            if (houseForm.imageFile) {
                houseUpdateData.append('image', {
                    name: houseForm.imageFile.fileName,
                    type: houseForm.imageFile.type,
                    uri: Platform.OS === 'android' ? houseForm.imageFile.uri : houseForm.imageFile.uri.replace("file://", "")
                });
            }

            return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/${houseForm.id}`, {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Token ${userToken}`,
                    },
                    body: houseUpdateData
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        houseForm = results.id ? renameKey(results) : renameKey(results.results);

                        dispatch({
                            type: HOUSE_UPDATE_HOUSE,
                            payload: {
                                houseForm
                            }
                        })
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }
}

export const houseDelete = (userToken, houseId) => {
    console.log('Deleting house with id ' + houseId);

    // Call API to delete house with given houseId
    // ...

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/${houseId}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response;
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.status === 204) {
                    dispatch({
                        type: HOUSE_DELETE_HOUSE,
                        payload: {
                            id: houseId
                        }
                    })
                } else if (results.code === 1003) {
                    alert('Bạn không có quyền xóa căn nhà này!');
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// Rooms
export const roomTemplateFetch = (userToken) => {
    // API get sample rooms

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/houses/get-sample-rooms', {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.results.length > 0) {
                        templateRooms = results.results;
                        for (i = 0; i < templateRooms.length; i++) {
                            templateRooms[i] = renameKey(templateRooms[i]);
                        }

                        dispatch({
                            type: HOUSE_REDUCER_UPDATE,
                            payload: {
                                props: 'templateRooms',
                                value: templateRooms
                            }
                        })
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const roomTemplateCreate = (userToken, houseId, roomArray) => {
    console.log('Creating multiple template rooms...');

    // API create multiple template rooms

    let data = {
        house: houseId,
        rooms: roomArray
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/update-house-with-sample-rooms`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    houseForm = results.id ? renameKey(results) : renameKey(results.results);

                    dispatch({
                        type: HOUSE_CREATE_ROOM_TEMPLATE,
                        payload: {
                            houseForm
                        }
                    });
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const roomCreate = (userToken, houseId) => {
    console.log("Creating a new empty room...");

    // Call API to create a new empty room with given houseId
    // ...

    let data = {
        house: houseId,
        name: 'Phòng mới'
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/rooms`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    newRoom = results.id ? renameKey(results) : renameKey(results.results);

                    dispatch({
                        type: HOUSE_CREATE_ROOM,
                        payload: {
                            roomForm: newRoom
                        }
                    });
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const roomUpdate = (userToken, roomForm) => {
    console.log('Updating room with id ' + roomId, roomForm);

    // Call API to update room with given houseId, roomId
    // ...

    data = {
        house: roomForm.house,
        name: roomForm.name
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/rooms/${roomForm.id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    roomForm = renameKey(results);

                    dispatch({
                        type: HOUSE_UPDATE_ROOM,
                        payload: {
                            roomForm: roomForm
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const roomDelete = (userToken, roomIdArray) => {
    console.log('Deleting room with id ' + roomId);

    // Call API to delete room with given houseId and roomId
    // ...

    // return {
    //     type: HOUSE_DELETE_ROOM,
    //     payload: null
    // }

    data = {
        id_list: roomIdArray
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/rooms/delete`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200) {
                    dispatch({
                        type: HOUSE_DELETE_ROOM,
                        payload: {
                            roomIdArray
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// Chore actions
export const choreFetchSingle = (userToken, choreId) => {
    // Call API to fetch a single chore (from ChoreMainScene,
    // because we navigate directly from nothing to one specific chore)
    // ...
}

export const choreCreate = (userToken, choreForm) => {
    console.log("Creating a new chore...", choreForm);

    // Call API to create a new chore
    // ...

    // return {
    //     type: HOUSE_CREATE_CHORE,
    //     payload: {
    //         choreForm: newChore
    //     }
    // }

    if (choreForm.repeat == '') {
        choreForm.repeat = 'daily';
    }

    return async (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        let imageRes = [];
        if (choreForm.imagesToUpload.length) {
            let imageData = new FormData();
            for (let i = 0; i < choreForm.imagesToUpload.length; i++) {
                imageData.append('image', {
                    name: choreForm.imagesToUpload[i].modificationDate + '.jpg',
                    type: 'image/jpeg',
                    uri: Platform.OS === 'android' ? choreForm.imagesToUpload[i].path : choreForm.imagesToUpload[i].path.replace("file://", "")
                });
            }
    
            await fetch('https://mybutler.giupviecxanh.vn/api/v1/tasks/upload-image', {
                method: 'POST',
                headers: {
                    'Authorization': `Token ${userToken}`
                },
                body: imageData
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log('Image API Res', results);
                if (results.code === 200) {
                    imageRes = results.results['image_urls'];
                } else {
                    let errorString = '';
                    if (results.code) {
                        errorString += `${results.code}: ${results.message}`
                    } else {
                        errorString += results;
                    }
                    alert('Có lỗi xảy ra khi upload ảnh. Vui lòng thử lại!\n' + errorString);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }

        let data = {
            house: choreForm.house,
            rooms: [ choreForm.room ],
            title: choreForm.name,
            priority: choreForm.priority,
            repeat: choreForm.repeat,
            repeat_list: choreForm.repeatList,
            butler: choreForm.butler.id,
            note: choreForm.note
        }

        if (imageRes.length) {
            data.images = imageRes;
        }

        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Token ${userToken}`
            },
            body: JSON.stringify(data)
        })
        .then((response) => {
            if (response.status == 500) {
                console.log(response);
                return null;
            } else {
                return response.json();
            }
        })
        .then((results) => {
            dispatch(notificationLoadingStatusUpdate(false));

            if (!results) {
                return;
            }

            console.log('Chore Create API Res', results);
            if (results.code === 200 || results.length > 0) {
                chores = results;
                for (i = 0; i < chores.length; i++) {
                    chores[i] = renameKey(chores[i]);
                }

                dispatch({
                    type: HOUSE_CREATE_CHORE,
                    payload: {
                        chores
                    }
                });
            } else {
                alert(`${results.code}: ${results.message}`);
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const choreUpdate = (userToken, choreForm) => {
    console.log('Updating chore... ', choreForm);

    // Call API to update chore with give houseId, roomId, choreId
    // ...

    return async (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        let imageUploadRes = [];
        if (choreForm.imagesToUpload.length) {
            let imageData = new FormData();
            for (let i = 0; i < choreForm.imagesToUpload.length; i++) {
                imageData.append('image', {
                    name: choreForm.imagesToUpload[i].modificationDate + '.jpg',
                    type: 'image/jpeg',
                    uri: Platform.OS === 'android' ? choreForm.imagesToUpload[i].path : choreForm.imagesToUpload[i].path.replace("file://", "")
                });
            }

            await fetch('https://mybutler.giupviecxanh.vn/api/v1/tasks/upload-image', {
                method: 'POST',
                headers: {
                    'Authorization': `Token ${userToken}`
                },
                body: imageData
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log('Image Upload API Res', results);
                if (results.code === 200) {
                    imageUploadRes = results.results['image_urls'];
                } else {
                    let errorString = '';
                    if (results.code) {
                        errorString += `${results.code}: ${results.message}`
                    } else {
                        errorString += results;
                    }
                    alert('Có lỗi xảy ra khi upload ảnh. Vui lòng thử lại!\n' + errorString);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }

        if (choreForm.imagesToDelete.length) {
            await fetch('https://mybutler.giupviecxanh.vn/api/v1/tasks/delete-task-image', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    id_list: choreForm.imagesToDelete
                })
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log('Image Delete API Res', results);
                if (results.code === 200) {
                    return;
                } else {
                    let errorString = '';
                    if (results.code) {
                        errorString += `${results.code}: ${results.message}`
                    } else {
                        errorString += results;
                    }
                    alert('Có lỗi xảy ra khi xóa ảnh. Vui lòng thử lại!\n' + errorString);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }

        let data = {
            house: choreForm.house,
            rooms: [ choreForm.room ],
            title: choreForm.name,
            priority: choreForm.priority,
            repeat: choreForm.repeat,
            repeat_list: choreForm.repeatList,
            note: choreForm.note
        }

        console.log('Task update data', data);

        if (imageUploadRes) {
            data.images = imageUploadRes;
        }

        if (choreForm.butler === null) {
            data.butler = null;
        } else if (choreForm.butler === undefined) {
            
        } else {
            data.butler = choreForm.butler.id;
        }

        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks/${choreForm.id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Token ${userToken}`
            },
            body: JSON.stringify(data)
        })
        .then((response) => {
            if (response.status == 500) {
                console.log(response);
                return null;
            } else {
                return response.json();
            }
        })
        .then((results) => {
            dispatch(notificationLoadingStatusUpdate(false));

            if (!results) {
                return;
            }

            console.log('Task Update API Res', results);
            if (results.code === 200 || results.id) {
                choreForm = renameKey(results);

                dispatch({
                    type: HOUSE_UPDATE_CHORE,
                    payload: {
                        choreForm: choreForm
                    }
                })
            } else {
                alert(`${results.code}: ${results.message}`);
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const choreDelete = (userToken, choreIdArray) => {
    console.log('Chore delete id ', choreIdArray);
    // Call API to delete an array of chores
    // ...

    data = {
        id_list: choreIdArray
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks/delete`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200) {
                    dispatch({
                        type: HOUSE_DELETE_CHORE,
                        payload: {
                            choreIdArray
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// Chore mass assign
export const choreMassAssign = (userToken, choreIdArray, selectedUserId) => {
    console.log('Mass assign', choreIdArray);

    data = {
        tasks: choreIdArray,
        butler: selectedUserId
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/tasks/tasks-to-butler`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.length > 0) {
                    dispatch({
                        type: HOUSE_MASS_ASSIGN_CHORE
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// ---
// Cancel house management
export const houseManagementCancel = (userToken, houseId) => {
    data = {
        house: houseId
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/houses/managers/deny`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }
                
                console.log('API Res', results);
                if (results.code === 200) {
                    dispatch({
                        type: HOUSE_MANAGEMENT_CANCEL,
                        payload: {
                            houseId
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}