import _ from 'lodash';
import {
    USER_REDUCER_UPDATE,
    USER_CONTACT_FETCH,
    CONTACT_USER_DELETE,
    CONTACT_USER_CREATE
} from './types';
import {
    renameKey,
    navigate
} from '../utils';
import {
    Platform
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
    notificationLoadingStatusUpdate
} from '../actions';

export const userReducerUpdate = (props, value) => {
    return {
        type: USER_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const usersInContactFetch = (userToken, offset = 0) => {
    // return {
    //     type: USER_REDUCER_UPDATE,
    //     payload: {
    //         props: 'usersInContact',
    //         value: usersInContact
    //     }
    // }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user/contacts?offset=${offset}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || _.has(results, 'count')) {
                        if (results.results.length > 0) {
                            try {
                                users = results.results;
                                for (i = 0; i < users.length; i++) {
                                    users[i] = renameKey(users[i]);
                                }

                                dispatch({
                                    type: USER_CONTACT_FETCH,
                                    payload: {
                                        props: 'usersInContact',
                                        value: users
                                    }
                                })
                            } catch {
                                // Do nothing
                            }
                        }
                    } else if (results.code === 1002) {
                        // alert('Có lỗi xảy ra! Vui lòng đăng nhập lại.');
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const getUserByPhone = (userToken, phone) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user?phone=${phone}`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`,
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.results.length > 0) {
                        users = results.results;
                        for (i = 0; i < users.length; i++) {
                            users[i] = renameKey(users[i]);
                            users[i] = {
                                user: users[i]
                            }
                        }

                        dispatch({
                            type: USER_CONTACT_FETCH,
                            payload: {
                                props: 'usersInContact',
                                value: users
                            }
                        })
                    } else if (results.results.length == 0) {
                        alert('Người dùng với số điện thoại này không tồn tại!');
                    } else if (results.code) {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const getUserProfile = (userToken) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user/profile`, {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`,
                    }
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        userProfile = renameKey(results);

                        dispatch({
                            type: USER_REDUCER_UPDATE,
                            payload: {
                                props: 'userProfile',
                                value: userProfile
                            }
                        })
                    } else if (results.code === 1002) {
                        alert('Có lỗi xảy ra! Vui lòng đăng nhập lại.');
                        AsyncStorage.clear();
                        navigate('Auth');
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const userUpdate = (userToken, userProfile) => {
    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        if (userProfile.imageFile) {
            data = new FormData();
            data.append('fullname', userProfile.fullname);
            data.append('profile_image', {
                name: userProfile.imageFile.fileName,
                type: userProfile.imageFile.type,
                uri: Platform.OS === 'android' ? userProfile.imageFile.uri : userProfile.imageFile.uri.replace("file://", "")
            });

            return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user?phone=${userProfile.cellphone}`, {
                    method: 'PUT',
                    headers: {
                        'Authorization': `Token ${userToken}`,
                    },
                    body: data
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        userProfile = renameKey(results);
            
                        dispatch({
                            type: USER_REDUCER_UPDATE,
                            payload: {
                                props: 'userProfile',
                                value: userProfile
                            }
                        })
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        } else {
            data = {
                fullname: userProfile.fullname
            }
        
            return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user?phone=${userProfile.cellphone}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    userProfile = renameKey(results);
        
                    dispatch({
                        type: USER_REDUCER_UPDATE,
                        payload: {
                            props: 'userProfile',
                            value: userProfile
                        }
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }
}

export const userChangePassword = (userToken, newPassword) => {
    console.log('Change password...', newPassword);

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/auth/change-password`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Token ${userToken}`,
                    },
                    body: JSON.stringify({
                        new_password: newPassword
                    })
                })
                .then((response) => {
                    if (response.status == 500) {
                        console.log(response);
                        return null;
                    } else {
                        return response.json();
                    }
                })
                .then((results) => {
                    dispatch(notificationLoadingStatusUpdate(false));

                    if (!results) {
                        return;
                    }

                    console.log('API Res', results);
                    if (results.code === 200 || results.id) {
                        alert('Đổi mật khẩu thành công.')

                        dispatch({
                            type: USER_REDUCER_UPDATE,
                            payload: {
                                props: 'changePassword',
                                value: 'success'
                            }
                        })
                    } else {
                        alert(`${results.code}: ${results.message}`);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
    }
}

export const contactUserDelete = (userToken, contactId) => {
    console.log('Deleting contact user...', contactId);

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user/contacts/${contactId}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                }
            })
            .then((response) => {
                if (response.status == 500) {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin');
                    console.log(response);
                    return null;
                } else if (response.status == 204) {
                    return response;
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }

                console.log('API Res', results);
                if (results.status === 204) {
                    dispatch({
                        type: CONTACT_USER_DELETE
                    })
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

export const contactUserCreate = (userToken, phone) => {
    console.log('Creating new contact user...', phone);

    data = {
        phone
    }

    return (dispatch) => {
        dispatch(notificationLoadingStatusUpdate(true));
        return fetch(`https://mybutler.giupviecxanh.vn/api/v1/user/contacts`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify(data)
            })
            .then((response) => {
                console.log(response);
                if (response.status == 500) {
                    console.log(response);
                    return null;
                } else {
                    return response.json();
                }
            })
            .then((results) => {
                dispatch(notificationLoadingStatusUpdate(false));

                if (!results) {
                    return;
                }
                
                console.log('API Res', results);
                if (results.code === 200 || results.id) {
                    newContact = renameKey(results);

                    dispatch({
                        type: CONTACT_USER_CREATE,
                        payload: {
                            value: newContact
                        }
                    })
                } else if (results.code === 1206) {
                    alert('Người dùng với SĐT này không tồn tại. Bạn hãy mời họ dùng myButler để tạo tài khoản trước nhé!');
                } else {
                    alert(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}