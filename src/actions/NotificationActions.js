import _ from 'lodash';
import {
    NOTIFICATION_REDUCER_UPDATE,
    NOTIFICATION_LOADING_STATUS_UPDATE
} from './types';
// import {
//     renameKey,
//     pushNotification
// } from '../utils';

export const notificationReducerUpdate = (props, value) => {
    return {
        type: NOTIFICATION_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const notificationLoadingStatusUpdate = (loading) => {
    return {
        type: NOTIFICATION_LOADING_STATUS_UPDATE,
        payload: {
            loading
        }
    }
}

export const notificationRegisterFCM = (userToken, fcmToken) => {
    return (dispatch) => {
        return fetch('https://mybutler.giupviecxanh.vn/api/v1/notifications/register_device/gcm', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    registration_id: fcmToken,
                    cloud_message_type: 'FCM'
                })
            })
            .then((res) => {
                console.log('API Res', res);
                if (res.status === 200) {
                    return res.json();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng liên hệ admin!')
                    return null;
                }
            })
            .then((results) => {
                if (!results) {
                    return;
                }

                console.log(results);
                console.log('----------');

                if (results.code === 200 || results.results === 200) {
                    dispatch({
                        type: NOTIFICATION_REDUCER_UPDATE,
                        payload: {
                            props: 'fcmToken',
                            value: fcmToken
                        }
                    })
                } else if (results.code) {
                    console.log(`${results.code}: ${results.message}`);
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

// export const notificationFetch = (userToken) => {
//     return (dispatch) => {
//         return fetch(`https://mybutler.giupviecxanh.vn/api/v1/notifications`, {
//                     method: 'GET',
//                     headers: {
//                         'Accept': 'application/json',
//                         'Content-Type': 'application/json',
//                         'Authorization': `Token ${userToken}`
//                     }
//                 })
//                 .then((response) => {
//                     if (response.status == 500) {
//                         return null;
//                     } else {
//                         return response.json();
//                     }
//                 })
//                 .then((results) => {
//                     if (!results) {
//                         return;
//                     }

//                     if (results.code === 200 || _.has(results, 'count')) {
//                         try {
//                             if (results.results.length > 0) {
//                                 let notifications = results.results;
//                                 for (i = 0; i < notifications.length; i++) {
//                                     notifications[i] = renameKey(notifications[i]);
//                                 }

//                                 let notiContent = '';
//                                 let notiInvContent = '';

//                                 for (i = 0; i < notifications.length; i++) {
//                                     notiContent += notifications[i].content;
//                                     if (i != notifications.length - 1) {
//                                         notiContent += '\n';
//                                     }

//                                     dispatch(notificationStatusUpdate(userToken, notifications[i].id, true))
//                                 }

//                                 pushNotification.localNotification('Công việc hoàn thành', notiContent);
//                             }
//                         } catch {
//                             // Do nothing
//                         }
//                     } else if (results.code === 1002) {
//                         // alert('Có lỗi xảy ra! Vui lòng đăng nhập lại.');
//                     } else if (results.code) {
//                         alert(`${results.code}: ${results.message}`);
//                     }
//                 })
//                 .catch((error) => {
//                     console.error(error);
//                 });
//     }
// }

// export const notificationStatusUpdate = (userToken, notiId, status) => {
//     return (dispatch) => {
//         return fetch(`https://mybutler.giupviecxanh.vn/api/v1/notifications/${notiId}`, {
//                     method: 'PUT',
//                     headers: {
//                         'Accept': 'application/json',
//                         'Content-Type': 'application/json',
//                         'Authorization': `Token ${userToken}`
//                     },
//                     body: JSON.stringify({
//                         status
//                     })
//                 })
//                 .then((response) => {
//                     if (response.status == 500) {
//                         return null;
//                     } else {
//                         return response.json();
//                     }
//                 })
//                 .then((results) => {
//                     console.log(results);
//                     if (!results) {
//                         return;
//                     }

//                     if (results.code === 200 || results.id) {
//                         dispatch({
//                             type: NOTIFICATION_REDUCER_UPDATE,
//                             payload: {
//                                 props: 'statusUpdate',
//                                 value: 'success'
//                             }
//                         })
//                     } else if (results.code) {
//                         alert(`${results.code}: ${results.message}`);
//                     }
//                 })
//                 .catch((error) => {
//                     console.error(error);
//                 });
//     }
// }