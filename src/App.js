/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import NetWorkProvider from './NetworkProvider';
// import { pushNotification } from './utils';
// pushNotification.configure();

type Props = {};
class App extends Component<Props> {
    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
                <NetWorkProvider />
            </Provider>                
        );        
    }
}

export default App;